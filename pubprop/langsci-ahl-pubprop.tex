\documentclass[
  12pt,
  american,
  a4paper,
]{article}

\author{Carsten Becker}
\title{Publishing proposal}
\date{June 17, 2023}

% Avoid orphans and widows
\clubpenalties=4 10000 5000 500 0
\widowpenalties=4 10000 5000 500 0

\usepackage{babel}
\usepackage{csquotes}
\usepackage{fontspec}
\usepackage{libertinus}
\usepackage{microtype}

\usepackage[
  style=langsci-unified,
  backend=biber,
  maxcitenames=2,
  maxbibnames=99,
  safeinputenc,
  natbib,
]{biblatex}
\addbibresource{../assets/bibliography.bib}
\hyphenation{Haupt-rei-he}

% Make divider for multicites and multipostnotes semicolon
\renewcommand{\multicitedelim}{\addsemicolon\addspace}
\renewcommand{\multipostnotedelim}{\addsemicolon\addspace}

\DeclareNameWrapperFormat{labelname:poss}{#1's}
\DeclareFieldFormat{shorthand:poss}{%
  \ifnameundef{labelname}{#1's}{#1}}
\DeclareFieldFormat{citetitle:poss}{\mkbibemph{#1}'s}
\DeclareFieldFormat{label:poss}{#1's}
\newrobustcmd*{\posscitealias}{%
  \AtNextCite{%
    \DeclareNameWrapperAlias{labelname}{labelname:poss}%
    \DeclareFieldAlias{shorthand}{shorthand:poss}%
    \DeclareFieldAlias{citetitle}{citetitle:poss}%
    \DeclareFieldAlias{label}{label:poss}}}
\newrobustcmd*{\posscite}{%
  \posscitealias%
  \textcite}
\newrobustcmd*{\Posscite}{\bibsentence\posscite}
\newrobustcmd*{\posscites}{%
  \posscitealias%
  \textcites}

\usepackage[shortlabels]{enumitem}
\newlist{sections}{enumerate}{10}
\setlist[sections]{noitemsep,label*=\arabic*.}

\usepackage{langsci-gb4e}
% \usepackage{tagpair}
\usepackage{leipzig}
\newleipzig{Concord}{concord}{syntactic concord}
\newleipzig{Index}{index}{referential index}
\newleipzig{St}{str}{strong}

% % Smaller attribution tags for less visual clutter
% % https://tex.stackexchange.com/a/627508/
% \usepackage{xpatch}
% \xpatchcmd{\taggedline}{{#2}}{{\footnotesize#2}}{}{}

\usepackage[
  pdfusetitle,
  bookmarks,
  linktoc=section,
  colorlinks=false,
  hidelinks,
]{hyperref}

\newcommand{\nosh}{\AtNextCite{\AtEachCitekey{\clearfield{shorthand}}}}

\begin{document}
\maketitle

\section{Suggested title}

\emph{Gender Resolution bei mittelhochdeutsch \emph{beide}: Eine
Analyse im Rahmen der Lexical-Functional Grammar}.

\bigskip

\noindent%
[\emph{Gender resolution of Middle High German \emph{beide}: A lexical-functional analysis}]

\section{Aim of the monograph}

The monograph surveys gender agreement of Middle High German (MHG)
\textit{bėide} `both' in its various grammatical contexts: as a D-quantifier
(\ref{ex:beidequant}) and as part of the related correlative conjunction
\textit{bėide \ldots{} unde} `both \ldots{} and' (\ref{ex:beideconj})
\autocite[623--628]{ksw2}.

\begin{exe}
\ex \label{ex:beidequant}
    \gll vlrich vnd frow Elzbet \textelp{} diſen prief /
        den ſi beidev habent gebeten ze ſchriben \\
        %
        Ulrich[\Nom.\Sg.\M] and Mrs. Elsbeth[\Nom.\Sg.\F] {} this charter
          / which \Tpl.\Nom{} both-\Nom.\Pl.\N.\St{} have asked to
          write \\
    \trans `Ulrich and Mrs.~Elsbeth \textelp{} this charter, which they
      both have asked to write'
  \jambox{\pvolcite[]{4}[176:26--27 {[No.~2843, Salzburg, 1297]}]{cao}}

\ex \label{ex:beideconj}
    \gll Beide zeberg vnd zetal \\
        both to=hill and to=valley \\
    \gll Fvrter ſie {vber al} \\
      led=he them everywhere \\
  \trans `Both on the hill and in the valley---he led them everywhere.'
  \jambox{(\emph{Kaiserchronik}, Vienna, Austrian Nat.~Lib., Cod.~2693,
    83vb:3--4)}
\end{exe}

The diplomatic transcriptions of Ger\-man-language medieval charters in the
\emph{Corpus der altdeutschen Original\-urkunden bis zum Jahr 1300}
(\emph{CAO}; \cites{cao}{cao-online}) on the one hand and the
\emph{Kaiserchronik} (\emph{KC}; \nosh\cites{schroeder1895}{kcdigital}[see
also][]{nellmann1983}) as represented by the most important complete
manuscripts of its three recensions on the other serve as data sources.
Different than in Modern Standard German \autocite[772]{woellstein2022}, the
Upper German dialects of the MHG period (c.~1050--1350~CE) distinguished
between masculine--feminine \textit{-e} and neuter \textit{-iu} in the
nominative--accusative plural of strong adjectives \autocite[182]{ksw2}.
Strikingly, both \textit{bėide} and \textit{bėidiu} occur in identical
grammatical contexts of combined reference in varieties which otherwise
productively distinguish both inflections in adjectives with single reference.

The aim of the monograph is to analyze and quantify the occurrence of both
inflected forms of \textit{bėide} with regard to (combined) formal and semantic
person features, word distance and syntactic domain of agreement controller and
target, as well as geography, with special attention to gender resolution
\autocite{corbett1983}. The Lexical-functional Grammar (LFG) framework
\autocites{bresnankaplan1982}{bresnanetal2016} provides a robust means to model
this phenomenon \autocites[171--195]{wechslerzlatic2003}{wechsler2009},
updating \posscite{askedal1973} analysis based on Prague School structuralism,
early generative grammar, and a very limited sample set.

Even though the phenomenon of the neuter serving as a resolution gender in
anaphoric reference also with animates is well-attested in the history of
Germanic languages \autocite[see e.g.][]{hock2008}, little work has so far been
done on a data-driven analysis of the phenomenon in MHG, especially one firmly
rooted in manuscript evidence rather than normalized editions. Furthermore,
\emph{communis opinio} on conjunctional \textit{bėide} holds it is fossilized
in either declension form. The book shows how the surveyed sources support this
conclusion, and searches for factors determining the respective historical
outcome.

\section{Table of contents}

\begin{sections}
\item
  Einleitung\\
  {[}Introduction{]}

  \begin{sections}
  \item
    \textit{Bėide} und das Phänomen der \textit{Gender Resolution}\\
    {[}\textit{Bėide} and the phenomenon of Gender Resolution{]}
  \item
    \textit{Bėide} auch konjunktional\\
    {[}\textit{Bėide} also conjunctional{]}
  \item
    Aufbau der Arbeit\\
    {[}Setup of the work{]}
  \end{sections}

\item
  Forschungsüberblick\\
  {[}Research overview{]}

  \begin{sections}    
  \item
    \textit{Bėide} als Quantor\\
    {[}\textit{Bėide} as a quantifier{]}
  \item
    \textit{Bėide} als Konjunktion\\
    {[}\textit{Bėide} as a conjunction{]}
  \end{sections}

\item
  Materialien\\
  {[}Materials{]}

  \begin{sections}    
  \item
    Das \emph{Corpus der altdeutschen Originalurkunden}\\
    {[}The \emph{Corpus der altdeutschen Originalurkunden}{]}
  \item
    Die \emph{Kaiserchronik}\\
    {[}The \emph{Kaiserchronik}{]}
  \end{sections}

\item
  Terminologie\\
  {[}Terminology{]}

  \begin{sections}
  \item
    Controller, Target und Domäne\\
    {[}Controller, target, and domain{]}
  \item
    Index und Concord\\
    {[}Index and concord{]}
  \item
    Genus, Sexus und Belebtheit\\
    {[}Gender, sex, and animacy{]}
  \item
    Gender Resolution\\
    {[}Gender resolution{]}
  \item
    Kongruenz in der Lexical-Functional Grammar\\
    {[}Agreement in Lexical-functional Grammar{]}
  \item
    Floating Quantifiers\\
    {[}Floating quantifiers{]}
  \item
    Die Kongruenzhierarchie\\
    {[}The agreement hierarchy{]}
  \item
    Erweiterter Konjunktionsbegriff\\
    {[}Extended notion of conjunction{]}
  \end{sections}

\item
  Methodik der Datenerhebung\\
  {[}Method of data collection{]}

  \begin{sections}
  \item
    Datenerhebung aus dem \textit{Corpus der altdeutschen
    Originalurkunden}\\
    {[}Data collection from the \textit{Corpus der altdeutschen
    Originalurkunden}{]}
  \item
    Datenerhebung aus der \emph{Kaiserchronik}\\
    {[}Data collection from the \emph{Kaiserchronik}{]}

    \begin{sections}
    \item
      Ausgeschlossene Belege\\
      {[}Excluded examples{]}
    \end{sections}
  \end{sections}

\item
  \textit{-e} und \textit{-iu} im Paradigma der starken Adjektivflexion\\
  {[}\textit{-e} and \textit{-iu} in the strong adjective inflection's
  paradigm{]}

  \begin{sections}
  \item
    Adjektivdeklination im \textit{Corpus der altdeutschen
    Originalurkunden}\\
    {[}Adjective declension in the \textit{Corpus der altdeutschen
    Originalurkunden}{]}

    \begin{sections}    
    \item
      Anlage der Stichprobe\\
      {[}Construction of the sample{]}
    \item
      Diskussion\\
      {[}Discussion{]}
    \item
      Zusammenfassung\\
      {[}Summary{]}
    \end{sections}

  \item
    Adjektivdeklination in der \emph{Kaiserchronik}\\
    {[}Adjective declension in the \emph{Kaiserchronik}{]}

    \begin{sections}
    \item
      Anlage der Stichprobe\\
      {[}Construction of the sample{]}
    \item
      Diskussion\\
      {[}Discussion{]}
    \item
      Zusammenfassung\\
      {[}Summary{]}
    \end{sections}
  \end{sections}

\item
  Auswertung zum \emph{Corpus der altdeutschen Originalurkunden}\\
  {[}Analyzing the \emph{Corpus der altdeutschen Originalurkunden}{]}

  \begin{sections}
  \item
    Geografische Verteilung der gesammelten Belege\\
    {[}Geographic distribution of collected examples{]}
  \item
    Targets nach Personenmerkmalen des Controllers\\
    {[}Targets according to controller's person features{]}

    \begin{sections}    
    \item
      Nominale Controller\\
      {[}Nominal controllers{]}

      \begin{sections}
      \def\labelenumiv{\arabic{enumiv}.}
      
      \item
        Kombinierte nominale Controller\\
        {[}Combined nominal controllers{]}
      \item
        Einfache nominale Controller\\
        {[}Single nominal controllers{]}
      \item
        Zusammenfassung\\
        {[}Summary{]}
      \end{sections}
    \item
      Anaphorische Controller\\
      {[}Anaphoric controllers{]}

      \begin{sections}      
      \item
        Indirekter Bezug auf kombinierte nominale Controller\\
        {[}Indirect reference to combined nominal controllers{]}
      \item
        Indirekter Bezug auf unkombinierte Plural-Controller\\
        {[}Indirect reference to non-combined plural controllers{]}
      \item
        Zu Askedals (1973) Hypothese der ‚Monoflexion‘\\
        {[}On \posscite{askedal1973} hypothesis of `monoinflection'{]}
      \item
        Zusammenfassung und Vergleich\\
        {[}Summary and comparison{]}
      \end{sections}
    \end{sections}

  \item
    Targets nach Distanz zum Controller\\
    {[}Targets according to distance to controller{]}

    \begin{sections}    
    \item
      Nominale Controller\\
      {[}Nominal controllers{]}
    \item
      Anaphorische Controller\\
      {[}Anaphoric controllers{]}
    \item
      Wortformenabstand zu kombinierten Erstcontrollern\\
      {[}Distance in word forms to combined primary controllers{]}
    \item
      Zusammenfassung und Vergleich\\
      {[}Summary and comparison{]}
    \end{sections}

  \item
    \textit{Bėide} als Konjunktion\\
    {[}\textit{Bėide} as a conjunction{]}

    \begin{sections}    
    \item
      Mit zwei Controllern\\
      {[}With two controllers{]}
    \item
      Mit zwei Targets\\
      {[}With two targets{]}
    \item
      Rein syntaktischer Kontext\\
      {[}Purely syntactic context{]}
    \item
      Zusammenfassung\\
      {[}Summary{]}
    \end{sections}
  \end{sections}

\item
  Auswertung zur \emph{Kaiserchronik}\\
  {[}Analyzing the \emph{Kaiserchronik}{]}

  \begin{sections}    
  \item
    Verteilung der gesammelten Belege in Zeit und Raum\\
    {[}Geographic and temporal distribution of collected examples{]}
  \item
    Targets nach Personenmerkmalen des Controllers\\
    {[}Targets according to controller's person features{]}

    \begin{sections}    
    \item
      Nominale Controller\\
      {[}Nominal controllers{]}

      \begin{sections}      
      \item
        Kombinierte nominale Controller\\
        {[}Combined nominal controllers{]}
      \item
        Einfache nominale Controller\\
        {[}Single nominal controllers{]}
      \item
        Zusammenfassung\\
        {[}Summary{]}
      \end{sections}

    \item
      Anaphorische Controller\\
      {[}Anaphoric controllers{]}

      \begin{sections}      
      \item
        Indirekter Bezug auf kombinierte nominale Controller\\
        {[}Indirect reference to combined nominal controllers{]}
      \item
        Indirekter Bezug auf unkombinierte Plural-Controller\\
        {[}Indirect reference to non-combined plural controllers{]}
      \item
        Zu Askedals (1973) Hypothese der ‚Monoflexion‘\\
        {[}On \posscite{askedal1973} hypothesis of `monoinflection'{]}
      \item
        Zusammenfassung und Vergleich\\
        {[}Summary and comparison{]}
      \end{sections}
    \end{sections}

  \item
    Targets nach Distanz zum Controller\\
    {[}Targets according to distance to controller{]}

    \begin{sections}    
    \item
      Nominale Controller\\
      {[}Nominal controllers{]}
    \item
      Anaphorische Controller\\
      {[}Anaphoric controllers{]}
    \item
      Wortformenabstand zu kombinierten Erstcontrollern\\
      {[}Distance in word forms to combined primary controllers{]}
    \item
      Zusammenfassung und Vergleich\\
      {[}Summary and comparison{]}
    \end{sections}

  \item
    \textit{Bėide} als Konjunktion\\
    {[}\textit{Bėide} as a conjunction{]}

    \begin{sections}    
    \item
      Mit zwei Controllern\\
      {[}With two controllers{]}
    \item
      Mit zwei Targets\\
      {[}With two targets{]}
    \item
      Rein syntaktischer Kontext\\
      {[}Purely syntactic context{]}
    \item
      Zusammenfassung\\
      {[}Summary{]}
    \end{sections}
  \end{sections}

\item
  Diskussion\\
  {[}Discussion{]}

  \begin{sections}
  \item
    \textit{Bėide} als Quantor\\
    {[}\textit{Bėide} as a quantifier{]}

    \begin{sections}    
    \item
      Direkter Bezug zwischen Controller und Target\\
      {[}Direct relation between controller and target{]}

      \begin{sections}      
      \item
        Belebte kombinierte Controller\\
        {[}Animate combined controllers{]}
      \item
        Unbelebte kombinierte Controller\\
        {[}Inanimate combined controllers{]}
      \end{sections}

    \item
      Indirekter Bezug zwischen Controller und Target\\
      {[}Indirect relation between controller and target{]}

      \begin{sections}      
      \item
        Belebte kombinierte Controller\\
        {[}Animate combined controllers{]}
      \item
        Unbelebte kombinierte Controller\\
        {[}Inanimate combined controllers{]}
      \end{sections}

    \item
      Neutrum bei maskulinem und femininem Bezug\\
      {[}Neuter with masculine and feminine reference{]}
    \item
      Zusammenfassung\\
      {[}Summary{]}
    \end{sections}

  \item
    \textit{Bėide} als Konjunktion\\
    {[}\textit{Bėide} as a conjunction{]}
  \end{sections}

\item
  Zusammenfassung und Ausblick\\
  {[}Summary and prospect{]}

  \begin{sections}
  \item
    Ergebnisse\\
    {[}Results{]}

    \begin{sections}
    \item
      \textit{Bėide} als Quantor\\
      {[}\textit{Bėide} as a quantifier: animates{]}
    \item
      \textit{Bėide} als Quantor\\
      {[}\textit{Bėide} as a quantifier: inanimates{]}
    \item
      \textit{Bėide} als Konjunktion\\
      {[}\textit{Bėide} as a conjunction{]}
    \end{sections}

  \item
    Ausblick\\
    {[}Prospect{]}
  \end{sections}
\end{sections}

\begin{sections}[A.]
\item
  Anhang\\
  {[}Appendix{]}

  \begin{sections}
  \item
    Liste der gesichteten Urkunden des \emph{CAO}\\
    {[}List of surveyed charters from the \emph{CAO}{]}

    \begin{sections}
    \item
      Ausgewertete Urkunden\\
      {[}Evaluated charters{]}
    \item
      Ausgesonderte Urkunden\\
      {[}Rejected charters{]}
    \end{sections}

  \item
    Stichprobe zur Grafie von mhd. \textit{e}\\
    {[}Sample on the spelling of MHG \textit{e}{]}
  \item
    Belegzahlen zur \emph{CAO}-Adjektivstichprobe\\
    {[}Tally of samples in the \emph{CAO} adjective survey{]}
  \end{sections}
\end{sections}

\section{Chapter-by-chapter summary}

\textbf{Chapter 1} introduces the observed phenomenon as well as the notion of
gender resolution \autocite{corbett1983}. It also illustrates how conjunctional
\textit{bėide} is used in MHG, seeing that the \textit{bėide \ldots{} unde}
construction had been fully replaced by \textit{sowohl \ldots{} als auch} `both
\ldots{} and' by the 18th century
\autocites[530, fn. 2]{walchhaeckel1988}[222]{dal2014}. Moreover, a chapter
outline is provided.

\bigskip

\textbf{Chapter 2} gives a brief overview of existing research, anchoring the
survey in the greater research context. Examples of neuter with mixed-sex
animate reference in other Germanic languages are given. Descriptions of the
phenomenon by \textcites[978]{grimm1848}[311--312]{grimm1890}[331]{grimm1898},
\textcite[39]{behaghel1928}, \textcite[now][384]{paul2007}, and
\textcite[now][188]{dal2014} are reported in light of \posscite{askedal1973}
criticism of their respective shortcomings. Weaknesses of
\citeauthor{askedal1973}'s study are criticized in turn
\autocite[see also][118]{fleischerschallert2011}.

\bigskip

\textbf{Chapter 3} characterizes the manuscript sources used regarding their
genesis, peculiarities, and structure. The \emph{CAO} is a collection of over
4,200 charters in late 13th-century MHG language mostly from the Upper German
dialect area in diplomatic transcription. \textcite{schulze2011} has shown that
medieval German charters are not simply translations from Latin and employ less
specifically legal jargon than previously assumed. She highlights their value
as a source for historical dialectal geography, refuting earlier claims that
our ignorance of most of their scribes' identity and linguistic socialization
disqualifies their dialectal authenticity. Instead, aggregating and mapping
examples makes regional feature clusters apparent
\autocites[122]{deboor1974}{beckerschallert2021}. The 13th-century charters
accepted to \posscites{ksw3,ksw2} grammar corpus only cover a few important
centers of writing, notably excluding Bavarian, for these earlier reservations
\autocite[cf.][1311--1318]{wegera2000}.

The \emph{KC} is a versified mirror of princes focusing on the spread of
Christianity during Roman and German emperors' reigns.
% , with individual episodes extended to hagiography-based novellas.
Anonymously created in the mid-12th century, the \emph{KC} is transmitted
mainly in the South-Eastern Upper and Central German areas of the 13th and 14th
century, totalling fifty identified witnesses. It experienced two independent
revisions (recensions B and C) around 1200 and 1250. In contrast to the
\emph{CAO}, the manuscripts of the \emph{KC} are neither dated nor inherently
localizable other than by their scribal dialect. Diplomatic transcriptions were
created for the \citetitle{kcdigital} project \autocite{kcdigital}.

\bigskip

\textbf{Chapter 4} introduces important terminology. It establishes working
definitions especially of such complex notions as gender and sex in their
grammatical sense, and the difference between agreement by \Concord{}
(NP-internal, distributive, formal) and \Index{} (collective, semantic) in LFG.

\bigskip

\textbf{Chapter 5} details the method of data collection. Since both source
collections exist in plain text only, data had to be gathered manually by
querying texts for relevant word forms with regular expressions based on the
spelling variants documented in the \textit{Wörterbuch der mittelhochdeutschen
Urkundensprache} (\emph{WMU}; \nosh\cite{wmu}) for the \emph{CAO}, and by way
of trial for the \emph{KC}. All occurrences of \textit{bėide} and
\textit{bėidiu} were manually annotated for morphological and semantic person
features, together with their immediate pronominal controllers if present, and
the NPs originally introducing the referents (`primary controllers'). The
distance in tokens between immediate controller(s) and target, syntactic
domain of the immediate agreement relationship
\autocite[see][54]{corbett2006}, occurrence of floating, and the order of
controller and target were recorded, too.

\bigskip

\textbf{Chapter 6} establishes whether charters from eleven major Upper German
places of issue and their environs as well as the nine surveyed \emph{KC}
manuscripts provide valid examples of the distinction between \textit{-e}
(\Acc.\Sg.\F, \Nom+\Acc.\Pl.\M+\F) and \textit{-iu} (\Nom.\Sg.\F,
\Nom+\Acc.\Pl.\N) forms at all by collecting and annotating commonly occurring
adjectives. With the exception of Strasbourg, the surveyed localities
consistently distinguish both forms. The distinction is completely absent from
only three \emph{KC} manuscripts, and requires some caution in four of the
remaining ones. The distinction between both inflected forms tends to be
clearer in the plural than in the singular for both text collections.

\bigskip

\textbf{Chapters 7 and 8} focus on the distribution of collected examples from
both the \emph{CAO} and the \emph{KC} as regards geography and grammatical
categories, and discuss noteworthy individual examples in a qualitative
perspective. Both chapters are structured identically: first, the geographical
distribution of collected examples of quantifying \textit{bėide} is shown and
discussed. Second, the sample for each source is analyzed for person features
involved, then by the absolute distance in tokens between agreement
controller and target as well as the extent of the agreement relationship's
syntactic domain. Third, the forms of conjunctional \textit{bėide} are analyzed
for their distribution in relation to the person features of NP conjuncts,
thereby further disproving \posscite{askedal1974} claim of conjunctional
\textit{bėide} still regularly showing agreement with NPs in MHG
\autocites[see][]{gjelsten1980}[628]{ksw2}.

In summary, the quantifier's neuter form \textit{bėidiu} frequently occurs with
mixed-sex animate referents, also when discourse referents are introduced by
pronouns not overtly gender-marked (\textit{ich} `I', \textit{du} `you', etc.).
Examples from the \emph{CAO} also regularly show neuter \textit{bėidiu} with
inanimates independent of its combined referents' grammatical gender. The
\emph{KC} did not yield any contexts of \textit{bėide} quantifying a pair of
inanimate referents, though only few examples of combined reference were
recorded in general. Floated quantifiers very regularly show semantic
agreement. \textit{Bėide} directly modifying two NPs as a quantifier is
extremely rare. Absolute distance between (primary) controllers and
\textit{bėide} targets had no measurable effect on the quantifier's agreement
form. In contexts of \textit{bėide} referring to single NPs in the plural, the
gender-based, formal pattern of agreement canonically manifests.

Conjunctional \textit{bėide} prominently features in the \emph{KC} and occurs
without restrictions to phrase type. This points to full desemanticization,
with \textit{bėide} serving as a focus particle
\autocites[cf.][425--428]{johannessen2005}{koenig1991}. Unsurprisingly, the
occurrence of conjunctional \textit{bėide} and \textit{bėidiu} is almost evenly
split.

\bigskip

\textbf{Chapter 9} introduces \posscite[171--195]{wechslerzlatic2003} model of
gender resolution as intersecting feature sets based on semantics of sex and
animacy and applies it to the sample. Especially the handful of cases of
combined male reference triggering neuter \textit{bėidiu} when
masculine--feminine \textit{bėide} is expected canonically cannot be explained
by their approach, but by overgeneralization of the gender-resolution pattern
with its neuter default \autocite[cf.][302]{corbett1991}. Overall, there seems
to be a degree of competition when \textit{bėide} modifies \textit{si} `they'
(\Tpl.\Nom+\Acc) as part of the same nominal group with reference to mixed-sex
animate controllers between semantic (\textit{bėidiu} \N, approx. 75\,\% of
cases) and formal agreement (\textit{bėide} \M+\F, approx. 25\,\%). The floated
quantifier, on the other hand, behaves as expected of a pronoun by strongly
favoring semantic agreement. Lastly, the genesis of conjunctional
\textit{bėide} is explained as grammaticalization in terms of
\textcite{lehmann2015}. Charting both variants reveals a preference for
\textit{bėidiu} in central Upper German, supporting \posscite[627--628]{ksw2}
findings.

\bigskip

\textbf{Chapter 10} summarizes the survey results and briefly identifies
possibilities for future research on MHG gender agreement morphology. Even
though a considerable amount of text was surveyed (\emph{CAO}: approx. 2
million tokens, \emph{KC} approx. 1 million), not all possible combinations of
person features are attested. It would also be desirable to survey other
split-NP constructions in MHG regarding semantic agreement as well as binding
restrictions. Analyzing the development of D-quantifiers in the greater context
of paradigmatic changes in the NP from MHG to New High German might also be of
interest, since they inhabit a middle ground between determiners and adjectives
proper. More generally, it would be desirable if faithful transcriptions of
more historical texts were made available electronically in full, given that
transcription constitutes an important preparatory step in producing scholarly
editions of historical texts.

\printbibliography

\end{document}
