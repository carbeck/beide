#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''A script to create the PostgreSQL database which holds all the records of
   this survey. This program only generates the structural prerequisites but
   does not import operational data.

   Carsten Becker, 2019
   <carsten [dot] becker [at] uni [dash] marburg [dot] de>
'''

import psycopg2
import credentials

# Definitions for tables
#
# Contains values for those tables with fixed values. This is in JSON format
# because I don't want to type out all the CREATE TABLE and INSERT statements
# manually. Also, for compatibility with whatever future purpose there may be.
data = [
    {# Table 'sources.places'
        'schema' : 'sources',
        'name' : 'places',
        'pkey' : 'id',
        'fields' : [
            ('place_name', 'varchar'),
            ('place_x', 'numeric'),
            ('place_y', 'numeric'),
            ('comment', 'varchar')
        ],
    },

    {# Table 'sources.meta'
        'schema' : 'sources',
        'name' : 'meta',
        'pkey' : 'id',
        'fields' : [
            ('src', 'varchar'),
            ('lang', 'varchar'),
            ('year_from', 'integer'),
            ('year_to', 'integer'),
            ('plcid', 'integer', ('sources.places', 'id', '')),
            ('comment', 'varchar')
        ],
    },

    {# Table 'sources.lines'
        'schema' : 'sources',
        'name' : 'lines',
        'pkey' : 'id',
        'fields' : [
            ('text', 'varchar'),
            ('loc', 'varchar'),
            ('schroeder', 'varchar'),
            ('srcid', 'integer', ('sources.meta', 'id', '')),
        ],
    },

    {# Table 'features.pos'
     # Abbreviations according to HiTS tagset, Dipper et al. (2013).
     #
     # NOTE: Due to later additions, these aren't in alphabetical order!
        'schema' : 'features',
        'name' : 'pos',
        'pkey' : 'id',
        'fields' : [
            ('val', 'varchar'),
            ('value_en', 'varchar'),
            ('value_de', 'varchar'),
        ],
        'values' : [
            (
                'Ø',
                '-',
                '-'
            ),
			(
                '?',
                '(unknown)',
                '(unbekannt)'
            ),
			(
                'ADJS',
                'adjective, substituting',
                'Adjektiv, substituierend'
            ),
			(
                'CARDS',
                'cardinal numeral, substituting',
                'Kardinalzahl, substituierend'
            ),
			(
                'DDS',
                'determiner, definite/demonstrative, substituting',
                'Determinativ, definit/demonstrativ, substituierend'
            ),
			(
                'DIA',
                'determiner, indefinite, attributive, preposed',
                'Determinativ, indefinit, attributiv, vorangestellt'
            ),
			(
                'DIN',
                'determiner, indefinite, attributive, postposed',
                'Determinativ, indefinit, attributiv, nachgestellt'
            ),
			(
                'DIS',
                'determiner, indefinite, substituting',
                'Determinativ, indefinit, substituierend'
            ),
			(
                'DPOSA',
                'determiner, possessive, attributive, preposed',
                'Determinativ, possessiv, attributiv, vorangestellt'
            ),
			(
                'DRELS',
                'determiner, relativizing, substituting',
                'Determinativ, relativisch, substituierend'
            ),
			(
                'NA',
                'noun, common',
                'Nomen appelativum'
            ),
			(
                'NE',
                'noun, proper',
                'Eigenname'
            ),
			(
                'PPER',
                'pronoun, personal, irreflexive',
                'Pronomen, personal, irreflexiv'
            ),
			(
                'PRF',
                'pronoun, personal, reflexive',
                'Pronomen, personal, reflexiv'
            ),
			(
                'PTKREL',
                'relative particle',
                'Relativpartikel'
            ),
            (
                'VVFIN',
                'full verb, finite',
                'Vollverb, finit'
            ),
            (
                'DID',
                'determinative, indefinite, predicative',
                'Determinativ, indefinit, prädikativ'
            ),
            (
            	'ADJN',
            	'adjective, attributive, postposed',
            	'Adjektiv, attributiv, nachgestellt'
            ),
			(
				'ADJD',
				'adjective, predicative',
				'Adjektiv, prädikativ'
			),
			(
				'AVD',
				'adverb',
				'Adverb'
			),
			(
				'DIA-KO',
				'determiner, indefinite, attributive, preposed, conjunctive',
				'Determinativ, indefinit, attributiv, vorangestellt, konjunktional'
			),
			(
				'DIN/DID',
				'determiner, indefinite, postposed or predicative',
				'Determinativ, indefinit, nachgestellt oder prädikativ'
			),
            (
                'APPR',
                'preposition',
                'Präposition'
            ),
            (
                'VVPP',
                'full verb, preterite participle',
                'Vollverb, Partizip Präteritum'
            ),
        ],
    },

    {# Table 'features.pers'
        'schema' : 'features',
        'name' : 'pers',
        'pkey' : 'id',
        'fields' : [
            ('val', 'varchar'),
            ('value_en', 'varchar'),
            ('value_de', 'varchar'),
        ],
        'values' : [
            (
                'Ø',
                '-',
                '-'
            ),
			(
                '?',
                '(unknown)',
                '(unbekannt)'
            ),
			(
                '1',
                '1st',
                '1.'
            ),
			(
                '2',
                '2nd',
                '2.'
            ),
			(
                '3',
                '3rd',
                '3.'
            ),
			(
                'Ø+Ø',
                '- + -',
                '- + -'
            ),
			(
                'Ø+?',
                '- + (unknown)',
                '- + (unbekannt)'
            ),
			(
                'Ø+1',
                '- + 1st',
                '- + 1.'
            ),
			(
                'Ø+2',
                '- + 2nd',
                '- + 2.'
            ),
			(
                'Ø+3',
                '- + 3rd',
                '- + 3.'
            ),
			(
                '?+Ø',
                '(unknown) + -',
                '(unbekannt) + -'
            ),
			(
                '?+?',
                '(unknown) + (unknown)',
                '(unbekannt) + (unbekannt)'
            ),
			(
                '?+1',
                '(unknown) + 1st',
                '(unbekannt) + 1.'
            ),
			(
                '?+2',
                '(unknown) + 2nd',
                '(unbekannt) + 2.'
            ),
			(
                '?+3',
                '(unknown) + 3rd',
                '(unbekannt) + 3.'
            ),
			(
                '1+Ø',
                '1st + -',
                '1. + -'
            ),
			(
                '1+?',
                '1st + (unknown)',
                '1. + (unbekannt)'
            ),
			(
                '1+1',
                '1st + 1st',
                '1. + 1.'
            ),
			(
                '1+2',
                '1st + 2nd',
                '1. + 2.'
            ),
			(
                '1+3',
                '1st + 3rd',
                '1. + 3.'
            ),
			(
                '2+Ø',
                '2nd + -',
                '2. + -'
            ),
			(
                '2+?',
                '2nd + (unknown)',
                '2. + (unbekannt)'
            ),
			(
                '2+1',
                '2nd + 1st',
                '2. + 1.'
            ),
			(
                '2+2',
                '2nd + 2nd',
                '2. + 2.'
            ),
			(
                '2+3',
                '2nd + 3rd',
                '2. + 3.'
            ),
			(
                '3+Ø',
                '3rd + -',
                '3. + -'
            ),
			(
                '3+?',
                '3rd + (unknown)',
                '3. + (unbekannt)'
            ),
			(
                '3+1',
                '3rd + 1st',
                '3. + 1.'
            ),
			(
                '3+2',
                '3rd + 2nd',
                '3. + 2.'
            ),
			(
                '3+3',
                '3rd + 3rd',
                '3. + 3.'
            ),
        ],
    },

    {# Table 'features.num'
        'schema' : 'features',
        'name' : 'num',
        'pkey' : 'id',
        'fields' : [
            ('val', 'varchar'),
            ('value_en', 'varchar'),
            ('value_de', 'varchar'),
        ],
        'values' : [
            (
                'Ø',
                '-',
                '-'
            ),
			(
                '?',
                '(unknown)',
                '(unbekannt)'
            ),
			(
                'SG',
                'singular',
                'Singular'
            ),
			(
                'PL',
                'plural',
                'Plural'
            ),
			(
                'Ø+Ø',
                '- + -',
                '- + -'
            ),
			(
                'Ø+?',
                '- + (unknown)',
                '- + (unbekannt)'
            ),
			(
                'Ø+SG',
                '- + singular',
                '- + Singular'
            ),
			(
                'Ø+PL',
                '- + plural',
                '- + Plural'
            ),
			(
                '?+Ø',
                '(unknown) + -',
                '(unbekannt) + -'
            ),
			(
                '?+?',
                '(unknown) + (unknown)',
                '(unbekannt) + (unbekannt)'
            ),
			(
                '?+SG',
                '(unknown) + singular',
                '(unbekannt) + Singular'
            ),
			(
                '?+PL',
                '(unknown) + plural',
                '(unbekannt) + Plural'
            ),
			(
                'SG+Ø',
                'singular + -',
                'Singular + -'
            ),
			(
                'SG+?',
                'singular + (unknown)',
                'Singular + (unbekannt)'
            ),
			(
                'SG+SG',
                'singular + singular',
                'Singular + Singular'
            ),
			(
                'SG+PL',
                'singular + plural',
                'Singular + Plural'
            ),
			(
                'PL+Ø',
                'plural + -',
                'Plural + -'
            ),
			(
                'PL+?',
                'plural + (unknown)',
                'Plural + (unbekannt)'
            ),
			(
                'PL+SG',
                'plural + singular',
                'Plural + Singular'
            ),
			(
                'PL+PL',
                'plural + plural',
                'Plural + Plural'
            ),
        ],
    },

    {# Table 'features.gend'
        'schema' : 'features',
        'name' : 'gend',
        'pkey' : 'id',
        'fields' : [
            ('val', 'varchar'),
            ('value_en', 'varchar'),
            ('value_de', 'varchar'),
        ],
        'values' : [
            (
                'Ø',
                '-',
                '-'
            ),
			(
                '?',
                '(unknown)',
                '(unbekannt)'
            ),
			(
                'M',
                'masculine',
                'Maskulin'
            ),
			(
                'F',
                'feminine',
                'Feminin'
            ),
			(
                'N',
                'neuter',
                'Neutrum'
            ),
			(
                'Ø+Ø',
                '- + -',
                '- + -'
            ),
			(
                'Ø+?',
                '- + (unknown)',
                '- + (unbekannt)'
            ),
			(
                'Ø+M',
                '- + masculine',
                '- + Maskulin'
            ),
			(
                'Ø+F',
                '- + feminine',
                '- + Feminin'
            ),
			(
                'Ø+N',
                '- + neuter',
                '- + Neutrum'
            ),
			(
                '?+Ø',
                '(unknown) + -',
                '(unbekannt) + -'
            ),
			(
                '?+?',
                '(unknown) + (unknown)',
                '(unbekannt) + (unbekannt)'
            ),
			(
                '?+M',
                '(unknown) + masculine',
                '(unbekannt) + Maskulin'
            ),
			(
                '?+F',
                '(unknown) + feminine',
                '(unbekannt) + Feminin'
            ),
			(
                '?+N',
                '(unknown) + neuter',
                '(unbekannt) + Neutrum'
            ),
			(
                'M+Ø',
                'maskuline + -',
                'Maskulin + -'
            ),
			(
                'M+?',
                'masculine + (unknown)',
                'masculine + (unbekannt)'
            ),
			(
                'M+M',
                'masculine + masculine',
                'Maskulin + Maskulin'
            ),
			(
                'M+F',
                'masculine + feminine',
                'Maskulin + Feminin'
            ),
			(
                'M+N',
                'masculine + neuter',
                'Maskulin + Neutrum'
            ),
			(
                'F+Ø',
                'feminine + -',
                'Feminin + -'
            ),
			(
                'F+?',
                'feminine + (unknown)',
                'Feminin + (unbekannt)'
            ),
			(
                'F+M',
                'feminine + masculine',
                'Feminin + Maskulin'
            ),
			(
                'F+F',
                'feminine + feminine',
                'Feminin + Feminin'
            ),
			(
                'F+N',
                'feminine + neuter',
                'Feminin + Neutrum'
            ),
			(
                'N+Ø',
                'neuter + -',
                'Neutrum + -'
            ),
			(
                'N+?',
                'neuter + (unknown)',
                'Neutrum + (unbekannt)'
            ),
			(
                'N+M',
                'neuter + masculine',
                'Neutrum + Maskulin'
            ),
			(
                'N+F',
                'neuter + feminine',
                'Neutrum + Feminin'
            ),
			(
                'N+N',
                'neuter + neuter',
                'Neutrum + Neutrum'
            ),
        ],
    },

    {# Table 'features.sex'
     #
     # NOTE: Due to later additions, these aren't in alphabetical order!
        'schema' : 'features',
        'name' : 'sex',
        'pkey' : 'id',
        'fields' : [
            ('val', 'varchar'),
            ('value_en', 'varchar'),
            ('value_de', 'varchar'),
        ],
        'values' : [
            (
                'Ø',
                '-',
                '-'
            ),
			(
                '?',
                '(unknown)',
                '(unbekannt)'
            ),
			(
                'M',
                'male',
                'männlich'
            ),
			(
                'F',
                'female',
                'weiblich'
            ),
			(
                'Ø+Ø',
                '- + -',
                '- + -'
            ),
			(
                'Ø+?',
                '- + (unknown)',
                '- + (unbekannt)'
            ),
			(
                'Ø+M',
                '- + male',
                '- + männlich'
            ),
			(
                'Ø+F',
                '- + female',
                '- + weiblich'
            ),
			(
                '?+Ø',
                '(unknown) + -',
                '(unbekannt) + -'
            ),
			(
                '?+?',
                '(unknown) + (unknown)',
                '(unbekannt) + (unbekannt)'
            ),
			(
                '?+M',
                '(unknown) + male',
                '(unbekannt) + männlich'
            ),
			(
                '?+F',
                '(unknown) + female',
                '(unbekannt) + weiblich'
            ),
			(
                'M+Ø',
                'male + -',
                'männlich + -'
            ),
			(
                'M+?',
                'male + (unknown)',
                'männlich + (unbekannt)'
            ),
			(
                'M+M',
                'male + male',
                'männlich + männlich'
            ),
			(
                'M+F',
                'male + female',
                'männlich + weiblich'
            ),
			(
                'F+Ø',
                'female + -',
                'weiblich + -'
            ),
			(
                'F+?',
                'female + (unknown)',
                'weiblich + (unbekannt)'
            ),
			(
                'F+M',
                'female + male',
                'weiblich + männlich'
            ),
			(
                'F+F',
                'female + female',
                'weiblich + weiblich'
            ),
            (
                '*',
                '(any)',
                '(beliebig)'
            ),
            (
                '*+*',
                '(any) + (any)',
                '(beliebig) + (beliebig)'
            ),
            (
                'Ø+*',
                '- + (any)',
                '- + (beliebig)'
            ),
            (
                '?+*',
                '(unknown) + (any)',
                '(unbekannt) + (beliebig)'
            ),
            (
                'M+*',
                'male + (any)',
                'männlich + (beliebig)'
            ),
            (
                'F+*',
                'female + (any)',
                'eiblich + (beliebig)'
            ),
            (
                '*+Ø',
                '(any) + -',
                '(beliebig) + -'
            ),
            (
                '*+?',
                '(any) + (unknown)',
                '(beliebig) + (unbekannt)'
            ),
            (
                '*+M',
                '(any) + male',
                '(beliebig) + männlich'
            ),
            (
                '*+F',
                '(any) + female',
                '(beliebig) + weiblich'
            ),
        ],
    },

    {# Table 'features.anim'
        'schema' : 'features',
        'name' : 'anim',
        'pkey' : 'id',
        'fields' : [
            ('val', 'varchar'),
            ('value_en', 'varchar'),
            ('value_de', 'varchar'),
        ],
        'values' : [
            (
                'Ø',
                '-',
                '-'
            ),
			(
                '?',
                '(unknown)',
                '(unbekannt)'
            ),
			(
                'A',
                'animate',
                'belebt'
            ),
			(
                'I',
                'inanimate',
                'unbelebt'
            ),
			(
                'Ø+Ø',
                '- + -',
                '- + -'
            ),
			(
                'Ø+?',
                '- + (unknown)',
                '- + (unbekannt)'
            ),
			(
                'Ø+A',
                '- + animate',
                '- + belebt'
            ),
			(
                'Ø+I',
                '- + inanimate',
                '- + unbelebt'
            ),
			(
                '?+Ø',
                '(unknown) + -',
                '(unbekannt) + -'
            ),
			(
                '?+?',
                '(unknown) + (unknown)',
                '(unbekannt) + (unbekannt)'
            ),
			(
                '?+A',
                '(unknown) + animate',
                '(unbekannt) + belebt'
            ),
			(
                '?+I',
                '(unknown) + inanimate',
                '(unbekannt) + unbelebt'
            ),
			(
                'A+Ø',
                'animate + -',
                'belebt + -'
            ),
			(
                'A+?',
                'animate + (unknown)',
                'belebt + (unbekannt)'
            ),
			(
                'A+A',
                'animate + animate',
                'belebt + belebt'
            ),
			(
                'A+I',
                'animate + inanimate',
                'belebt + unbelebt'
            ),
			(
                'I+Ø',
                'inanimate + -',
                'unbelebt + -'
            ),
			(
                'I+?',
                'inanimate + (unknown)',
                'unbelebt + (unbekannt)'
            ),
			(
                'I+A',
                'inanimate + animate',
                'unbelebt + belebt'
            ),
			(
                'I+I',
                'inanimate + inanimate',
                'unbelebt + unbelebt'
            ),
        ],
    },

    {# Table 'features.case'
        'schema' : 'features',
        'name' : 'case',
        'pkey' : 'id',
        'fields' : [
            ('val', 'varchar'),
            ('value_en', 'varchar'),
            ('value_de', 'varchar'),
        ],
        'values' : [
            (
                'NOM',
                'nominative',
                'Nominativ'
            ),
            (
                'ACC',
                'accusative',
                'Akkusativ'
            ),
            (
                'DAT',
                'dative',
                'Dativ'
            ),
            (
                'GEN',
                'genitive',
                'Genitiv'
            ),
        ],
    },

    {# Table 'features.ctorder'
        'schema' : 'features',
        'name' : 'ctorder',
        'pkey' : 'id',
        'fields' : [
            ('val', 'varchar'),
            ('value_en', 'varchar'),
            ('value_de', 'varchar'),
        ],
        'values' : [
            (
                'c>t',
                'controller → target',
                'Controller → Target'
            ),
            (
                't>c',
                'target → controller',
                'Target → Controller'
            ),
        ],
    },

    {# Table 'features.distsyn'
        'schema' : 'features',
        'name' : 'distsyn',
        'pkey' : 'id',
        'fields' : [
            ('val', 'varchar'),
            ('value_en', 'varchar'),
            ('value_de', 'varchar'),
        ],
        'values' : [
            (
                '0',
                'same phrase',
                'gleiche Phrase'
            ),
            (
                '1',
                'same clause',
                'gleicher Teilsatz'
            ),
            (
                '2',
                'other clause',
                'anderer Satz'
            ),
        ],
    },

    {# Table 'features.agrrel'
        'schema' : ' features',
        'name' : 'agrrel',
        'pkey' : 'id',
        'fields' : [
            ('val', 'varchar'),
            ('value_en', 'varchar'),
            ('value_de', 'varchar'),
        ],
        'values' : [
            (
                'attributive',
                'attributive',
                'attributiv'
            ),
            (
                'phoric',
                'phoric',
                'phorisch'
            ),
            (
                'exophoric',
                'exophoric',
                'exophorisch'
            ),
            (
                'predicative',
                'predicative',
                'prädikativ'
            ),
            (
                'co-predicative',
                'co-predicative',
                'koprädikativ'
            ),
        ],
    },

    {# Table 'public.controller'
        'schema' : 'public',
        'name' : 'controller',
        'pkey' : 'id',
        'fields' : [
            ('text', 'varchar'),
            ('linid', 'integer', ('sources.lines', 'id', '')),
            ('loc', 'varchar'),
            ('co_ctrlid', 'integer'),
            ('form_pers', 'integer', ('features.pers', 'id', '')),
            ('form_num', 'integer', ('features.num', 'id', '')),
            ('form_gend', 'integer', ('features.gend', 'id', '')),
            ('form_case', 'integer', ('features.case', 'id', '')),
            ('sem_pers', 'integer', ('features.pers', 'id', '')),
            ('sem_num', 'integer', ('features.num', 'id', '')),
            ('sem_sex', 'integer', ('features.sex', 'id', '')),
            ('sem_anim', 'integer', ('features.anim', 'id', '')),
            ('pos', 'integer', ('features.pos', 'id', '')),
            ('flex_type', 'varchar'),
            ('flex_rhyme', 'boolean'),
            ('flex_elidable', 'boolean'),
            ('orig', 'boolean'),
            ('comment', 'varchar'),
        ],
    },

    {# Table 'public.target'
        'schema' : 'public',
        'name' : 'target',
        'pkey' : 'id',
        'fields' : [
            ('text', 'varchar'),
            ('linid', 'integer', ('sources.lines', 'id', '')),
            ('loc', 'varchar'),
            ('form_pers', 'integer', ('features.pers', 'id', '')),
            ('form_num', 'integer', ('features.num', 'id', '')),
            ('form_gend', 'integer', ('features.gend', 'id', '')),
            ('form_case', 'integer', ('features.case', 'id', '')),
            ('sem_pers', 'integer', ('features.pers', 'id', '')),
            ('sem_num', 'integer', ('features.num', 'id', '')),
            ('sem_sex', 'integer', ('features.sex', 'id', '')),
            ('sem_anim', 'integer', ('features.anim', 'id', '')),
            ('pos', 'integer', ('features.pos', 'id', '')),
            ('flex_type', 'varchar'),
            ('flex_rhyme', 'boolean'),
            ('flex_elidable', 'boolean'),
            ('floatquant', 'boolean'),
            ('intermed', 'boolean'),
            ('comment', 'varchar'),
        ],
    },

    {# Table 'public.domain'
        'schema' : 'public',
        'name' : 'domain',
        'pkey' : 'id',
        'fields' : [
            ('ctorder', 'integer'),
            ('dist_word', 'integer'),
            ('dist_syn', 'integer'),
            ('agrrel', 'integer'),
        ],
    },

    {# Table 'public.sets'
        'schema' : 'public',
        'name' : 'sets',
        'pkey' : 'id',
        'fields' : [
            ('setno', 'integer'),
            ('ctrlid', 'integer', ('public.controller','id', '')),
            ('targid', 'integer', ('public.target','id', '')),
            ('domid', 'integer', ('public.domain','id', '')),
            ('srcid', 'integer', ('sources.meta', 'id', '')),
            ('set_id_parallel', 'varchar'),
            ('intermsc_var', 'boolean'),
            ('active', 'boolean'), # FIXME: should include default = TRUE
        ],
    },

    {# Table public.overlap
        'schema' : 'public',
        'name' : 'overlap',
        'pkey' : 'id',
        'fields' : [
            ('targid', 'integer'),
            ('ctrlid', 'integer'),
        ],
    }

    # {# Table ''
    #     'schema' : '',
    #     'name' : '',
    #     'pkey' : 'id',
    #     'fields' : [
    #         ('', ''),
    #         ('', ''),
    #         ('', ''),
    #     ],
    #     'values' : [
    #         ('', ''),
    #         ('', ''),
    #         ('', ''),
    #     ],
    # },
]

# Open a database connection 
#
# This supposes a database `beide` to already exist on the host. To newly create
# a database, open `psql` in a terminal and execute: "CREATE DATABASE beide;".
# To set up the required credentials, make a copy of the `credentials-sample.py`
# file called `credentials.py` and edit its contents according to your server's
# configuration.
try:
    conn = psycopg2.connect(
        dbname = credentials.DB,
        user = credentials.USER,
        host = credentials.HOST,
        password = credentials.PASS
        )
except psycopg2.DatabaseError as e:
    raise e

# Instantiate DB cursor
cur = conn.cursor()

# Automatically commit things
conn.autocommit = True

# Create main structure: schemas, tables, predefined values
for dataset in data:

    # Create table schema, if any is defined
    try:
        cur.execute('CREATE SCHEMA IF NOT EXISTS {}'.format(dataset['schema']))
    except psycopg2.DatabaseError as e:
        raise e

    # Create table
    # (presupposes that key `fields` exists in `dataset`)

    ## Initiate basic CREATE TABLE statement
    stmt = 'CREATE TABLE {schema}.{name} ({pkey} serial PRIMARY KEY'.format(
        **dataset)

    for f in dataset['fields']:
        # Take a tuple `f` in the `fields` register and format a line in the 
        # statement accordingly
        field = ', {} {}'.format(*f)

        # If `f` contains 3 definitions (for foreign key statements), add that
        # text as well
        if len(f) == 3:
            field += ' REFERENCES {} ({}) {}'.format(*f[2])

        # Add the `field` definition line to the CREATE TABLE statement
        stmt += field

    # Return CREATE TABLE statement
    stmt += ')'
    
    # Execute query
    try:
        cur.execute(stmt)
    except psycopg2.DatabaseError as e:
        raise e

    # Fill with predefined values, if any are defined
    if 'values' in dataset:
        for vset in dataset['values']:
            stmt = 'INSERT INTO {}.{} ({}) VALUES ({})'.format(
                dataset['schema'],
                dataset['name'],
                ', '.join([name for name, encoding in dataset['fields']]),
                ', '.join(['%s' for val in vset])
            )

            # Execute query
            try:
                cur.execute(stmt, vset)
            except psycopg2.DatabaseError as e:
                raise e

# Close cursor
cur.close()

# Close DB connection
conn.close()
