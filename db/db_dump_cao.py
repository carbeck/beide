#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''Export deed texts with their meta data from preexisting CAO DB as JSON
   files to use with the `beide` project.

   Carsten Becker, 2019
   <carsten [dot] becker [at] uni [dash] marburg [dot] de>
'''

import credentials
import json
import os
import os.path
import psycopg2
import psycopg2.extras
import zipfile

# Numbers of deeds which contain examples
deednos = ["78", "81", "131", "165", "171", "179", "190", "199", "201", "214",
	"326", "371", "389", "415", "491", "501-A", "501-B", "508", "519", "524",
	"549", "559", "560", "583", "602", "610", "619", "623", "629", "636",
	"656", "661-A", "661-B", "677", "682", "777", "885", "904", "923", "925",
	"937", "971-A", "971-B", "979", "1001", "1055", "1073", "1121", "1145",
	"1154", "1169", "1201-A", "1201-B", "1218", "1221", "1229", "1234", "1244",
	"1259", "1270", "1282", "1304", "1359", "1382", "1414", "1416", "1429",
	"1436", "1460", "1503", "1504", "1514", "1584", "1657", "1661", "1662",
	"1717", "1747", "1820", "1843", "1898", "1923", "1950", "1956", "1972-B",
	"2001", "2005", "2008", "2011", "2033", "2055", "2092", "2110", "2111",
	"2174", "2183", "2214", "2226", "2240", "2253", "2309", "2310", "2338-A",
	"2338-B", "2350", "2359", "2366", "2367", "2396", "2401", "2445", "2497",
	"2520", "2522", "2529", "2535", "2563", "2568", "2583", "2607", "2651-A",
	"2651-B", "2694", "2719", "2733", "2748", "2786", "2824", "2841", "2843",
	"2872", "2915", "2930", "2931", "2957", "2960", "2962", "3020-A", "3020-B",
	"3022", "3034", "3038", "3045", "3047", "3056", "3116", "3133", "3141-A",
	"3141-B", "3160", "3171", "3197", "3224-A", "3224-B", "3248", "3249",
	"3261", "3262", "3330", "3331", "3376", "3496", "N-52-78-c", "N-92-124-a",
	"N-100-144-a", "N-109-A-178-a", "N-109-B-178-a", "N-115-191-a",
	"N-150-324-b", "N-197-477-a", "N-202-492-a", "N-210-520-a", "N-230-582-a",
	"N-235-605-a", "N-241-616-b", "N-272-709-a", "N-288-752-a", "N-294-762-a",
	"N-305-807-a", "N-337-936-a", "N-357-969-a", "N-384-1077-a",
	"N-385-1077-b", "N-386-1077-c", "N-401-1125-a", "N-456-1280-b",
	"N-475-1353-b", "N-518-1527-a", "N-524-1545-a", "N-526-1548-b",
	"N-557-1669-a", "N-567-1686-a", "N-590-1769-b", "N-664-2055-a",
	"N-701-2149-b", "N-702-2149-c", "N-723-2211-a", "N-727-2240-a",
	"N-756-2352-a", "N-812-2352-a"
]

# Establish DB connection
try:
    conn = psycopg2.connect(
        host=credentials.HOST, 
        dbname="cao",
        user=credentials.USER,
        password=credentials.PASS
    )
    
except psycopg2.OperationalError as e:
    raise e
    
# Establishing DB cursor
cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

# Automatically commit things
conn.autocommit = True

# Prepare to write data into 'data/cao.zip' at the end
with zipfile.ZipFile(os.path.join('data','cao.zip'), mode='w',
    compression=zipfile.ZIP_DEFLATED) as zip:

    # Loop over deed numbers in `deedno`
    for n in deednos:
        # FIRST, get the line numbers and text from the DB
        query = """SELECT row AS loc, text FROM lines WHERE deedno_cao_long = %s 
                ORDER BY page, line ASC"""

        # Execute query
        cur.execute(query, (n,))

        # Fetch all the results
        deedtext = []
        for row in cur.fetchall():
            deedtext.append(dict(row))

        # SECOND, get the meta data
        query = """
            SELECT meta_cao.deedno_cao_long AS deedno_long,
                meta_cao.deedno_trier AS deedno_trier,
                meta_cao.date_from,
                meta_cao.date_to,
                meta_cao.date_note,
                places.id AS place_id_caodb,
                places.id_place_trier AS place_id_trier,
                places.place,
                places.x,
                places.y,
                CONCAT('http://tcdh01.uni-trier.de/cgi-bin/iCorpus/CorpusIndex.tcl?hea=qf&for=qfcoraltdu&cnt=qfcoraltdu&xid=', meta_cao.deedno_trier) AS link_trier
            FROM meta_cao, (
                SELECT id_place_trier
                FROM meta_cao
                INNER JOIN meta_places
                    ON meta_cao.deedno_cao_long = meta_places.deedno_cao_long
                WHERE meta_cao.deedno_cao_long = %(n)s
            ) AS placeids
            JOIN places ON placeids.id_place_trier = places.id_place_trier
            WHERE meta_cao.deedno_cao_long = %(n)s
        """

        # Execute query
        cur.execute(query, {'n' : n})

        # Fetch all the results
        deedmeta = []
        for row in cur.fetchall():
            deedmeta.append(dict(row))

        # Make dates strings; JSON can't deal with datetime objects
        for i in deedmeta:
            i["date_from"] = {
                'ymd' : str(i["date_from"]),
                'year' : str(i["date_from"]).split('-')[0],
                'month' : str(i["date_from"]).split('-')[1],
                'day' : str(i["date_from"]).split('-')[2]
            }
            i["date_to"] = {
                'ymd' : str(i["date_to"]),
                'year' : str(i["date_to"]).split('-')[0],
                'month' : str(i["date_to"]).split('-')[1],
                'day' : str(i["date_to"]).split('-')[2]
            }

        # THIRD, unify the information We're only dealing with deeds which have
        # a single place attached to them, so it's safe to assume that
        # `deedmeta` doesn't need to be embedded in a list the way it's returned
        # form the query.
        data = {'lines' : deedtext, 'meta' : deedmeta[0]}

        # Cast into JSON format (pretty-printed for human readability)
        jsondata = '{}\n'.format(json.dumps(data, indent=4, sort_keys=True))

        # Write into ZIP file
        zip.writestr('cao_{}.json'.format(n), jsondata)

# Close cursor
cur.close()

# Close DB connection
conn.close()
