-- CREATE OR REPLACE VIEW public.sets_firstctrl_lasttarg AS
SELECT DISTINCT
    s.setno,
    s.set_id_parallel,
    s.intermsc_var,
    s.ctrlid          AS ctrlid_first,
    intertarg.targids AS targid_intermed,
    maxtarg.targid    AS targid_last,
    s.srcid
--
-- List of controllers marked as original
--
FROM (
	SELECT c.id AS ctrlid
	FROM public.controller c
	WHERE c.orig = TRUE
) AS origctrl
--
-- Join "sets" table to get information on targets, too
--
JOIN public."sets" s ON s.ctrlid = origctrl.ctrlid
--
-- Join a list of intermediary targets (array)
--
JOIN (
    SELECT
        s.setno,
        array_agg(DISTINCT s.targid) AS targids
    FROM public."sets" s
    -- Exclude highest targid per set from the list
    WHERE s.targid NOT IN (
        SELECT MAX(s.targid)
        FROM public."sets" s
        GROUP BY s.setno
    )
    GROUP BY s.setno
) AS intertarg ON intertarg.setno = s.setno
--
-- Join the set's final target
--
JOIN (
    SELECT
        s.setno,
        s.targid
    FROM (
        --
        -- Largest targid per set
        --
        SELECT MAX(public."sets".targid) AS targid
        FROM public."sets"
        GROUP BY public."sets".setno
    ) AS m
    JOIN public."sets" s ON m.targid = s.targid
) AS maxtarg ON maxtarg.setno = s.setno
WHERE s.active = TRUE
ORDER BY
	ctrlid_first,
	targid_last
;
