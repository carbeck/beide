WITH RECURSIVE rset AS (
    --
    -- The non-recursive part
    --
    -- We simply select all those columns that we need as a starter. Since the
    -- main table is public.sets, we basically generate a sets table, but
    -- instead of the direct target of a controller, we include the target-
    -- turned-controller's target.
    --
    SELECT
        s.setno,
        s.ctrlid,            -- original ctrlid
        ls.targid AS targid, -- linked targid
        s.srcid
    FROM public."sets" s
    --
    -- Join public.overlap to tell us which target becomes a controller
    --
    JOIN public.overlap o  ON o.targid  = s.targid
    --
    -- And join public.sets back on this target-turned-controller
    --
    JOIN public."sets"  ls ON ls.ctrlid = o.ctrlid
    WHERE
            o.ctrlid NOTNULL
        AND s.active = TRUE
    --
    -- The recursive part
    --
    -- Do the same thing as above, but cycle through lines that reference
    -- each other.
    --
    UNION
        SELECT
            rs.setno,
            rs.ctrlid,
            rls.targid AS targid,
            rs.srcid
        FROM public."sets" rs
        JOIN public.overlap ro  ON ro.targid  = rs.targid
        JOIN public."sets"  rls ON rls.ctrlid = ro.ctrlid
        INNER JOIN rset ON rset.targid = rs.targid
) SELECT *
FROM rset
ORDER BY
    setno,
    ctrlid,
    targid
;
