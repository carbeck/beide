-- SELECT defines which columns to show and how to name them in the output.
-- Comment lines out to hide clutter you don't need for a given query.
--
SELECT
    sets1.setno             AS setno
    , sets1.set_id_parallel AS setpar
    , sets1.intermsc_var    AS setvar
    -- Controller 1+2
    , ctrl.ctrlid1          AS c_id1
    , ctrl.ctrlid2          AS c_id2
    , ctrl.loc1             AS c_loc1
    , ctrl.loc2             AS c_loc2
    , ctrl.schroeder1       AS c_schroeder1
    , ctrl.schroeder2       AS c_schroeder2
    , ctrl.text1            AS c_text1
    , ctrl.text2            AS c_text2
    , ctrl.sem1             AS c_sem1
    , ctrl.sem2             AS c_sem2
    , org_fgend.gend[1]     AS c_ogend1
    , org_fgend.gend[2]     AS c_ogend2
    , ctrl.form1            AS c_form1
    , ctrl.form2            AS c_form2
    , ctrl.pos1             AS c_pos1
    , ctrl.pos2             AS c_pos2
    , ctrl.flex1            AS c_flex1
    , ctrl.flex2            AS c_flex2
    , ctrl.elid1            AS c_elid1
    , ctrl.elid2            AS c_elid2
    , ctrl.orig1            AS c_orig1
    , ctrl.orig2            AS c_orig2
    , ctrl.comm1            AS c_comm1
    , ctrl.comm2            AS c_comm2
    -- Target
    , targ.id               AS t_id
    , targ.loc              AS t_loc
    , targ.schroeder        AS t_schroeder
    , targ."text"           AS t_text
    , targ.sem              AS t_sem
    , targ.form             AS t_form
    , targ.pos              AS t_pos
    , targ.flex             AS t_flex
    , targ.flex_elidable    AS t_elid
    , targ.floatquant       AS t_float
    , targ.intermed         AS t_intermed
    , targ.comm             AS t_comm
    -- Domain 1+2
    , dom1.dist_word        AS dist_word1
    , dom2.dist_word        AS dist_word2
    , dom1.dist_syn         AS dist_syn1
    , dom2.dist_syn         AS dist_syn2
    , dom1.ctorder          AS ctorder1
    , dom2.ctorder          AS ctorder2
    , dom1.agrrel           AS agrrel1
    , dom2.agrrel           AS agrrel2
    -- Location
    , meta.src
    , meta.place
    , meta.y
    , meta.x
    , meta.place_comment
    , meta.lang
    , meta."year"
    , meta."year_comment"
--
-- FROM and JOIN operations define which tables to get the data from.
-- Since our database is rather complex, this passage is rather lengthy.
-- You likely won't need to change things here.
--
FROM public."sets" sets1
--
-- Controller
--
JOIN (
    SELECT
        --
        -- Controller 1
        --
        ctrl1.id            AS ctrlid1,
        ctrl1.co_ctrlid     AS coctrlid1,
        ctrl1.loc           AS loc1,
        clines1.schroeder   AS schroeder1,
        ctrl1."text"        AS text1,
        concat_ws('.',
            fpers1.val,
            fnum1.val,
            gend1.val,
            case1.val
        )                   AS form1,
        concat_ws('.',
            spers1.val,
            snum1.val,
            sex1.val,
            anim1.val
        )                   AS sem1,
        pos1.val            AS pos1,
        ctrl1.flex_type     AS flex1,
        ctrl1.flex_elidable AS elid1,
        ctrl1.orig          AS orig1,
        ctrl1."comment"     AS comm1,
        --
        -- Controller 2
        --
        ctrl2.id            AS ctrlid2,
        ctrl2.co_ctrlid     AS coctrlid2,
        ctrl2.loc           AS loc2,
        clines2.schroeder   AS schroeder2,
        ctrl2."text"        AS text2,
        concat_ws('.',
            fpers2.val,
            fnum2.val,
            gend2.val,
            case2.val
        )                   AS form2,
        concat_ws('.',
            spers2.val,
            snum2.val,
            sex2.val,
            anim2.val
        )                   AS sem2,
        pos2.val            AS pos2,
        ctrl2.flex_type     AS flex2,
        ctrl2.flex_elidable AS elid2,
        ctrl2.orig          AS orig2,
        ctrl2."comment"     AS comm2
    FROM public.controller ctrl1
    --
    -- Join table on itself to get coordinated controller by ID
    -- We want to keep NULL values in case there's no second controller
    --
    LEFT JOIN controller ctrl2 ON ctrl1.co_ctrlid = ctrl2.id
    --
    -- Features belonging to controller 1 -- make IDs human-readable
    --
    LEFT JOIN features.pers   fpers1 ON ctrl1.form_pers = fpers1.id
    LEFT JOIN features.num    fnum1  ON ctrl1.form_num  = fnum1.id
    LEFT JOIN features.gend   gend1  ON ctrl1.form_gend = gend1.id
    LEFT JOIN features."case" case1  ON ctrl1.form_case = case1.id
    LEFT JOIN features.pers   spers1 ON ctrl1.sem_pers  = spers1.id
    LEFT JOIN features.sex    sex1   ON ctrl1.sem_sex   = sex1.id
    LEFT JOIN features.anim   anim1  ON ctrl1.sem_anim  = anim1.id
    LEFT JOIN features.num    snum1  ON ctrl1.sem_num   = snum1.id
    LEFT JOIN features.pos    pos1   ON ctrl1.pos       = pos1.id
    --
    -- Features belonging to controller 2 -- make IDs human-readable
    -- We want to keep NULL values in case there's no second controller
    --
    LEFT JOIN features.pers   fpers2 ON ctrl2.form_pers = fpers2.id
    LEFT JOIN features.num    fnum2  ON ctrl2.form_num  = fnum2.id
    LEFT JOIN features.gend   gend2  ON ctrl2.form_gend = gend2.id
    LEFT JOIN features."case" case2  ON ctrl2.form_case = case2.id
    LEFT JOIN features.pers   spers2 ON ctrl2.sem_pers  = spers2.id
    LEFT JOIN features.sex    sex2   ON ctrl2.sem_sex   = sex2.id
    LEFT JOIN features.anim   anim2  ON ctrl2.sem_anim  = anim2.id
    LEFT JOIN features.num    snum2  ON ctrl2.sem_num   = snum2.id
    LEFT JOIN features.pos    pos2   ON ctrl2.pos       = pos2.id
    --
    -- Join lines for Schroeder verse number
    --
    LEFT JOIN sources.lines clines1 ON ctrl1.loc = clines1.loc
    LEFT JOIN sources.lines clines2 ON ctrl2.loc = clines2.loc
    --
    -- Check to not include AB/BA duplicates
    --
    WHERE
           ctrl1.id < ctrl2.id
        OR ctrl2.id IS NULL
) AS ctrl ON ctrl.ctrlid1 = sets1.ctrlid
--
-- Target
--
JOIN (
    SELECT
        target.id,
        target.loc,
        tlines.schroeder,
        target."text",
        concat_ws('.',
            fpers.val,
            fnum.val,
            gend.val,
            "case".val
        )                AS form,
        concat_ws('.',
            spers.val,
            snum.val,
            sex.val,
            anim.val
        )                AS sem,
        pos.val          AS pos,
        target.flex_type AS flex,
        target.flex_elidable,
        target.floatquant,
        target.intermed,
        target."comment" AS comm
    FROM public.target target
    --
    -- Join target features -- make IDs human-readable
    --
    LEFT JOIN features.pers   fpers  ON target.form_pers = fpers.id
    LEFT JOIN features.num    fnum   ON target.form_num  = fnum.id
    LEFT JOIN features.gend   gend   ON target.form_gend = gend.id
    LEFT JOIN features."case" "case" ON target.form_case = "case".id
    LEFT JOIN features.pers   spers  ON target.sem_pers  = spers.id
    LEFT JOIN features.sex    sex    ON target.sem_sex   = sex.id
    LEFT JOIN features.anim   anim   ON target.sem_anim  = anim.id
    LEFT JOIN features.num    snum   ON target.sem_num   = snum.id
    LEFT JOIN features.pos    pos    ON target.pos       = pos.id
    --
    -- Join lines for Schroeder verse number
    --
    LEFT JOIN sources.lines tlines ON target.loc = tlines.loc
) AS targ ON targ.id = sets1.targid
--
-- Information on grammatical gender of original controller(s)
--
LEFT JOIN (
	SELECT
	    s.setno,
	    ARRAY_AGG(gend.val) AS gend
	FROM sets_firstctrl_lasttarg s
	JOIN public.controller  c    ON c.id        = s.ctrlid_first
	LEFT JOIN features.gend gend ON c.form_gend = gend.id
	GROUP BY s.setno
) AS org_fgend ON org_fgend.setno = sets1.setno
--
-- Domain of Controller 1
--
JOIN (
    SELECT
        "domain".id,
        "domain".dist_word,
        features.distsyn.val AS dist_syn,
        features.ctorder.val AS ctorder,
        features.agrrel.val  AS agrrel
    FROM public."domain" "domain"
    --
    -- Join domain features -- make IDs human-readable
    --
    LEFT JOIN features.ctorder ON "domain".ctorder  = features.ctorder.id
    LEFT JOIN features.distsyn ON "domain".dist_syn = features.distsyn.id
    LEFT JOIN features.agrrel  ON "domain".agrrel   = features.agrrel.id
) AS dom1 ON dom1.id = sets1.domid
--
-- Domain of Controller 2
--
LEFT JOIN public."sets" sets2 ON (
	    ctrl.coctrlid1 = sets2.ctrlid
	AND sets1.setno    = sets2.setno
)
LEFT JOIN(
    SELECT
        "domain".id,
        "domain".dist_word,
        features.distsyn.val AS dist_syn,
        features.ctorder.val AS ctorder,
        features.agrrel.val  AS agrrel
    FROM public."domain" "domain"
    --
    -- Join domain features -- make IDs human-readable
    --
    LEFT JOIN features.ctorder ON "domain".ctorder  = features.ctorder.id
    LEFT JOIN features.distsyn ON "domain".dist_syn = features.distsyn.id
    LEFT JOIN features.agrrel  ON "domain".agrrel   = features.agrrel.id
) AS dom2 ON dom2.id = sets2.domid
--
-- Meta information
--
JOIN (
    SELECT
        sources.meta.id,
        src,
        place_name               AS place,
        place_y                  AS y,
        place_x                  AS x,
        sources.places."comment" AS "place_comment",
        lang                     AS lang,
        year_from                AS "year",
        sources.meta."comment"   AS "year_comment"
    FROM sources.meta
    --
    -- Join meta features -- make IDs human-readable
    --
    JOIN sources.places ON sources.places.id = sources.meta.plcid
) AS meta ON meta.id = sets1.srcid
--
-- WHERE limits the data retreived from the DB to some condition(s). Adapt
-- accordingly.
--
-- In this particular example, we want to filter out targets marked as
-- "intermediate" (i.e. those that become themselves controllers) to only
-- capture targets which are 'beide/-iu'. We also filter out uninflected
-- targets.
--
WHERE
    targ.intermed = FALSE
    AND targ.flex NOTNULL
    AND sets1.active = TRUE
    AND meta.src LIKE 'KC-%'
--
-- ORDER BY defines the columns according to which data is sorted in the
-- output. Adapt accordingly.
--
ORDER BY
    meta.src
    , ctrl.ctrlid1
    , targ.id
    , ctrl.loc1
    , targ.loc
;
