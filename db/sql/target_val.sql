CREATE OR REPLACE VIEW target_val AS
SELECT
    targ.id,
    targ."text",
    targ.linid,
    targ.loc,
    fpers.val  AS form_pers,
    fnum.val   AS form_num,
    gend.val   AS form_gend,
    "case".val AS form_case,
    spers.val  AS sem_pers,
    snum.val   AS sem_num,
    sex.val    AS sem_sex,
    anim.val   AS sem_anim,
    pos.val    AS pos,
    targ.flex_type,
    targ.flex_rhyme,
    targ.flex_elidable,
    targ.floatquant,
    targ.intermed,
    targ."comment"
FROM public.target targ
LEFT JOIN features.pers   fpers ON form_pers = fpers.id
LEFT JOIN features.num    fnum  ON form_num  = fnum.id
LEFT JOIN features.gend   gend  ON form_gend = gend.id
LEFT JOIN features."case" "case"ON form_case = "case".id
LEFT JOIN features.pers   spers ON sem_pers  = spers.id
LEFT JOIN features.sex    sex   ON sem_sex   = sex.id
LEFT JOIN features.anim   anim  ON sem_anim  = anim.id
LEFT JOIN features.num    snum  ON sem_num   = snum.id
LEFT JOIN features.pos    pos   ON pos       = pos.id
;
