CREATE OR REPLACE VIEW domain_val AS
SELECT
    dom.id,
    ctor.val AS ctorder,
    dom.dist_word,
    dsyn.val AS dist_syn,
    agrl.val AS agrrel
FROM public."domain" dom
LEFT JOIN features.ctorder ctor ON dom.ctorder   = ctor.id
LEFT JOIN features.distsyn dsyn ON dom.dist_syn  = dsyn.id
LEFT JOIN features.agrrel  agrl ON dom.agrrel    = agrl.id
;
