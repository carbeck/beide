-- Display the rough numbers (ignoring precedence between controller and target)
-- for the distances between two controlllers and their target
--
SELECT DISTINCT
	s.setno
	, dw2.xid       AS c1_id
	, dw1.xid       AS c2_id
	, c1."text"     AS c1_text
	, c2."text"     AS c2_text
	, dw1.dist_word AS dist_word1
	, dw2.dist_word AS dist_word2
	, dw1.dist_syn  AS dist_syn1
	, dw2.dist_syn  AS dist_syn2
	, dw1.tids_text
	, dw1.tid_last
	, t."text"      AS t_text
	, t.flex_type   AS t_flex_type
	, m.src
FROM public."sets" AS s
--
-- Rough distance from first controller to last target
-- (Doesn't take precedence into account)
--
JOIN (
	SELECT
		s.setno
		, sum(d.dist_word)                      AS dist_word
		, max(d.dist_syn)                       AS dist_syn
		, array_agg(s.ctrlid ORDER BY s.ctrlid) AS cids
		, array_agg(s.targid ORDER BY s.targid) AS tids
		, array_agg(t."text" ORDER BY s.targid) AS tids_text
		, x.co_ctrlid                           AS xid
		, max(targid)                           AS tid_last
	FROM public."sets" AS s
	JOIN public.target_val AS t ON t.id = s.targid
	JOIN public.domain_val AS d ON d.id = s.domid
	--
	-- List of co-controllers per set to exclude from calculation
	--
	JOIN (
		SELECT
			s.setno
			, max(c.co_ctrlid) AS co_ctrlid
		FROM public."sets" AS s
		JOIN public.controller AS c ON c.id = s.ctrlid
		WHERE c.co_ctrlid NOTNULL
		GROUP BY s.setno
	) AS x ON x.setno = s.setno
	WHERE s.ctrlid != x.co_ctrlid
	GROUP BY
		s.setno
		, x.co_ctrlid
	ORDER BY s.setno
) AS dw1 ON dw1.setno = s.setno
--
-- Rough distance from second controller to last target
-- (Doesn't take precedence into account)
--
JOIN (
	SELECT
		s.setno
		, sum(d.dist_word)                      AS dist_word
		, max(d.dist_syn)                       AS dist_syn
		, array_agg(s.ctrlid ORDER BY s.ctrlid) AS cids
		, array_agg(s.targid ORDER BY s.targid) AS tids
		, array_agg(t."text" ORDER BY s.targid) AS tids_text
		, x.co_ctrlid                           AS xid
		, max(targid)                           AS tid_last
	FROM public."sets" AS s
	JOIN public.target_val AS t ON t.id = s.targid
	JOIN public.domain_val AS d ON d.id = s.domid
	--
	-- List of co-controllers per set to exclude from calculation
	--
	JOIN (
		SELECT
			s.setno
			, min(c.co_ctrlid) AS co_ctrlid
		FROM public."sets" AS s
		JOIN public.controller AS c ON c.id = s.ctrlid
		WHERE c.co_ctrlid NOTNULL
		GROUP BY s.setno
	) AS x ON x.setno = s.setno
	WHERE s.ctrlid != x.co_ctrlid
	GROUP BY
		s.setno
		, x.co_ctrlid
	ORDER BY s.setno
) AS dw2 ON dw2.setno = s.setno
-- Join information on ctrlid1 (= dw2's excluded co-controller)
JOIN public.controller_val AS c1 ON c1.id = dw2.xid
-- Join information on ctrlid2 (= dw1's excluded co-controller)
JOIN public.controller_val AS c2 ON c2.id = dw1.xid
-- Join information on last target (same for both dw1 and dw2)
JOIN public.target_val     AS t  ON t.id = dw1.tid_last
-- Join source info
JOIN sources.meta          AS m  ON m.id = s.srcid
WHERE
	s.active = TRUE
	AND m.src LIKE 'KC%'
	AND s.setno < 2000
	AND c1.sem_anim = 'A'
	AND (
		   (c1.sem_sex = 'M' AND c2.sem_sex = 'F')
		OR (c1.sem_sex = 'F' AND c2.sem_sex = 'M')
	)
ORDER BY
	dist_syn1
	, dist_syn2
	, dist_word1
	, dist_word2
;
