--
-- Give a count of records for 'beide/-iu' per place
--
SELECT
    p.place_name,
    ARRAY_AGG(t.loc),
    COUNT(t.id),
    p.place_y,
    p.place_x
FROM public.target t
--
-- Join place information per line in the target table
--
JOIN sources.lines  l ON l.id = t.linid
JOIN sources.meta   m ON m.id = l.srcid
JOIN sources.places p ON p.id = m.plcid
--
-- We only want to count records with 'beide/-iu'
--
WHERE t.intermed = FALSE
GROUP BY
    p.place_name,
    p.place_y,
    p.place_x
ORDER BY p.place_name
;
