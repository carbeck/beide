SELECT
    -- Controller 1+2
    setno
    , set_id_parallel    AS setpar
    , intermsc_var       AS setvar
    , ctrl1.ctrlid1      AS c_id1
    , ctrl2.ctrlid2      AS c_id2
    , ctrl1.loc1         AS c_loc1
    , ctrl2.loc2         AS c_loc2
    , ctrl1.schroeder    AS c_schroeder1
    , ctrl2.schroeder    AS c_schroeder2
    , ctrl1.text1        AS c_text1
    , ctrl2.text2        AS c_text2
    , ctrl1.sem1         AS c_sem1
    , ctrl2.sem2         AS c_sem2
    , ctrl1.form1        AS c_form1
    , ctrl2.form2        AS c_form2
    , ctrl1.pos1         AS c_pos1
    , ctrl2.pos2         AS c_pos2
    , ctrl1.flex1        AS c_flex1
    , ctrl2.flex2        AS c_flex2
    , ctrl1.elid1        AS c_elid1
    , ctrl2.elid2        AS c_elid2
    , ctrl1.orig1        AS c_orig1
    , ctrl2.orig2        AS c_orig2
    , ctrl1.comm1        AS c_comm1
    , ctrl2.comm2        AS c_comm2
    , targint
    -- Target
    , targ.id            AS t_id
    , targ.loc           AS t_loc
    , targ.schroeder     AS t_schroeder
    , targ."text"        AS t_text
    , targ.sem           AS t_sem
    , targ.form          AS t_form
    , targ.pos           AS t_pos
    , targ.flex          AS t_flex
    , targ.flex_elidable AS t_elid
    , targ.floatquant    AS t_float
    , targ.intermed      AS t_intermed
    , targ.comm          AS t_comm
    -- Domain
--  , dom.dist_word
--  , dom.dist_syn
--  , dom.ctorder
--  , dom.agrrel
    -- Location
    , meta.src
    , meta.place
    , meta.y
    , meta.x
    , meta.place_comment
    , meta.lang
    , meta."year"
    , meta."year_comment"
--
-- FROM and JOIN operations define which tables to get the data from.
-- Since our database is rather complex, this passage is rather lengthy.
-- You likely won't need to change things here.
--
FROM (
    --
    -- Table containing setno, first ctrlid, intermediary targets (as text),
    -- last targid, and scrcid in a set.
    --
    -- Wrapping this in a subquery so we don't have to awkwardly juggle
    -- with including each and every thing joined on in the GROUP BY clause.
    --
    SELECT DISTINCT
        s.setno,
        s.set_id_parallel,
        s.intermsc_var,
        s.ctrlid_first      AS ctrlid1,
        scopy.ctrlid_first  AS ctrlid2,
        array_agg(t."text") AS targint,
        s.targid_last,
        s.srcid
    FROM sets_firstctrl_lasttarg s
    LEFT JOIN (
        SELECT
            s.setno,
            MAX(s.ctrlid_first) AS ctrlid_first
        FROM sets_firstctrl_lasttarg s
        GROUP BY s.setno
        HAVING COUNT(ctrlid_first) >= 2
    ) AS scopy ON scopy.setno = s.setno
    JOIN public.target t ON t.id = ANY(s.targid_intermed)
    WHERE
         s.ctrlid_first < scopy.ctrlid_first
         OR scopy.ctrlid_first IS NULL
    GROUP BY
            s.setno,
            s.set_id_parallel,
            s.intermsc_var,
            s.targid_intermed,
            s.ctrlid_first,
            scopy.ctrlid_first,
            s.targid_last,
            s.srcid
    ORDER BY
        s.setno,
        s.ctrlid_first,
        s.targid_last
) AS sfl
--
-- Controller 1
--
LEFT JOIN (
    SELECT
        ctrl1.id            AS ctrlid1,
        ctrl1.co_ctrlid     AS coctrlid1,
        ctrl1.loc           AS loc1,
        clines1.schroeder   AS schroeder,
        ctrl1."text"        AS text1,
        concat_ws('.',
            fpers1.val,
            fnum1.val,
            gend1.val,
            case1.val
        )                   AS form1,
        concat_ws('.',
            spers1.val,
            sex1.val,
            anim1.val,
            snum1.val
        )                   AS sem1,
        pos1.val            AS pos1,
        ctrl1.flex_type     AS flex1,
        ctrl1.flex_elidable AS elid1,
        ctrl1.orig          AS orig1,
        ctrl1."comment"     AS comm1
    FROM public.controller ctrl1
    --
    -- Features belonging to controller 1 -- make IDs human-readable
    --
    LEFT JOIN features.pers   fpers1 ON ctrl1.form_pers = fpers1.id
    LEFT JOIN features.num    fnum1  ON ctrl1.form_num  = fnum1.id
    LEFT JOIN features.gend   gend1  ON ctrl1.form_gend = gend1.id
    LEFT JOIN features."case" case1  ON ctrl1.form_case = case1.id
    LEFT JOIN features.pers   spers1 ON ctrl1.sem_pers  = spers1.id
    LEFT JOIN features.sex    sex1   ON ctrl1.sem_sex   = sex1.id
    LEFT JOIN features.anim   anim1  ON ctrl1.sem_anim  = anim1.id
    LEFT JOIN features.num    snum1  ON ctrl1.sem_num   = snum1.id
    LEFT JOIN features.pos    pos1   ON ctrl1.pos       = pos1.id
    --
    -- Join lines for Schroeder verse number
    --
    LEFT JOIN sources.lines clines1 ON ctrl1.loc = clines1.loc
) AS ctrl1 ON ctrl1.ctrlid1 = sfl.ctrlid1
--
-- Controller 2
--
LEFT JOIN (
    SELECT
        ctrl2.id            AS ctrlid2,
        ctrl2.co_ctrlid     AS coctrlid2,
        ctrl2.loc           AS loc2,
        clines2.schroeder   AS schroeder,
        ctrl2."text"        AS text2,
        concat_ws('.',
            fpers2.val,
            fnum2.val,
            gend2.val,
            case2.val
        )                   AS form2,
        concat_ws('.',
            spers2.val,
            sex2.val,
            anim2.val,
            snum2.val
        )                   AS sem2,
        pos2.val            AS pos2,
        ctrl2.flex_type     AS flex2,
        ctrl2.flex_elidable AS elid2,
        ctrl2.orig          AS orig2,
        ctrl2."comment"     AS comm2
    FROM public.controller ctrl2
    --
    -- Features belonging to controller 2 -- make IDs human-readable
    --
    LEFT JOIN features.pers   fpers2 ON ctrl2.form_pers = fpers2.id
    LEFT JOIN features.num    fnum2  ON ctrl2.form_num  = fnum2.id
    LEFT JOIN features.gend   gend2  ON ctrl2.form_gend = gend2.id
    LEFT JOIN features."case" case2  ON ctrl2.form_case = case2.id
    LEFT JOIN features.pers   spers2 ON ctrl2.sem_pers  = spers2.id
    LEFT JOIN features.sex    sex2   ON ctrl2.sem_sex   = sex2.id
    LEFT JOIN features.anim   anim2  ON ctrl2.sem_anim  = anim2.id
    LEFT JOIN features.num    snum2  ON ctrl2.sem_num   = snum2.id
    LEFT JOIN features.pos    pos2   ON ctrl2.pos       = pos2.id
    --
    -- Join lines for Schroeder verse number
    --
    LEFT JOIN sources.lines clines2 ON ctrl2.loc = clines2.loc
) AS ctrl2 ON ctrl2.ctrlid2 = sfl.ctrlid2
--
-- Target
--
JOIN (
    SELECT
        target.id,
        target.loc,
        tlines.schroeder,
        target."text",
        concat_ws('.',
            fpers.val,
            fnum.val,
            gend.val,
            "case".val
        )                AS form,
        concat_ws('.',
            spers.val,
            sex.val,
            anim.val,
            snum.val
        )                AS sem,
        pos.val          AS pos,
        target.flex_type AS flex,
        target.flex_elidable,
        target.floatquant,
        target.intermed,
        target."comment" AS comm
    FROM public.target target
    --
    -- Join target features -- make IDs human-readable
    --
    LEFT JOIN features.pers   fpers  ON target.form_pers = fpers.id
    LEFT JOIN features.num    fnum   ON target.form_num  = fnum.id
    LEFT JOIN features.gend   gend   ON target.form_gend = gend.id
    LEFT JOIN features."case" "case" ON target.form_case = "case".id
    LEFT JOIN features.pers   spers  ON target.sem_pers  = spers.id
    LEFT JOIN features.sex    sex    ON target.sem_sex   = sex.id
    LEFT JOIN features.anim   anim   ON target.sem_anim  = anim.id
    LEFT JOIN features.num    snum   ON target.sem_num   = snum.id
    LEFT JOIN features.pos    pos    ON target.pos       = pos.id
    --
    -- Join lines for Schroeder verse number
    --
    LEFT JOIN sources.lines tlines ON target.loc = tlines.loc
) AS targ ON targ.id = sfl.targid_last
--
-- Meta information
--
JOIN (
    SELECT
        sm.id,
        src,
        lang,
        place_name   AS place,
        year_from    AS "year",
        place_y      AS y,
        place_x      AS x,
        sp."comment" AS place_comment,
        sm."comment" AS year_comment
    FROM sources.meta sm
    --
    -- Join meta features -- make IDs human-readable
    --
    JOIN sources.places sp ON sp.id = sm.plcid
) AS meta ON meta.id = sfl.srcid
--
-- WHERE limits the data retreived from the DB to some condition(s). Adapt
-- accordingly.
--
-- In this particular example, we want to filter out targets marked as
-- "intermediate" (i.e. those that become themselves controllers) to only
-- capture targets which are 'beide/-iu'. We also filter out uninflected
-- targets.
--
WHERE
    targ.intermed = FALSE
    AND targ.flex NOTNULL
    AND meta.src LIKE 'KC-%'
--
-- ORDER BY defines the columns according to which data is sorted in the
-- output. Adapt accordingly.
--
ORDER BY
    c_id1
    , c_id2
    , t_id
;
