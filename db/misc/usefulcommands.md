Useful commands
===============

## Create DB dump

    $ pg_dump --no-owner --no-privileges --create beide | gzip > beide-YYYYMMDD.sql.gz

## Recreate DB from dump

    $ dropdb beide && createdb beide
    $ gunzip < beide-YYYYMMDD.sql.gz | psql beide
