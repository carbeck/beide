#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''Extract relevant sections of verses from master text source file for further
   processing -- we don't want all 1M+ lines in the database later.

   Carsten Becker, 2020
   <carsten [dot] becker [at] uni [dash] marburg [dot] de>
'''

import bisect
import csv
import os
import os.path
from pyexcel_ods import get_data   # Requires pyexcel_ods from PyPI
import re

# FUNCTION DEFINITIONS =========================================================

# Turn empty string into zero (int)
def strtozero(s=''):
    if s == '':
        return 0
    else:
        return s

# Turn zero to None
def zerotonone(i=''):
    if i == 0:
        return None
    else:
        return i

# Add the proper source designation
def makesource(fileid=''):
    if   re.match('stav_ms276',        fileid): return 'A_A1-stav_ms276'
    elif re.match('onb_cod_2779',      fileid): return 'B_B1-onb_cod_2779'
    elif re.match('onb_cod_2685',      fileid): return 'C_C1-onb_cod_2685'
    elif re.match('cpg361',            fileid): return 'A_H-cpg361'
    elif re.match('blb_augpap52',      fileid): return 'C_K-blb_augpap52'
    elif re.match('bsb_cgm37',         fileid): return 'A_M-bsb_cgm37'
    elif re.match('tnb_codxxiii_g_43', fileid): return 'B_P-tnb_codxxiii_g_43'
    elif re.match('onb_cod_2693',      fileid): return 'B_VB-onb_cod_2693'
    elif re.match('wzga_zams30',       fileid): return 'C_Z-wzga_zams30'

# Check if a line is already included in lines. If not so, format for inclusion.
def prepareline(line=dict(), schroeder=None):
    if line not in lines:
        return {
            'src':       makesource(line['fileid']),
            'loc':       line['lineid'],
            'text':      line['line'],
            'schroeder': schroeder,
        }

# Make unique list (should also work for non-hashable types)
# https://stackoverflow.com/a/24085464/
def make_unique(original_list):
    unique_list = []
    [unique_list.append(obj) for obj in original_list if obj not in unique_list]
    return unique_list

# MAIN PROGRAM =================================================================

# Read in ODS table containing annotations and stuff (we only want stuff from
# 'Tabelle1' here)
rawdata = get_data('beide_kc-20200302.ods')['Tabelle1']

# get_data() reads the lines into a plain old list. That's meh to handle, so we
# want the list as a dictionary with rawdata[0] as the list of keys, so ---

# Instantiate the variables to carry the data
data = []
keys = rawdata[0]
values = [x for x in rawdata[1:] if x != []]

# Zip the keys and values list together
for v in values:
    data.append(dict(zip(keys, v)))

# Now process the stuff according to the big file containing all the text lines
msclines = []

# Read file contents into a list of dictionaries for further processing.
# Doing list operations on the CSV file object on the fly doesn't seem to work.
with open('ausschnitte.csv', 'r') as csvin:
    reader = csv.DictReader(csvin)
    for row in reader:
        msclines.append(row)

# Iterate through rows in data. We don't need to process all the rows of the
# text source, of course, but only those between data['c_loc'] to
# data['t_loc'] + 5 before and after for context, for good measure. We also
# don't want duplicates.
#
# NOTE: The line IDs in the XML data from 'Kaiserchronik Digital' don't pad
# single-digit line numbers with zeroes, so you get 'l_X_123r-a.line_9'
# followed by 'l_X_123r-a.line_10'. Since this is not very
# programming-friendly, I took the liberty to insert those zeroes for better
# string sortability and to remove the superfluous 'l_': 'l_X_123r-a.line_9'
# is 'X_123r-a.line_09' now.
lines = []
window = 5
    
# List of the indexes of start and end points
points = []

for d in data:

    # Take 'c_loc' and 't_loc' since 't_loc' and 'c_loc' may be in any order
    # (there are a few targets preceding their controller)
    start, end = sorted([d['c_loc'], d['t_loc']])

    # Scan through lines of the text source
    for i, r in enumerate(msclines):
        
        # Find the i for start
        if r['lineid'] == start:
            # For every found start point, reinitialize the container
            container = []

            # If a starting lineid has been matched, add it to the container
            container.append(i)
        
        # Find the i for end
        if r['lineid'] == end:

            # If an ending lineid has been matched, add it to the container
            container.append(i)

            # Append the pair of start/end points to the overall list of points
            points.append(container)

            # Prolong the life of the universe by saving some entropy
            continue

# Copy lines into list
for start, end in points:

    # Lines before start
    for line in msclines[start - window : start]:
        lines.append(prepareline(line))

    # Start
    lines.append(prepareline(msclines[start]))

    # Lines between start and end
    for line in msclines[start + 1 : end]:
        lines.append(prepareline(line))

    # End
    lines.append(prepareline(msclines[end]))

    # Lines after end
    for line in msclines[end + 1 : end + window + 1]:
        lines.append(prepareline(line))

# Sort, and filter duplicates
lines = make_unique(sorted(lines, key=lambda i: i['loc']))

# Deal with verse numbers according to Schroeder (1895) Problem: For those
# manuscripts with continuous lines, multiple verse numbers may apply to the
# same line

# Make a list of all line ids and corresponding schroeder numbers
schroeder = []
for row in data:
    if row['c_schroeder']:
        schroeder.append({
            'loc':   row['c_loc'],
            'schroeder': row['c_schroeder'],
        })

    if row['t_schroeder']:
        schroeder.append({
            'loc':   row['t_loc'],
            'schroeder': row['t_schroeder'],
        })

# Delete duplicate entries
schroeder = make_unique(sorted(schroeder, key=lambda i: i['loc']))

# Create an alias of lines as a dict where the line id is the key, the line
# itself the value. This is for easy matching lines between both lists.
dictlines = {l['loc']: l for l in lines}

# Loop through schroeder and update dictlines (and thereby lines)
for s in schroeder:

    # If there's no value in the field, set one as int
    if dictlines[s['loc']]['schroeder'] == None:
        dictlines[s['loc']]['schroeder'] = s['schroeder']

    # If there's a value, convert to a list and add the other value to it
    elif dictlines[s['loc']]['schroeder'] != None:
        dictlines[s['loc']]['schroeder'] = [dictlines[s['loc']]['schroeder']]
        bisect.insort(dictlines[s['loc']]['schroeder'], s['schroeder'])

# Write the contents of lines to file
with open('kc_beide.csv', 'w') as csvout:
    writer = csv.DictWriter(csvout, fieldnames=lines[0].keys(), dialect='unix')

    writer.writeheader()
    for line in lines:
        writer.writerow(line)
