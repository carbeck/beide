#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''Export deed texts with their meta data from preexisting CAO DB as JSON
   files to use with the `beide` project.

   Carsten Becker, 2019
   <carsten [dot] becker [at] uni [dash] marburg [dot] de>
'''

import credentials
import json
import os
import os.path
import psycopg2
import psycopg2.extras
import re
import zipfile

# Numbers of deeds which contain examples
deednos = [
    "31", "86", "85", "83", "76", "75", "71", "260", "701", "632", "627", "604",
    "429", "369", "3536", "2353", "2375", "2406", "2412", "2468", "2532",
    "2713", "2862", "2866", "2913", "2925", "3049", "3062", "3104", "3130",
    "3147", "3150", "3319", "3332", "3339", "3346", "3397", "3428", "3451",
    "69-A", "72-B", "1076", "1137", "1153", "1217", "1352", "1545", "1566",
    "1568", "1578", "1620", "1758", "1764", "1802", "1831", "1971", "2293",
    "2307", "1126-A", "2735-B", "2625-B", "1972-A", "2625-A", "2735-A",
    "1126-B", "N-11-59-c", "N-14-61-a", "N-68-85-B", "N-36-71-B", "N-99-143-a",
    "N-2-A-54-b", "N-2-B-54-b", "N-220-550-a", "N-321-853-a", "N-328-905-a",
    "N-766-2396-a", "N-377-1043-a", "N-463-1297-a", "N-689-2123-c",
    "N-709-2163-b", "N-748-2315-a", "N-752-2320-b",
]

# Establish DB connection
try:
    conn = psycopg2.connect(
        host=credentials.HOST, 
        dbname="cao",
        user=credentials.USER,
        password=credentials.PASS
    )
    
except psycopg2.OperationalError as e:
    raise e
    
# Establishing DB cursor
cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

# Automatically commit things
conn.autocommit = True

# Prepare to write data into 'data/cao.zip' at the end
with zipfile.ZipFile(os.path.join('data','cao_beideunde.zip'), mode='w',
    compression=zipfile.ZIP_DEFLATED) as zip:

    # Loop over deed numbers in `deedno`
    for n in deednos:
        # FIRST, get the line numbers and text from the DB
        query = """SELECT row AS loc, text FROM lines WHERE deedno_cao_long = %s 
                ORDER BY page, line ASC"""

        # Execute query
        print(cur.mogrify(re.sub(r'\s+',' ', query), (n,)))
        cur.execute(query, (n,))

        # Fetch all the results
        deedtext = []
        for row in cur.fetchall():
            deedtext.append(dict(row))

        # SECOND, get the meta data
        query = """
            SELECT meta_cao.deedno_cao_long AS deedno_long,
                meta_cao.deedno_trier AS deedno_trier,
                meta_cao.date_from,
                meta_cao.date_to,
                meta_cao.date_note,
                places.id AS place_id_caodb,
                places.id_place_trier AS place_id_trier,
                places.place,
                places.x,
                places.y,
                CONCAT('http://tcdh01.uni-trier.de/cgi-bin/iCorpus/CorpusIndex.tcl?hea=qf&for=qfcoraltdu&cnt=qfcoraltdu&xid=', meta_cao.deedno_trier) AS link_trier
            FROM meta_cao, (
                SELECT id_place_trier
                FROM meta_cao
                INNER JOIN meta_places
                    ON meta_cao.deedno_cao_long = meta_places.deedno_cao_long
                WHERE meta_cao.deedno_cao_long = %(n)s
            ) AS placeids
            JOIN places ON placeids.id_place_trier = places.id_place_trier
            WHERE meta_cao.deedno_cao_long = %(n)s
        """

        # Execute query
        print(cur.mogrify(re.sub(r'\s+',' ', query),{'n' : n}))
        cur.execute(query, {'n' : n})

        # Fetch all the results
        deedmeta = []
        for row in cur.fetchall():
            deedmeta.append(dict(row))

        # Make dates strings; JSON can't deal with datetime objects
        for i in deedmeta:
            i["date_from"] = {
                'ymd' : str(i["date_from"]),
                'year' : str(i["date_from"]).split('-')[0],
                'month' : str(i["date_from"]).split('-')[1],
                'day' : str(i["date_from"]).split('-')[2]
            }
            i["date_to"] = {
                'ymd' : str(i["date_to"]),
                'year' : str(i["date_to"]).split('-')[0],
                'month' : str(i["date_to"]).split('-')[1],
                'day' : str(i["date_to"]).split('-')[2]
            }

        # THIRD, unify the information We're only dealing with deeds which have
        # a single place attached to them, so it's safe to assume that
        # `deedmeta` doesn't need to be embedded in a list the way it's returned
        # form the query.
        data = {'lines' : deedtext, 'meta' : deedmeta[0]}

        # Cast into JSON format (pretty-printed for human readability)
        jsondata = '{}\n'.format(json.dumps(data, indent=4, sort_keys=True))

        # Write into ZIP file
        zip.writestr('cao_{}.json'.format(n), jsondata)

# Close cursor
cur.close()

# Close DB connection
conn.close()
