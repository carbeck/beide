#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''Copy operational data into the database.

   Carsten Becker, 2019
   <carsten [dot] becker [at] uni [dash] marburg [dot] de>
'''

import csv
import json
import os
import os.path
import psycopg2
import credentials
import zipfile

# FUNCTION DEFINITIONS =========================================================

# Function getplcidbycoord()
def getplcidbycoord(x=0, y=0):
    '''Gets place ID from database based on coordinates provided.
    
        getplcidbycoord(x [float], y [float]) => [int]
    '''

    # Instantiate new DB cursor
    cur_getplcidbycoord = conn.cursor()

    # Query to select the place ID from the places table
    query = '''
        SELECT id
        FROM sources.places
        WHERE round(place_x, 4) = round({}, 4)
        AND round(place_y, 4) = round({}, 4)
    '''.format(x, y)

    # Execute query
    try:
        cur_getplcidbycoord.execute(query)
    except psycopg2.DatabaseError as e:
        raise e

    # Get result, return it
    try:
        return cur_getplcidbycoord.fetchone()[0]
    except:
        return None

    # Close cursor
    cur_getplcidbycoord.close()

# Function getlinidbyloc()
def getlinidbyloc(loc=''):
    '''Gets line ID from database based on location provided
    
        getlinidbyloc(loc [str]) => [int]
    '''

    # Instantiate new DB cursor
    cur_getlinidbyloc = conn.cursor()

    # Query to select the line ID from the sources table
    query = """SELECT id FROM sources.lines WHERE loc = '{}'""".format(loc)

    # Execute query
    try:
        cur_getlinidbyloc.execute(query)
    except psycopg2.DatabaseError as e:
        raise e

    # Get result, return it
    try:
        return cur_getlinidbyloc.fetchone()[0]
    except:
        return None

    # Close cursor
    cur_getlinidbyloc.close()

# Function getfeatidbyval()
def getfeatidbyval(val='', tbl=''):
    '''Matches the provided value with the values in the provided table and
    returns that line's index.
    
        getfeatidbyval(val [str], tbl [str]) => [int]
    '''

    # Instantiate new DB cursor
    cur_getfeatidbyval = conn.cursor()

    # Query to select the feature's ID from the provided table
    query = """SELECT id FROM features.{} WHERE val = '{}'""".format(
        tbl, val)

    # Execute query
    try:
        cur_getfeatidbyval.execute(query)
    except psycopg2.DatabaseError as e:
        raise e

    # Get result, return it
    try:
        return cur_getfeatidbyval.fetchone()[0]
    except:
        return None

    # Close cursor
    cur_getfeatidbyval.close()

# Function makebool()
def makebool(val=''):
    '''Empty cells in CSV files are an empty string. This becomes False when
    casting as boolean, but we'd rather have that be None (= NULL in SQL).
    
        makebool(val [int]) => [bool]
    '''

    if type(val) == int and val in [0, 1]:
        return bool(val)

# MAIN PROGRAM =================================================================

# Open a database connection 
try:
    conn = psycopg2.connect(
        dbname = credentials.DB,
        user = credentials.USER,
        host = credentials.HOST,
        password = credentials.PASS
        )
except psycopg2.DatabaseError as e:
    raise e

# Instantiate DB cursor
cur = conn.cursor()

# Automatically commit things
conn.autocommit = True

# FIRST insert geographical data on CAO deeds from `data/places_cao_beideunde.csv`
# (a.k.a. the low-hanging fruit)
#
# {# Table 'sources.places'
#     'schema' : 'sources',
#     'name' : 'places',
#     'pkey' : 'id',
#     'fields' : [
#         ('place_name', 'varchar'),
#         ('place_x', 'numeric'),
#         ('place_y', 'numeric'),
#         ('comment', 'varchar')
#     ],
# },
with open(os.path.join('data', 'places_cao_beideunde.csv'), 'r') as csvfile:

    # Read CSV file into dictionary (OrderedDict, actually)
    reader = csv.DictReader(csvfile)

    # Loop over rows in CSV file
    for row in reader:
        query = '''
            INSERT INTO sources.places (
                place_name,
                place_x,
                place_y,
                comment
            )
            VALUES (
                %(place_name)s,
                %(place_x)s,
                %(place_y)s,
                %(comment)s
            )
            '''

        # Execute query
        try:
            params = {
                'place_name' : row['place_name'],
                'place_x' : row['place_x'],
                'place_y' : row['place_y'],
                'comment' : row['comment'],
            }
            cur.execute(query, params)
        except psycopg2.DatabaseError as e:
            print(cur.mogrify(query, params))
            raise e

# SECOND insert text from CAO deeds in `data/cao_beideunde.zip`
# (a.k.a. the hard part, pt. 1)
#
# {# Table 'sources.meta'
#     'schema' : 'sources',
#     'name' : 'meta',
#     'pkey' : 'id',
#     'fields' : [
#         ('src', 'varchar'),
#         ('lang', 'varchar'),
#         ('year_from', 'integer'),
#         ('year_to', 'integer'),
#         ('plcid', 'integer', ('sources.places', 'id', '')),
#         ('comment', 'varchar')
#     ],
# },
#
# {# Table 'sources.lines'
#     'schema' : 'sources',
#     'name' : 'lines',
#     'pkey' : 'id',
#     'fields' : [
#         ('text', 'varchar'),
#         ('loc', 'varchar'),
#         ('srcid', 'integer', ('sources.meta', 'id', '')),
#     ],
# },
with zipfile.ZipFile(os.path.join('data', 'cao_beideunde.zip'), 'r') as zip:

    # Get the list of files included in the ZIP and loop over it
    for file in zip.namelist():

        # Open the current file for reading ...
        with zip.open(file, 'r') as f:

            # ... and parse its JSON contents
            data = json.load(f)

            # Query to insert meta values
            #
            # Since we need this value in the next step, the ID of the newly
            # inserted line is returned and can be fetched.
            query = '''
                INSERT INTO sources.meta (
                    src,
                    year_from,
                    year_to,
                    plcid,
                    comment
                )
                VALUES (
                    %(src)s,
                    %(year_from)s,
                    %(year_to)s,
                    %(plcid)s,
                    %(comment)s
                )
                RETURNING sources.meta.id
            '''

            # Execute query
            try:
                params = {
                    'src'       : 'CAO-{}'.format(data['meta']['deedno_long']),
                    'year_from' : data['meta']['date_from']['year'],
                    'year_to'   : data['meta']['date_to']['year'],
                    'plcid'     : getplcidbycoord(
                                    data['meta']['x'],
                                    data['meta']['y']
                                ),
                    'comment'   : data['meta']['date_note']
                }
                cur.execute(query, params)
            except psycopg2.DatabaseError as e:
                print(cur.mogrify(query, params))
                raise e

            # Get value of current sources.meta.id
            #
            # We need this to connect a line of text to the metadata about the
            # deed that contains it in the next step
            srcid = cur.fetchone()[0]

            # Loop over lines of text
            for line in data['lines']:

                # Query to insert line values
                query = '''
                    INSERT INTO sources.lines (
                        text,
                        loc,
                        srcid
                    )
                    VALUES (
                        %(text)s,
                        %(loc)s,
                        %(srcid)s
                    )
                '''

                # Execute query
                try:
                    params = {
                        'text'  : line['text'],
                        'loc'   : line['loc'],
                        'srcid' : srcid
                    }
                    cur.execute(query, params)
                except psycopg2.DatabaseError as e:
                    print(cur.mogrify(query, params))
                    raise e

# THIRD insert analysis data from `data/beide_cao_beideunde.csv`
# (a.k.a. the hard part, pt. 2)
#
# Not gonna reproduce all the table definitions here. Look into db_bootstrap.py
# for those. We will make heavy use of getfeatidbyval() to match the manually
# entered, human-readable values to their associated ID in the various feature
# tables.
with open(os.path.join('data', 'beide_cao_beideunde.csv'), 'r') as csvfile:
    
    # Read CSV file into dictionary (OrderedDict, actually)
    reader = csv.DictReader(csvfile)
    
    # Loop over rows in CSV file
    for row in reader:

        # Python's csv module will import every value as string, so we have to
        # do some manual type conversion
        for k,v in row.items():

            # Empty string to None
            if v == '':
                row[k] = None
            else:
                try:
                    # Everything else to int
                    row[k] = int(v)
                except:
                    # Do nothing if that won't work (e.g. because it's a string)
                    pass

        # CONTROLLER
        query = '''
            INSERT INTO public.controller (
                id,
                linid,
                text,
                loc,
                co_ctrlid,
                form_pers,
                form_num,
                form_gend,
                form_case,
                sem_pers,
                sem_num,
                sem_sex,
                sem_anim,
                pos,
                flex_type,
                flex_rhyme,
                flex_elidable,
                orig,
                comment
            )
            VALUES (
                %(id)s,
                %(linid)s,
                %(text)s,
                %(loc)s,
                %(co_ctrlid)s,
                %(form_pers)s,
                %(form_num)s,
                %(form_gend)s,
                %(form_case)s,
                %(sem_pers)s,
                %(sem_num)s,
                %(sem_sex)s,
                %(sem_anim)s,
                %(pos)s,
                %(flex_type)s,
                %(flex_rhyme)s,
                %(flex_elidable)s,
                %(orig)s,
                %(comment)s
            )
            ON CONFLICT DO NOTHING
        '''

        # Execute query
        try:
            params = {
                'id'            : row['c_id'],
                'linid'         : getlinidbyloc(row['c_loc']),
                'text'          : row['c_text'],
                'loc'           : row['c_loc'],
                'co_ctrlid'     : row['c_assoc_c'],
                'form_pers'     : getfeatidbyval(row['c_form_pers'], 'pers'),
                'form_num'      : getfeatidbyval(row['c_form_num'], 'num'),
                'form_gend'     : getfeatidbyval(row['c_form_gend'], 'gend'),
                'form_case'     : getfeatidbyval(row['c_form_case'], 'case'),
                'sem_pers'      : getfeatidbyval(row['c_sem_pers'], 'pers'),
                'sem_num'       : getfeatidbyval(row['c_sem_num'], 'num'),
                'sem_sex'       : getfeatidbyval(row['c_sem_sex'], 'sex'),
                'sem_anim'      : getfeatidbyval(row['c_sem_anim'], 'anim'),
                'pos'           : getfeatidbyval(row['c_pos'], 'pos'),
                'flex_type'     : row['c_flex_type'],
                'flex_rhyme'    : makebool(row['c_flex_rhyme']),
                'flex_elidable' : makebool(row['c_flex_elidable']),
                'orig'          : makebool(row['c_orig']),
                'comment'       : row['c_comment'],
            }
            cur.execute(query, params)
        except psycopg2.DatabaseError as e:
            print(cur.mogrify(query, params))
            raise e

        # TARGET
        query = '''
            INSERT INTO public.target (
                id,
                linid,
                text,
                loc,
                form_pers,
                form_num,
                form_gend,
                form_case,
                sem_pers,
                sem_num,
                sem_sex,
                sem_anim,
                pos,
                flex_type,
                flex_rhyme,
                flex_elidable,
                floatquant,
                intermed,
                comment
            )
            VALUES (
                %(id)s,
                %(linid)s,
                %(text)s,
                %(loc)s,
                %(form_pers)s,
                %(form_num)s,
                %(form_gend)s,
                %(form_case)s,
                %(sem_pers)s,
                %(sem_num)s,
                %(sem_sex)s,
                %(sem_anim)s,
                %(pos)s,
                %(flex_type)s,
                %(flex_rhyme)s,
                %(flex_elidable)s,
                %(floatquant)s,
                %(intermed)s,
                %(comment)s
            )
            ON CONFLICT DO NOTHING
        '''

        # Execute query
        try:
            params = {
                'id'            : row['t_id'],
                'linid'         : getlinidbyloc(row['t_loc']),
                'text'          : row['t_text'],
                'loc'           : row['t_loc'],
                'form_pers'     : getfeatidbyval(row['t_form_pers'], 'pers'),
                'form_num'      : getfeatidbyval(row['t_form_num'], 'num'),
                'form_gend'     : getfeatidbyval(row['t_form_gend'], 'gend'),
                'form_case'     : getfeatidbyval(row['t_form_case'], 'case'),
                'sem_pers'      : getfeatidbyval(row['t_sem_pers'], 'pers'),
                'sem_num'       : getfeatidbyval(row['t_sem_num'], 'num'),
                'sem_sex'       : getfeatidbyval(row['t_sem_sex'], 'sex'),
                'sem_anim'      : getfeatidbyval(row['t_sem_anim'], 'anim'),
                'pos'           : getfeatidbyval(row['t_pos'], 'pos'),
                'flex_type'     : row['t_flex_type'],
                'flex_rhyme'    : makebool(row['t_flex_rhyme']),
                'flex_elidable' : makebool(row['t_flex_elidable']),
                'floatquant'    : makebool(row['t_floatquant']),
                'intermed'      : makebool(row['t_intermed']),
                'comment'       : row['t_comment'],
            }
            cur.execute(query, params)
        except psycopg2.DatabaseError as e:
            print(cur.mogrify(query, params))
            raise e

        # DOMAIN
        query = '''
            INSERT INTO public.domain (
                ctorder,
                dist_word,
                dist_syn,
                agrrel
            )
            VALUES (
                %(ctorder)s,
                %(dist_word)s,
                %(dist_syn)s,
                %(agrrel)s
            )
            ON CONFLICT DO NOTHING
            RETURNING public.domain.id
        '''

        # Execute query
        try:
            params = {
                'ctorder'   : getfeatidbyval(row['d_order'], 'ctorder'),
                'dist_word' : row['d_dist_word'],
                'dist_syn'  : getfeatidbyval(row['d_dist_syn'], 'distsyn'),
                'agrrel'    : getfeatidbyval(row['d_agrrel'], 'agrrel'),
            }
            cur.execute(query, params)
        except psycopg2.DatabaseError as e:
            print(cur.mogrify(query, params))
            raise e

        # Get value of current public.domain.id
        domid = cur.fetchone()[0]

        # SETS
        #
        # First, infer the srcid from a lineid associated with this record
        # set---we will simply use the controller's linid here. We can't access
        # the correct srcid directly from the `srcid` variable set earlier
        # because we're in a different loop, so the `srcid` variable will always
        # return the value of the last entered record if called here.
        query = '''
            SELECT DISTINCT src.srcid AS srcid
            FROM public.controller
            JOIN sources.lines src ON src.id = public.controller.linid
            WHERE public.controller.id = {}
        '''.format(row['c_id'])

        # Execute query, set srcid
        try:
            cur.execute(query)
            srcid = cur.fetchone()[0]
        except psycopg2.DatabaseError as e:
            print(cur.mogrify(query, params))
            raise e

        # Do the actual query for sets
        query = '''
            INSERT INTO public.sets (
                setno,
                ctrlid,
                targid,
                domid,
                srcid
            )
            VALUES (
                %(setno)s,
                %(ctrlid)s,
                %(targid)s,
                %(domid)s,
                %(srcid)s
            )
        '''

        # Execute query
        try:
            params = {
                'setno'  : row['set_id'],
                'ctrlid' : row['c_id'],
                'targid' : row['t_id'],
                'domid'  : domid,
                'srcid'  : srcid,
            }
            cur.execute(query, params)
        except psycopg2.DatabaseError as e:
            print(cur.mogrify(query, params))
            raise e

        # OVERLAP
        query = '''
            INSERT INTO public.overlap (
                targid,
                ctrlid
            )
            VALUES (
                %(targid)s,
                %(ctrlid)s
            )
        '''

        # Execute query
        try:
            params = {
                'targid' : row['t_id'],
                'ctrlid' : row['t_c_fk'],
            }
            cur.execute(query, params)
        except psycopg2.DatabaseError as e:
            print(cur.mogrify(query, params))
            raise e

# Close cursor
cur.close()

# Close DB connection
conn.close()
