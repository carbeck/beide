#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''Export KC texts with their meta data from TXT files as JSON files to use with
   the `beide` project.

   Carsten Becker, 2020
   <carsten [dot] becker [at] uni [dash] marburg [dot] de>
'''

import csv
import json
import os
import os.path
import zipfile

# Manuscripts which contain examples
#
# NOTE: The dates are adapted from what it says in the comment and are just
# approximations. I added coordinates everywhere to be able to put things on a
# map at all. In cases where only areas are named (e.g. Bavaria, central
# Germany), the coordinates approximate their center; in cases where tentative
# place names are given (e.g. Vienna?), the coordinates of this place have been
# used. The citations for each item can be viewed on the manuscript's HSC page
# (http://www.handschriftencensus.de/<hscid>). Please also note that the data
# model here differs slightly from that used for the deeds.

mscmeta = [
    {
        'src': 'A_A1-stav_ms276',
        'repository': {
            'place': 'Vorau',
            'institution': 'Archiv des Augustiner Chorherrenstifts',
            'callno': 'StaV-Ms 276'
        },
        'lang': 'bair.-österr. (vgl. Schneider 1987: 40f.)',
        'date_from': '1176-01-01',
        'date_to': '1200-12-31',
        'date_note': '4. Viertel 12. Jh. (KCD; Schneider 1987: 37f.)',
        'url': 'https://doi.org/10.11588/diglit.29478',
        'hscid': 1432,
        'place': 'Vorau',
        'place_id_caodb': None,
        'place_id_trier': None,
        'place_comment' : 'Vorau (HSC)',
        'x': 15.888889,
        'y': 47.4
    },
    {
        'src': 'A_M-bsb_cgm37',
        'repository': {
            'place': 'München',
            'institution': 'Bayerische Staatsbibliothek',
            'callno': 'Cgm 37'
        },
        'lang': 'bair. (Schröder 1895: 9; Klein 1988: 128)',
        'date_from': '1326-01-01',
        'date_to': '1400-31-12',
        'date_note': '2. Viertel 14. Jh. (KCD; Hernad 1996: 12; Montag and Schneider 2003: 30)',
        'url': 'https://doi.org/10.11588/diglit.28530',
        'hscid': 2119,
        'place': 'Bayern',
        'place_id_caodb': None,
        'place_id_trier': None,
        'place_comment' : 'Bayern (KCD)',
        'x': 11.395,
        'y': 48.949444
    },
    {
        'src': 'A_H-cpg361',
        'repository': {
            'place': 'Heidelberg',
            'institution': 'Universitätsbibliothek',
            'callno': 'Cod. Pal. germ. 361'
        },
        'lang': 'hess. (vgl. Klein 1988: 128)',
        'date_from': '1226-01-01',
        'date_to': '1350-31-12',
        'date_note': '2. Viertel 13. Jh. (KCD); Mitte 13. Jh. (HSC)',
        'url': 'https://doi.org/10.11588/diglit.167',
        'hscid': 1181,
        'place': 'Mainz',
        'place_id_caodb': 509,
        'place_id_trier': 'ol0537',
        'place_comment' : 'Hessen (Mainz?) (KCD)',
        'x': 8.2711111111111,
        'y': 50
    },
    {
        'src': 'B_B1-onb_cod_2779',
        'repository': {
            'place': 'Wien',
            'institution': 'Österreichische Nationalbibliothek',
            'callno': 'Cod. 2779'
        },
        'lang': 'ostobd. (niederösterr.) (Klein 1988: 151); bair.-österr. (Knapp and Niesner 2000: IX); mittelbair. (Becker 1977: 61)',
        'date_from': '1301-01-01',
        'date_to': '1325-12-31',
        'date_note': '1. Viertel 14. Jh. (KCD; Menhardt 1960: 292; Knapp and Niesner 2000: IX); 2. Viertel 14. Jh. (Fingernagel and Roland: 266)',
        'url': 'https://doi.org/10.11588/diglit.28854',
        'hscid': 2693,
        'place': 'Wien',
        'place_id_caodb': 976,
        'place_id_trier': 'ol1030',
        'place_comment' : 'Wien? (KCD)',
        'x': 16.373056,
        'y': 48.208333
    },
    {
        'src': 'B_VB-onb_cod_2693',
        'repository': {
            'place': 'Wien',
            'institution': 'Österreichische Nationalbibliothek',
            'callno': 'Cod. 2693'
        },
        'lang': 'bair.-österr. (Menhardt 1960: 119)',
        'date_from': '1276-01-01',
        'date_to': '1300-12-31',
        'date_note': '4. Viertel 13. Jh. (KCD; Schneider 1987: 224)',
        'url': 'https://doi.org/10.11588/diglit.28856',
        'hscid': 1215,
        'place': None,
        'place_id_caodb': None,
        'place_id_trier': None,
        'place_comment' : None,
        'x': 13.25,
        'y': 47.92
    },
    {
        'src': 'B_P-tnb_codxxiii_g_43',
        'repository': {
            'place': 'Prag',
            'institution': 'Nationalbibliothek der Tschechischen Republik',
            'callno': 'Cod. XXIII.G.43'
        },
        'lang': 'md. (Gärtner 1995: 372)',
        'date_from': '1226-01-01',
        'date_to': '1250-12-31',
        'date_note': '2. Viertel 13. Jh. (KCD; Gärtner 1995: 372)',
        'url': 'https://doi.org/10.11588/diglit.28879',
        'hscid': 1168,
        'place': None,
        'place_id_caodb': None,
        'place_id_trier': None,
        'place_comment' : None,
        'x': 9.68,
        'y': 50.44
    },
    {
        'src': 'C_C1-onb_cod_2685',
        'repository': {
            'place': 'Wien',
            'institution': 'Österreichische Nationalbibliothek',
            'callno': 'Cod. 2685'
        },
        'lang': 'bair.-österr. (Schröder 1895: 23)',
        'date_from': '1301-01-01',
        'date_to': '1310-12-31',
        'date_note': 'Anfang 14. Jh. (KCD; Fingernagel and Roland 1997: 135)',
        'url': 'https://doi.org/10.11588/diglit.29130',
        'hscid': 2013,
        'place': None,
        'place_id_caodb': None,
        'place_id_trier': None,
        'place_comment' : None,
        'x': 13.25,
        'y': 47.92
    },
    {
        'src': 'C_K-blb_augpap52',
        'repository': {
            'place': 'Karlsruhe',
            'institution': 'Badische Landesbibliothek',
            'callno': 'Cod. Aug. 52'
        },
        'lang': 'alem. (Schröder 1895: 24)',
        'date_from': '1350-01-01',
        'date_to': '1377-12-31',
        'date_note': '2. Hälfte 14. Jh. (vor 1378) (KCD; Schröder 1895: 24)',
        'url': 'https://doi.org/10.11588/diglit.28454',
        'hscid': 8470,
        'place': 'Reichenau',
        'place_id_caodb': 678,
        'place_id_trier': 'ol0717',
        'place_comment' : 'Reichenau? (KCD)',
        'x': 9.06208,
        'y': 47.69874
    },
    {
        'src': 'C_Z-wzga_zams30',
        'repository': {
            'place': 'Schloss Zeil',
            'institution': 'Fürstl. Waldburg zu Zeil und Trauchburgsches Gesamtarchiv (Leutkirch)',
            'callno': 'ZAMs 30'
        },
        'lang': 'schwäb. (Schröder 1895: 24)',
        'date_from': '1490-01-01',
        'date_to': '1500-12-31',
        'date_note': 'Ende 15. Jh. (KCD; Schröder 1895: 24)',
        'url': 'https://doi.org/10.11588/diglit.28560',
        'hscid': 8471,
        'place': 'Schwaben',
        'place_id_caodb': None,
        'place_id_trier': None,
        'place_comment' : 'Schwaben (KCD)',
        'x': 9.99,
        'y': 48.39
    },
]

# Read lines from CSV source into list as dictionaries
msctext = []
with open(os.path.join('data', 'kc_texts.csv'), mode='r') as csvin:
    reader = csv.DictReader(csvin)
    for row in reader:
        msctext.append(row)

# Prepare to write data into 'data/cao.zip' at the end
with zipfile.ZipFile(os.path.join('data','kc.zip'), mode='w',
    compression=zipfile.ZIP_DEFLATED) as zip:

    # Loop over the line items by manuscript
    for item in mscmeta:

        # Filter for lines to just include the lines for the current manuscript
        lines = []
        for line in msctext:
            if line['src'] == item['src']:
                lines.append(line)

        # Properly type schroeder column as integer/None according to its
        # content (Dear reader, I facepalmed hard when this threw errors
        # when reading things back into the database. The csv library
        # apparently handles anything in a CSV file as a string even though
        # the format actually *does* support integers and empty cells.)
        for line in lines:

            # Empty string -> None
            if line['schroeder'] == '':
                line['schroeder'] = None

            # All other cases: evaluate the str to type it
            else:
                line['schroeder'] = eval(line['schroeder'])

        # Split date strings for maybe some extra use
        item["date_from"] = {
            'ymd':   str(item["date_from"]),
            'year':  str(item["date_from"]).split('-')[0],
            'month': str(item["date_from"]).split('-')[1],
            'day':   str(item["date_from"]).split('-')[2]
        }
        item["date_to"] = {
            'ymd':   str(item["date_to"]),
            'year':  str(item["date_to"]).split('-')[0],
            'month': str(item["date_to"]).split('-')[1],
            'day':   str(item["date_to"]).split('-')[2]
        }

        # Unify the information
        data = {'lines' : lines, 'meta' : item}

        # Cast into JSON format (pretty-printed for human readability)
        jsondata = '{}\n'.format(json.dumps(data, indent=4, sort_keys=True))

        # Write into ZIP file
        zip.writestr('kc_{}.json'.format(item['src']), jsondata)
