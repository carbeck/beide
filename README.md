# Gender Resolution bei mittelhochdeutsch *beide*: Eine Analyse im Rahmen der Lexical-Functional Grammar
(Gender resolution of Middle High German *beide*: A lexical-functional analysis)

This repository contains the _LaTeX_ source files of my doctoral thesis project, the _PostgreSQL_ database (better refer to the spreadsheets, though), and some preliminary notes. The thesis was submitted to University of Marburg's Department of German Studies and Arts in May 2022 and defended in September 2022 with the requirement of minor revisions. It is written in German.

## Abstract (ish)

The thesis surveys gender agreement of Middle High German (MHG) _bėide_ 'both' in its various grammatical contexts: productive in the D-quantifier (1), and fossilized in the related correlative conjunction _bėide … unde_ 'both … and' (2; cf. Klein et al. 2018: 623–628).

```
(1) vlrich           vnd frow Elzbet            […] diſen
    Ulrich[NOM.SG.M] and Mrs. Elsbeth[NOM.SG.F]     this

    prief   / den   ſi      beidev            habent
    charter   which 3PL.NOM both-NOM.PL.N.STR have

    gebeten ze ſchriben
    asked   to write

    'Ulrich and Mrs. Elsbeth […] this charter, which they
    both have asked to write'

               (Wilhelm et al. 1932–2004: vol. 4, 176:26–27
                                [No. 2843, Salzburg, 1297])

(2) Beide zeberg  vnd zetal
    both  to=hill and to=valley

    Fvrter ſie  vber al
    led=he them everywhere

    'Both on the hill and in the valley—he led them
    everywhere.'

                (Kaiserchronik, Vienna, Austrian Nat. Lib.,
                                       Cod. 2693: 83vb:3–4)
```

The diplomatic transcriptions of German-language medieval charters in the _Corpus der altdeutschen Originalurkunden bis zum Jahr 1300_ (CAO; Wilhelm et al. 1932–2004; Gärtner et al. 2007) on the one hand and the _Kaiserchronik_ (KC; Schröder 1985; Chinca et al. 2018; see also Nellmann 1983) as represented by the most important complete manuscripts of its three recensions on the other serve as data sources. Different than in Modern Standard German (Wöllstein 2022: 772), the Upper German dialects of the MHG period (c. 1050–1350 CE) distinguished between masculine–feminine _-e_ and neuter _-iu_ in the nominative–accusative plural of strong adjectives (Klein et al. 2018: 182). Strikingly, both _bėide_ and _bėidiu_ occur in identical grammatical contexts of combined reference in varieties which otherwise productively distinguish both inflections in adjectives with single reference.

The work's aim is to analyze and quantify the occurrence of both inflected forms of _bėide_ with regard to (combined) formal and semantic person features, word distance and syntactic domain of agreement controller and target, as well as geography, with special attention to gender resolution (Corbett 1983). The Lexical-functional Grammar (LFG) framework (Bresnan & Kaplan 1982; Bresnan et al. 2016) with its feature-driven approach provides a robust means to model this phenomenon (Wechsler & Zlatić 2003: 171–195; Wechsler 2009), updating Askedal's (1973) analysis based on Prague School structuralism, early generative grammar, and a very limited sample set based on normalized editions of a handful of historical texts.

Even though the phenomenon of the neuter serving as a resolution gender in anaphoric reference also with animates is well-attested in the history of Germanic languages (see e.g. Hock 2008), little work has so far been done on a data-driven analysis of the phenomenon in MHG, especially one firmly rooted in manuscript evidence rather than normalized editions. Furthermore, _communis opinio_ on conjunctional _bėide_ holds it is fossilized in either declension form. The thesis shows how the surveyed sources support this conclusion, and searches for factors determining the respective historical outcome.

## Grading version

The state of the repository at the date of submission for grading was tagged "Abgabefassung" ('submitted version'). Click to access the [compiled PDF of the thesis at that stage](https://gitlab.com/carbeck/beide/-/blob/Abgabefassung/beide.pdf).

## Working draft (revised)

Since doctoral theses submitted to German public universities need to be published in order to be granted the degree diploma, I have since revised the document also to implement minor revisions requested by the grading assessment reports. See the [latest working draft compilation](https://gitlab.com/carbeck/beide/-/blob/master/beide.pdf) at the master branch. This is very explicitly a pre-peer review, pre-publication version subject to further changes.

On July 10, 2023, [Language Science Press](https://www.langsci-press.org) has approved my proposal for a book publication in the _Advances in Historical Linguistics_ (AHL) series pending further (small) revisions and formatting of the manuscript according to their stylesheet, before properly subjecting the manuscript to peer review. For their convenience, and also because a standardized project file structure wholly different from mine is required, the latter part will take place at [GitHub](https://github.com/carbeck/beide-pub) once the contents of the working draft are fit for this next step.

## References

* Askedal, John O. 1973. _Neutrum Plural mit persönlichem Bezug im Deutschen unter Berücksichtigung des germanischen Ursprungs_ (Germanistische Schriftenreihe der norwegischen Universitäten und Hochschulen 4). Trondheim: Universitetsforlaget.
* Bresnan, Joan, Ash Asudeh, Ida Toivonen & Stephen Wechsler. 2016. _Lexical-functional syntax_. 2nd edn. (Blackwell Textbooks in Linguistics 16). Chichester: Wiley–Blackwell.
* Bresnan, Joan & Ronald M. Kaplan. 1982. Introduction: Grammars as mental representations of language. In Joan Bresnan (ed.), _The mental representation of grammatical relations_ (MIT Press Series on Cognitive Theory and Mental Representation), XVII–LII. Cambridge: MIT Press.
* Chinca, Mark, Helen Hunter, Jürgen Wolf & Christopher Young (eds.). 2018. _Kaiserchronik digital_. Heidelberg: Universitätsbibliothek Heidelberg. [https://digi.ub.uni-heidelberg.de/kcd/](https://digi.ub.uni-heidelberg.de/kcd/) (26 November, 2018).
* Corbett, Greville G. 1983. Resolution rules: Agreement in person, number, and gender. In Gerald Gazdar, Ewan Klein & Geoffrey K. Pullum (eds.), _Order, concord and constituency_ (Linguistic Models 4), 175–206. Dordrecht: Foris.
* Gärtner, Kurt, Andrea Rapp & Andreas Gniffke (eds.). 2007. _Corpus der altdeutschen Originalurkunden bis zum Jahr 1300_. Elektronische Edition. Universität Trier. [http://tcdh01.uni-trier.de/cgi-bin/iCorpus/CorpusIndex.tcl](http://tcdh01.uni-trier.de/cgi-bin/iCorpus/CorpusIndex.tcl) (23 September, 2015).
* Hock, Hans Henrich. 2008. Early Germanic agreement with mixed-gender antecedents with focus on the history of German. In Karlene Jones-Bley, Martin E. Huld, Angela Della Volpe & Miriam Robbins Dexter (eds.), _Proceedings of the 19th annual UCLA Indo-European conference: Los Angeles, 2007_ (Journal of Indo-European Studies Monograph Series 54), 151–170. Washington: Institute for the Study of Man.
* Klein, Thomas, Hans-Joachim Solms & Klaus-Peter Wegera (eds.). 2018. _Mittelhochdeutsche Grammatik_. Vol. 2: _Flexionsmorphologie_. Berlin: De Gruyter.
* Nellmann, Eberhard. 1983. Kaiserchronik. In Kurt Ruh, Gundolf Keil, Werner Schröder, Burghart Wachinger, Franz Josef Worstbrock & Christine Stöllinger (eds.), _Verfasserlexikon: Die deutsche Literatur des Mittelalters_. Vol. 4: _Hildegard von Hürnheim – Koburger, Heinrich_, 2nd edn., 949–964. Berlin: De Gruyter.
* Schröder, Edward (ed.). 1895. _Deutsche Chroniken und andere Geschichtsbücher des Mittelalters_. Vol. 1: _Kaiserchronik eines Regensburger Geistlichen_ (Monumenta Germaniae Historica 1). Hannover: Hahn.
* Wechsler, Stephen. 2009. "Elsewhere" in gender resolution. In Kristin Hanson & Sharon Inkelas (eds.), _The nature of the word: Studies in honor of Paul Kiparsky_ (Current Studies in Linguistics), 567–586. Cambridge: MIT Press.
* Wechsler, Stephen & Larisa Zlatić. 2003. _The many faces of agreement_ (Stanford Monographs in Linguistics). Stanford: CSLI Publications.
* Wilhelm, Friedrich, Richard Newald, Helmut de Boor, Diether Haacke & Bettina Kirschstein (eds.). 1932–2004. _Corpus der altdeutschen Originalurkunden bis zum Jahr 1300_. Berlin: Erich Schmidt.
* Wöllstein, Angelika (ed.). 2022. _Die Grammatik_. 10th edn. (Duden 4). Berlin: Dudenverlag.

---
Carsten Becker, M.A.  
(Humboldt-Universität zu Berlin)  
[ORCID: 0000-0002-6023-551X](https://orcid.org/0000-0002-6023-551X)  
<carsten[dot]becker[at]hu[minus]berlin[dot]de>  
2017–2023
