\chapter{Zusammenfassung und Ausblick}
\label{ch:zusammenfassung}

Die starke Adjektivdeklination im oberdeutschen Sprachraum der
mittelhochdeutschen Periode macht im Nom.\ und Akk.\ Pl.\ noch einen
Unterschied zwischen Maskulinum und Femininum gegenüber dem Neutrum
\autocite[182]{ksw2}. Die Wortform des hier exemplarisch untersuchten
quantifizierenden Determinierers \wdef{beide} lautet daher je nach
Kongruenzkontext \norm{bėide} (Nom./Akk.\ Pl.\ M./F.\ stark) oder \norm{bėidiu}
(Nom./Akk.\ Pl.\ N.\ stark).

Gerade beim kombinierten belebten Bezug auf Referenten mit unterschiedlichem
Genus beziehungsweise Sexus -- männlich und weiblich als grammatikalisierte
semantische Geschlechterkategorien der Animata -- ist aber in der Regel die
formal neutrale Form zu finden. Diese Art von \q*{Auflösungsphänomen}, das bei
konfligierenden Genus- oder Sexusmerkmalen von kombinierten Personenmerkmalen
auftritt, wird als \textit{Gender Resolution} bezeichnet
\autocites{corbett1983}[264--306]{corbett1991}. Da jedoch auch Fälle zu
beobachten sind, in denen die formal maskulin-feminine Form auftritt, wurde in
dieser Arbeit der Frage nachgegangen, wie häufig die jeweiligen Formen in
verschiedenen syntaktischen Kontexten auftreten und welche Parameter einen
Einfluss auf das Erscheinen der einen oder anderen Form haben. Darüber hinaus
bestand die Frage, wie sich \norm{bėide} in der koordinierenden
Konstruktion \norm{bėide \dots\ unde} \wdef{sowohl \dots\ als auch} im
Vergleich zu quantifizierendem \norm{bėide} verhält.

Die älteren Grammatikwerke zum Mittelhochdeutschen
\autocites[384]{paul2007}[187--189, 222]{dal2014}[258]{michels1979}%
[221]{mettke2000} machen zu beiden Aspekten nur sehr pauschale Aussagen.
\citet[621--628]{ksw2} gehen zwar auf die möglichen syntaktischen Kontexte des
Quantors \norm{bėide} und deren Häufigkeit in ihrem Korpus sowie auf die
geografische Distribution der Formen der Konjunktion \norm{bėide} ein. Sie
machen aber keine Angaben zur Variationsbreite zwischen den Formen in den
verschiedenen Kontexten sowie deren steuernde Faktoren; letzterer Aspekt wäre
für den derzeit ausstehenden Band zur Syntax zu erwarten. Darüber hinaus stehen
zwar zwei Studien zum hier untersuchten Themenkomplex von
\citet{askedal1973,askedal1974} zur Verfügung, doch dienen ihm für die
mittelhochdeutsche Periode textkritische Editionen lediglich zweier
prominenter Verstexte der Artusepik als Quelle. Seine Schlussfolgerungen zum
konjunktionalen Gebrauch von \norm{bėide} \autocite{askedal1974} sind zudem
fragwürdig und beschränken sich auf nominale Konjunkte \autocite{gjelsten1980}.

Die Idee dieser Arbeit war daher, sowohl den Quantor als auch die Konjunktion
\norm{bėide} auf einer handschriftennahen und zumindest synchron breiteren
Basis als die Arbeiten von \citet{askedal1973,askedal1974} zu untersuchen. Als
Textgrundlage dienten zum einen das \citetitle*{cao} (\citetitle{cao}) als
Sammlung von alltagsnahen Gebrauchstexten in Prosa vor allem des späten
13.~Jahrhunderts und zum anderen die Haupthandschriften der \citetitle{kc}
(\citet{kc}) als erzählerisch ausgestalteter Sachtext in Versform, der
hauptsächlich im 13.\ und 14.\ Jahrhundert breit über\-liefert ist. Diese Texte
haben den Vorteil, dass sie digital in diplomatischer Transkription vorliegen.
Dies vereinfacht die Belegsammlung enorm, gerade auch, weil das hauptsächlich
untersuchte Phänomen, mittelhochdeutsch \norm{bėide} \wdef{beide} in Bezug auf
zwei Referenten, nicht ausgesprochen häufig auftritt.

Für das \citetitle{cao} beträgt die Frequenz von \norm{bėide} und \norm{bėidiu}
zusammen ca.~282 Vorkommen pro 1~Mio. Wortformen, für die \citet{kc} ca.~510
Vorkommen pro 1~Mio. Die Texte des \citetitle{cao} und der \citet{kc} wurden
mit Hilfe von regulären Ausdrücken durchsucht und anschließend manuell
annotiert \autocites[vgl.\ z.\,B.][33--37]{perkuhnetal2012}[zur Methode
vgl.][207--209]{beckerschallert2021}[155--158]{beckerschallert2022b}. Der
Umstand, dass die Urkunden häufig dem engeren Umkreis eines bestimmten
Ausstellungs\-orts zugeordnet werden können, erlaubt einen zusätzlichen Blick
auf die geografische Dimension der beobachteten Variation.
% Dies ermöglicht prinzipiell einen schreibdialektalen Vergleich mit
% literarischen Handschriften, deren regionale Herkunft in der Regel nur
% indirekt erschlossen werden kann.

\section{Ergebnisse}

In einer vorbereitenden Untersuchung der zahlenmäßig größten Ausstellungsorte
und ihrer näheren Umgebung sowie zu den auszuwertenden Handschriften der
\citet{kc} anhand einiger im Material häufig belegter Adjektive wurde
sichergestellt, dass am jeweiligen Ort oder in der jeweiligen Handschrift
grundsätzlich ein Unterschied in der Flexion zwischen \norm{\mbox{-e}} und
\norm{-iu} vorhanden ist. Dies steht vor dem Hintergrund der Genusnivellierung
im Plural der Adjektivdeklination zum Frühneuhochdeutschen hin
\autocite[191--192]{ebertetal1993}, deren Wirken sich jedoch weder im
\citetitle{cao} noch in den meisten \citet{kc} stark bemerkbar gemacht hat.
Ausnahmen bilden Straßburger Urkunden und die \citet{kc}-Handschrift
\citet{kc:Z}.

Die \citet{kc}-Handschriften wurden jeweils nach Eindeutigkeit und Vorkommen
der Opposition im jeweiligen syntaktischen Kontext in verschiedene Gruppen
eingeteilt, um sie in ihrer Relevanz für die Untersuchung zu priorisieren. Zur
Auswertung des Quantors \norm{bėide} dienten hauptsächlich die Handschriften
\citet{kc:B1}, \citet{kc:C1}, \citet{kc:K} und \citet{kc:VB}; bei der
Konjunktion \norm{bėide} kamen vor allem \citet{kc:A1}, \citet{kc:B1} und
\citet{kc:VB} zum Zug. Gerade die Vorauer Handschrift (\citet{kc:A1}) als
bedeutsamste \citet{kc}-Handschrift fand bei der Auswertung zum Quantor
\norm{bėide} also keine Verwendung, da in diesem Kontext nahezu
ausschließlich maskuline Controller belegt sind. Dies stellte generell bei der
Auswertung der \citet{kc} eine Herausforderung dar, die sich auf die Thematik
des Texts zurückführen lässt.

Die beiden Auswertungsserien sind prinzipiell miteinander vergleichbar. Die aus
der \citet{kc} exzerpierten Belege, wenn auch geringer in der Anzahl, verhalten
sich nicht grundlegend anders als die aus dem \citetitle{cao} gewonnenen.
Allerdings wurden für die \citet{kc} aufgrund der von
\textcites[89--90]{askedal1973}[191]{gjelsten1980} geäußerten Vorbehalte solche
Kontexte nicht gewertet, in denen \norm{bėide} am Versende oder vor einem Wort
steht, das mit einem Vokal beginnt. Sie sprechen diesbezüglich von
Neutralisierungs\-positionen. Für das \citetitle{cao} wurden Belege im
letzteren Kontext ebenfalls zunächst unter Vorbehalt betrachtet für den Fall,
dass dieser auch auf Prosatexte zutrifft, wie \citet[92]{askedal1973} zu
bedenken gibt. Jedoch ließ sich für die Prosatexte kein Unterschied zu
Nicht-\allowbreak{}Hiatus-\allowbreak{}Belegen feststellen.
\citeauthor{askedal1973}s Einwand ist also zumindest für das \citetitle{cao} zu
verneinen.

\subsection{\norm{Bėide} als Quantor: Animata}

Die Funktion von \norm{bėide} als Quantor betreffend stimmt die Mehrzahl
der gesammelten Belege zu belebten Referenten mit der bereits von
\textcites[312]{grimm1890}[39--41]{behaghel1928} formulierten Beobachtung
überein, dass beim Bezug auf zwei Referenten mit gleichem Genus die
maskulin-feminine Form steht, während in Bezug auf Referenten mit
unterschiedlichem Genus das Neutrum auftritt. Dies gilt allerdings auch zum
Beispiel für den Bezug auf Pronomen der ersten und zweiten Person, die formal
keine overte Genuskategorie besitzen, diese Informationen aber in ihrer
referentiellen Semantik beinhalten. Generell gilt, dass beim kombinierten Bezug
auf belebte Referenten deren semantisches Geschlecht (Sexus) beziehungsweise
die damit assoziierten Resolutionsregeln im Großteil der Fälle die
Kongruenzform bestimmen.

Betreffend der Distribution der syntaktischen Kontexte ist in Übereinstimmung
mit \citet[624, Abb.~P~179]{ksw2} anzumerken, dass der unmittelbare Bezug von
\norm{bėide} auf zwei Controller in beiden Auswertungsserien sehr selten
auftritt. In nahezu allen der wenigen belegten Fälle liegt in diesen Kontexten
Distanzstellung vor (\norm{$N_i$ unde $N_j$ \dots\ bėide}), was die Wortabfolge
betrifft. Der mittelbare Bezug auf zwei Controller über mindestens ein Pronomen
macht den Großteil der Belege aus (Kontaktstellung: \norm{$PRO_{i+j}$ bėide};
Distanzstellung: \norm{$PRO_{i+j}$ \dots\ bėide}). Genus\-indifferentes
\norm{si} \wdef{sie} stellt gefolgt von \norm{wir} \wdef{wir} den häufigsten
unmittelbaren Controller von \norm{bėide} dar.

Historisch ist zwar im Nom./Akk.\ bei den Personalpronomen der 3.~Pers.\ Pl.\
mit der Genus\-opposition \norm{sie} (\M+\F) gegenüber \norm{siu} (\N) zu
rechnen. Eine stichprobenhafte Teilauswertung hat ergeben, dass in den hier
verwendeten Materialien dieser Unterschied in aller Regel aber schon nicht mehr
nachvollziehbar ist, also generell \norm{si} steht, landschaftlich auch eine
Form vom Typ
\norm{sei},
\norm{seu},
\norm{sie} oder
\norm{siu}
\autocites[vgl.][213--214]{paul2007}[369, 390--397]{ksw2}[482--483]{wmu1}.
\posscite[99]{askedal1973} Hypothese, dass \norm{si bėide} eine
Konstruktion bildet, bei der das Neutrum immer nur an einem der beiden Glieder
markiert wird (\q*{Monoflexion}), kann für das ausgewertete Material daher
nicht nachvollzogen werden.

Gerade in Kontaktstellung zu Personalpronomen wurde bei belebtem Bezug die
höchste Variation zwischen \norm{bėide} und \norm{bėidiu} beobachtet. Die
Begründung dafür wurde in der Annahme gesucht, dass für die Auflösung des
gemischtgeschlechtlichen Bezugs entweder formale ($\M \lor \F \Rightarrow$
\norm{-e}) oder semantische Kongruenz ($\SM \cap \SF \Rightarrow$ \norm{-iu})
in Frage kommt, falls \norm{si} und \norm{bėide} eine syntaktische Phrase
bilden. Das Pronomen gibt nämlich in diesen Fällen selbst keine formalen
Genus\-merk\-male (mehr) vor, die per \Concord{} verfügbar wären. Etwa drei
Viertel der Targets mit gemischtgeschlechtlichem Bezug (belebt und unbelebt) im
\citetitle{cao} weisen in diesem Kontext Gender Resolution auf (die insgesamt
bedeutend geringere Belegzahl in der \citet{kc} ist nicht aussagekräftig).

Bei eindeutiger Distanzstellung des Targets wurde für das \citetitle{cao}
beobachtet, dass regel\-mäßig Gender Re\-solu\-tion vorliegt. Daraus kann
geschlossen werden, dass die Kongruenz im Sinne der syntaktischen
Konstituentenstruktur in diskontinuierlichen Kontexten über Koindizierung
(\Index) funktioniert. Unter der Hypothese, dass das Neutrum generell mit
Inanimata und im Umkehrschluss die maskulin-feminine Form mit Animata
assoziiert wird \autocite[243--245]{askedal1973}, ist eine auffällige Zunahme
von \norm{bėide} mit belebter kombinierter Referenz bei steigendem
Wortformenabstand nicht zu beobachten. Ohnehin stellte sich lineare Distanz
nicht als ausschlaggebender Faktor für die beob\-ach\-tete Variation heraus.

In wenigen Fällen tritt in beiden Auswertungs\-serien beim belebten, männlichen
Bezug die neutrale Form auf, ohne dass dafür eine systematische Motivation
gefunden werden konnte. \citet{wechslerzlatic2003,wechsler2009} modellieren
Gender Resolution bei Animata mit einer Schnittmengenoperation zwischen den
Sexusmerkmalen der involvierten Referenten. Zwei männliche Referenten bilden
eine Schnittmenge, die regelmäßig auf formaler Ebene das Maskulinum auslöst --
bis auf die genannten Ausnahmefälle.

\subsection{\norm{Bėide} als Quantor: Inanimata}

Bei unbelebtem gemischtem Bezug erfolgt noch regelmäßiger als bei den Animata
die Auflösung zum Neutrum. Allerdings tritt auch hier die Form
\norm{bėidiu} im \citetitle{cao} mehrfach mit Bezug auf kombinierte
maskuline Controller auf, und zwar etwas häufiger als die erwartete Form
\norm{bėide}. Für diese Kombination liegen aber insgesamt nur sechs Belege vor;
kombinierte feminine Controller sind im Belegmaterial keine vorhanden.

In allen anderen Fällen verhalten sich die mittelhochdeutschen Belege wie von
\citet{wechslerzlatic2003,wechsler2009} für das Isländische beschrieben. Das
Neutrum stellt in den untersuchten mittelhochdeutschen Texten das Default dar.
Es tritt immer dann ein, wenn keine Schnittmenge zwischen den jeweiligen
Genusmerkmalen der zu kombinierenden Controller und der Menge der
grammatikalisierten semantischen Genera ($G_s$) gebildet werden kann.

Der Ausnahmefall mit dem Neutrum als Kongruenzform bei kombinierten Maskulina
(sowohl belebten als auch unbelebten) kann mit diesem Ansatz nicht erklärt
werden. Anzunehmen ist, dass die Defaultform als solche übergeneralisiert auch
auf ansonsten unproblematische Kontexte mit kombinierter Referenz angewendet
wird. Diese Möglichkeit wird von \textcites[302]{corbett1991} angeführt,
allerdings im Rahmen seiner Untersuchungen zum
Bosnisch-\allowbreak{}Kroatisch-\allowbreak{}Mazedonisch-\allowbreak{}Serbischen
(BKMS) nur auf Inanimata bezogen
\autocites[vgl.~auch][190]{wechslerzlatic2003}[581]{wechsler2009}.

\subsection{\norm{Bėide} als Konjunktion}

Neben seiner Funktion als Quantor tritt \wdef{beide} im Mittelhochdeutschen
auch als Teil der korrelativen Konjunktion \norm{bėide \dots\ unde}
\wdef{sowohl \dots\ als auch} auf. In beiden Auswertungs\-serien,
\citetitle{cao} und \citet{kc}, treten als Konjunkte in dieser Konstruktion
verschiedene Phrasen\-typen und Wort\-arten auf: solche, die selbst Controller
sind; solche, die Kongruenztargets darstellen; und schließlich solche, die
Personenmerkmale weder definieren noch widerspiegeln.

Die Tatsache, dass \norm{bėide} in den mittelhochdeutschen Belegen auch mit
letzteren vorkommt, wurde als Evidenz für die weit fortgeschrittene
Grammatikalisierung der Konstruktion angesehen. Der Verwendungskontext von
\wdef{beide} ist in diesem Zusammenhang nicht auf seine Bedeutung als Quantor
von Nominalen beschränkt, sondern wurde auf andere syntaktische Zusammenhänge
ausgeweitet. \norm{Bėide} dient hier als Fokuspartikel, die die parallele
Gültigkeit der zwei genannten Optionen betont, nicht mehr als pronominal
verwendeter Quantor, der sich kataphorisch auf seine Referenten bezieht und
deren Zweizahl betont.

Bereits für das Spätalthochdeutsche lassen sich Belege finden, die einen
Übergang zwischen beiden Verwendungsarten andeuten \autocite[vgl.\ die
Beispiele in][627]{ksw2}. Entsprechend der Grammatikalisierungstheorie nach
\citet[146--150]{lehmann2015} geht der Wandel von einem freien Morphem zu einem
funktionalen typischerweise mit dem Verlust von paradigmatischer Variabilität
einher. Es überrascht daher nicht, dass \norm{bėide} in diesem Kontext
erstarrt erscheint, also keine klare Abhängigkeit von den Personenmerkmalen der
Konjunkte ausgemacht werden konnte, falls solche vorhanden sind. Stattdessen
deutete sich an, dass die geografische Verteilung einen wichtigeren Faktor
darstellt. Während \norm{bėidiu} im ganzen oberdeutschen Sprachgebiet auftritt,
liegen Belege für \norm{bėide} hauptsächlich an dessen Rändern vor. Die
Ergebnisse stimmen mit den Angaben von \citet[627--628]{ksw2} überein.

\section{Ausblick}

Generell wäre in dieser Arbeit wünschenswert gewesen, für alle möglichen
Kombinationen von Genera in allen syntaktischen Kontexten mehrere Belege zur
Verfügung zu haben. Dass sich dieser Wunsch nicht erfüllt hat, ist eine
Konsequenz der grundlegenden Herausforderung im Umgang mit historischen Texten
entsprechend dem \citeauthor{labov1994}'schen Diktum, dass die historische
Sprachwissenschaft die Kunst sei, den besten Nutzen aus schlechten Daten zu
ziehen.%
%
	\footnote{\foreignblockcquote{english}[11]{labov1994}{Historical
		linguistics can \textelp{} be thought of as the art of making the best
		use of bad data}.%
	}
%
Die Überlieferung ist letztlich dem Zufall unterworfen und fehlerbehaftet.
Evidenz kann nur indirekt erhoben werden, da es keine Muttersprachlerinnen und
Muttersprachler gibt, die gezielt befragt werden können
\autocite[11]{labov1994}. Gerade digitale Transkriptionen erleichtern es aber
enorm, an Belege und Beispiele zu kommen, selbst wenn keine morphologische oder
sogar syntaktische Annotation vorliegt.
% %
% 	\footnote{Darüber hinaus zeigt zum Beispiel die Arbeit von
% 		\citet{farrisarora2021}, dass schon einfache dependenzgrammatische
% 		Annotationen hilfreich sein können, um gezielt Belege für bestimmte
% 		syntaktische Kontexte zu finden, besonders auch für anscheinend
% 		seltenere syntaktische Kontexte wie \norm{bėide} in Bezug auf zwei
% 		Feminina.%
% 	}
% %
Für sprachgeschichtliche Untersuchungen ist daher wünschenswert,
Transkriptionen digital verfügbar zu machen, wenn sie im Rahmen von
Editionsvorhaben ohnehin anfallen.

Bei der Arbeit an dieser Untersuchung wurde deutlich, wie wichtig eine
sorgfältige Material\-auswahl ist. Es gilt zu bedenken, welche möglichen
morphologischen Kontexte von dem (oder den) untersuchten Text(en) ihrem Inhalt
gemäß abgedeckt werden können. Die Urkunden des \citetitle{cao} haben sich an
dieser Stelle als sehr geeignet erwiesen, da der Genusunterschied in der
Pluralflexion auf das Oberdeutsche begrenzt ist und das Urkundenkorpus gerade
diesen Raum dicht abdeckt. Darüber hinaus fungieren nicht selten Paare aus Mann
und Frau als Aussteller. Die Wortformen \norm{bėide} und \norm{bėidiu} kommen
häufig genug vor, dass eine größere Belegmenge mit Hilfe von regulären
Ausdrücken manuell durchsucht und annotiert werden konnte, andererseits aber
selten genug, dass eine möglichst exhaustive Auswertung in dieser Form möglich
war.

Die \citet{kc} stellte dagegen für die Belegsammlung eine Herausforderung
dar, weil aufgrund ihres Inhalts kaum Paare aus Mann und Frau gemeinsam
auftreten, obwohl einzelne Episoden explizit eine Protagonistin besitzen.
Möglicherweise wäre es aufschlussreicher gewesen, die verschiedenen Textzeugen
eines ähnlich breit überlieferten, erzählenden Texts zu untersuchen -- insofern
sie als digitale Transkriptionen verfügbar sind. Zum Beispiel bietet sich neben
einer Erweiterung von \posscite{askedal1973,askedal1974} Auswertung des
\tit{Tristan} und des \tit{Parzival} auf ihre einzelnen Textzeugen prinzipiell
auch der \tit{Iwein} Hartmanns von Aue an, dessen Überlieferung 33 derzeit
bekannte Textzeugen umfasst \autocites[vgl.][s.\,v.~\textit{Hartmann von Aue:
\tit{Iwein}}]{hsc}.%
%
	\footnote{Transkriptionen der einzelnen Textzeugen werden im Rahmen von
		\citetitle{iwdigital} verfügbar gemacht, siehe unter
		\urlcite{iwdigital}.%
	}

Phänomene im Rahmen von Spaltungskonstruktionen wie zum Beispiel der
Bedeutungs\-unterschied zwischen der Kontaktstellung und der Distanzstellung
von Quantoren sind eine interessante Fragestellung in Hinsicht auf ihre
formal\-syntaktische Modellierung \autocite[siehe
z.\,B.][]{pittner1995,merchant1996,fanselowcavar2002,nolda2007,shen2019}. In
der vorliegenden Auswertung deutete sich an, dass im auch Mittelhochdeutschen
ein grammatischer Unterschied vorliegt, der sich in der anscheinend höheren
Affinität zu semantischer Kongruenz beim distanten Quantor äußert. Denkbar wäre
eine Folgestudie, die explizit \q*{problematische} Controller in diesem Kontext
betrachtet, zum Beispiel Lexical Hybrids wie \norm{wīp} \wdef{Frau (\NeutF)},
und sie mit \q*{unproblematischen} Substantiven vergleicht. Bezüglich des
Bindungsverhaltens von Floating Quantifiers wäre genereller auch der Vergleich
mit Reflexivpronomina interessant, da auch sie sich im Mittelfeld befinden und
mit Subjekten koindiziert sind. Weiterhin könnte es lohnend sein, die
Überlegungen von \citet{spector2009} zur Spaltung von Topik- und
Subjektsfunktion in Konstruktionen mit Floating Quantifier näher zu
untersuchen, auch was die Modellierung des Objektsbezugs durch den Quantor
betrifft.

Darüber hinaus kam zur Sprache, dass sich im Plural der starken
Adjektivdeklination zum Neuhochdeutschen hin das Flexiv \norm{-e} auf alle
Genera ausbreitet \autocite[vgl.][191--192]{ebertetal1993}. Bereits
\citet{askedal1973} stellt dies in den Kontext der Numerusprofilierung.
\citet{dammelgillmann2014} behandeln diesbezüglich die Diachronie des
morphologischen Systems der Substantive, allerdings sind auch Adjektive als
Modifikatoren von Substantiven Teil des Nominalkomplexes. Zu untersuchen wäre
also, wie sich in ihrem Ansatz die Ausbreitung der maskulin-femininen
Flexionsendung in den größeren Kontext des Umbaus der Nominal\-flexion einfügt.

Vieles könnte im Rahmen dieser Untersuchung noch gesagt und präzisiert werden,
doch möchte ich an dieser Stelle einen vorläufigen Punkt setzen und mit Ishmael
schließen: \foreignblockcquote{english}[159]{melville:mobydick}{It was stated
at the outset, that this system would not be here, and at once, perfected. You
cannot but plainly see that I have kept my word.
% But I now leave my cetological System standing thus unfinished, even as the
% great Cathedral of Cologne was left, with the crane still standing upon the
% top of the uncompleted tower. For small erections may be finished by their
% first architects; grand ones, true ones, ever leave the copestone to
% posterity.
\textelp{}
God keep me from ever completing anything. This whole book is but a
draught---nay, but the draught of a draught}. % (Schluss von Kap. 32)
