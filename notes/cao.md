<!--
public.controller.loc IN ('I.179.200.19','I.201.211.25','I.389.359.25','I.519.456.09')
-> Semantik-Annotation ändern zu 3.PL.M

public.controller.loc IN ('II.629.057.25','III.2092.301.24')
-> Semantik-Annotation ändern zu 3.PL.F.A

public.controller.loc IN ('IV.2719.097.01','IV.2719.097.09')
-> Semantik-Annotation ändern zu 3.PL.?.A

2019-10-07: geändert für Controller ('beide'-Targets nicht nötig)
-->

[TOC]

---

# 1 Verteilung der Belege für mhd. *beide/-iu* im Raum

Bei der vorliegenden Auswertung wird das Material des _Corpus der altdeutschen
Originalurkunden bis zum Jahr 1300_ (CAO) zugrunde gelegt. Dieses Material hat
eine Schlagseite zum Westoberdeutschen hin, und hier besonders zum
Oberrheinischen und Hochalemannischen (Schulze 2011: 15). Die folgende Karte
zeigt die Abdeckung des gesammelten Belegmaterials in Bezug auf die Anzahl der
Belege für *beide/-iu* pro Urkundenort (Belege für Controller und intermediäre
Targets wie 'sie' oder 'wir' wurden dabei nicht berücksichtigt).

![Karte der Belegverteilung](grafiken/beide_iu-per-place.png "Karte der Belegverteilung"){width=500dp}

Die Karte zeigt, dass der alemannische Raum -- wie zu erwarten -- breit
abgedeckt wird. Besonders Zürich (13 Belege), Straßburg (11 Belege), Basel (11
Belege) und Freiburg i. Br. (10 Belege) stechen dabei heraus. Im
(Ost-)Schwäbischen dominiert Augsburg (28 Belege); im Bairischen tut sich vor
allem das Mittelbairische mit den großen Städten Wien (15 Belege) und Salzburg
(12 Belege) hervor. Ebenfalls dem Belegmaterial geschuldet ist die Tatsache,
dass die Belegdichte nördlich des Mains gegen Null tendiert, abgesehen von Köln
(7 Belege). Da das untersuchte Phänomen ohnehin nur das Oberdeutsche betrifft
(KSW 2.1: §§ A10--11), stellt die Verteilung der Belege kein größeres Problem
für die Datensammlung dar.

Die acht westmitteldeutschen Belege aus Köln (5 Belege), Sayn (Lkr.
Mayen-Koblenz; 2 Belege) und Veldenz (Lkr. Bernkastel-Wittlich; 1 Beleg) zeigen
keine Variation zwischen *beide*- und *beidiu*-Formen -- wie in der
*Mittelhochdeutschen Grammatik* von Klein, Solms und Wegera für das
Westmitteldeutsche als Regel formuliert (KSW II.1: § A10--11).[^wmd] Diese
Belege werden im weiteren Verlauf ignoriert. Bei der individuellen
Belegbesprechung finden sich im Westoberdeutschen (besonders im Alemannischen)
noch weitere Fälle, in denen am Ort zumindest nach kursorischer Durchsicht der
Belege für *e*- und *iu*-Formen für Urkundentexte am jeweiligen Ort in der
CAO-Datenbank anscheinend keine Variation vorliegt (Becker und Schallert 2020,
in Vorbereitung). Da im weiteren Verlauf aufgrund der Anzahl nicht sämtliche
Belege einzeln besprochen werden können, ist mit weiteren solchen Belegen zu
rechnen. Diese bilden eine Art unvermeidbares Grundrauschen nach dem bekannten
Labov'schen Diktum:

> Historical linguistics can [...] be thought of as the art of making the best
> use of bad data. [...] Our knowledge of what was distinctive and what was not
> is severely limited, since we cannot use the knowledge of native speakers to
> differentiate nondistinctive from distinctive variants. (Labov 1994: 11)

[^wmd]: Im Speziellen handelt es sich dabei um Belege aus den Urkunden Nrn. 78,
501 A/B, 602, 623 und 904.

Folgende zwei Belege wurden bei der nachfolgenden Untersuchung ignoriert, da
nicht ganz klar war, wie sie gewertet werden sollten.

    (?) Jch ſol in auch ir hovs vn̄ ir weingarten / baidev vn̄ ich von [in]
        han vn̄ ich in ['in' über der Zeile nachgetragen] gegeben auch han
        'Ich soll ihnen auch ihr Haus und ihren Weingarten, sowohl so ich von
        [ihnen erhalten] habe als auch so ich ihnen gegeben habe' (CAO II: Nr.
        1436, 639.7--8)

Ob *baidev* sich hier anaphorisch auf *ir hovs vn̄ ir weingarten* bezieht und
vom Relativsatz *vn̄ ich von [in] han* modifiziert wird oder ob sich *baidev*
auf die Relativpartikel *vn̄* bezeiht, ist hier nicht ganz klar. In eindeutigen
Fällen folgt *beide/-iu* gewöhnlich seinem pronominalen Controller. Möglich ist
aber auch die Lesart, bei der eine korrelative Konjunktion *beide ... unde*
vorliegt: 'sowohl die ich von ihnen erhalten habe, als auch (die) ich ihnen
gegeben habe'. Es ist also nicht sicher, ob dieser Beleg gewertet für das zu
untersuchende Phänomen relevant ist.

Der andere Beleg ist für das zu untersuchende Phänomen in jedem Fall
einschlägig, doch besteht hier das Problem der Einordnung nach syntaktischen
Kriterien, insbesondere beim Faktor der Distanz von dem/den Controller(n).

    (?) ſo ſol ich [= Rudger von Aichaim] vn̄ Gerhoh von Radek [...] ze
        Salzburch in varen / vn̄ baide dar vz nimmer chomen         
        'so sollen ich und Gerhoch von Radeck ... nach Salzburg hinein fahren
        und beide nicht mehr dort herauskommen' (CAO II: Nr. 1244, 493,37--40)

Bei diesem Beleg lässt sich dafür argumentieren, dass sich *baide* auf ein
Nullsubjekt bezieht. Die Frage hier ist, ob man von der Existenz von
Nullmorphemen ausgeht oder nicht. Das Konjunkt *baide dar vz nimmer chomen*
enthält ebenfalls kein finites Verb, das -- bei syntaktisch konservativer
Vorgehensweise -- als nächster overter Controller der phonetisch unrealisierten
Subjektsfunktion gezählt werden könnte. Das Verb wird stattdessen genauso wie
das Subjekt des Satzkonjunkts vom ersten Teil impliziert und fällt der Ellipse
anheim.

# 2 Targets in Abhängigkeit der Personenmerkmale des Controllers

Zunächst stellt sich das Problem, dass teilweise in derselben Urkunde alle
möglichen Kombinationen von *die/diu/di* und *sie/siu/si* als direkter
Controller von *beide* bzw. *beidiu* vorkommen. Die gesammelte Belegmenge pro
Ort erlaubt aber in der Regel nicht, alle relevanten Felder des Paradigmas für
die Alternation zwischen *-e* und *-iu* zu füllen (NOM/ACC.SG.F,
NOM/ACC.PL.M/F/N). Alle Urkunden zusätzlich auf die Form des Pronomens der
3SG.F.NOM/ACC und 3PL.M/F/N.NOM/ACC hin auszuwerten, ist aufgrund der
Belegmenge für ein solch hochfrequentes Lexem aufgrund der Bearbeitungszeit
nicht möglich. Diese Voraussetzungen machen es oftmals unmöglich, mit absoluter
Sicherheit zu sagen, welche Position im Paradigma das Resultat der Koordination
zweier NPs in Bezug auf das Genus einnimmt, da das Paradigma nicht für jeden
Ortspunkt einzeln rekonstruiert werden kann (zur Variation bei den
*s*-Pronomina vgl. KSW 2.1: §§ P81--84).[^1]

[^1]: Eventuell könnte man durch Unterteilung der Belege in Wobd./Oobd. etwas
mehr differenzieren, um den jeweiligen Eigenheiten der beiden
Sprachlandschaften besser gerecht zu werden.

Die folgende Tabelle gibt zur Einschätzung des für die vollständige
Aufarbeitung notwendigen Aufwands die Anzahl der Belege nach der automatischen
Annotation der CAO-Belege (vgl. Schmid 2019, im Erscheinen; Becker und
Schallert 2020, in Vorbereitung) für Personalpronomen der angegebenen
Kombination von Person, Genus und Numerus im *ganzen* Corpus an:

| Annotation PPER  | Belege  |
| -----------------|--------:|
| 3SG.F.NOM        |  2.125  |
| 3SG.F.ACC        |    500  |
| 3PL.M.NOM        |  1.036  |
| 3PL.F.NOM        |     11  |
| 3PL.N.NOM        |    144  |
| 3PL.*.NOM        |  7.119  |
| 3PL.M.ACC        |    131  |
| 3PL.F.ACC        |      2  |
| 3PL.N.ACC        |    191  |
| 3PL.*.ACC        |    905  |
| Summe            | 12.164  |

Die automatische Annotation ist basierend auf einer Stichprobe zur Kontrolle
zwar in etwa so zuverlässig wie die manuelle Annotation (Schmid 2019: [3]). Das
WMU gibt im Sammeleintrag "er" für die Pronomen der 3SG insgesamt 41.000 Belege
(als Schätzung?) an (WMU 1: 482).

Es wird daher zum Zweck der Vergleichbarkeit dienlich sein, von der Graphie zu
den grammatikalisch relevanten *semantischen* Merkmalen: Numerus (NUM),
Belebtheit (ANIM), Sexus (SEX) der Kongruenzrelation hin zu abstrahieren,[^2]
da diese in aller Regel durch den Kontext der jeweiligen Urkunde festgemacht
werden können, unabhängig von dem vor Ort vorherrschenden Genussystem.

[^2]: Die [ParGram Feature
Table](https://pargram.w.uib.no/documents/feature-table-no-check/) verwendet
'GEND' und 'GEND-SEM' als Featurebezeichnung. Ich verwende des Weiteren für
Abkürzungen die [Leipzig Glossing
Rules](https://www.eva.mpg.de/lingua/resources/glossing-rules.php) sowie A
('animate') für belebte und I ('inanimate') für unbelebte Referenten, um nicht
mit dem morphologisch-formal neutralen Genus zu kollidieren. Bei der Kategorie
Belebtheit werden zur Unterscheidung außerdem 'männlich' und 'weiblich'
verwendet, bei der Kategorie Genus dagegen 'maskulin' und 'feminin'.

Das Verhalten von *hybrid nouns* (Corbett 2006: XXX) wie *wîp* (weibliches
Geschlecht, neutrales Genus) oder *kint* (männliches oder weibliches
Geschlecht, neutrales Genus) wäre in dem hier untersuchten Zusammenhang
interessant, jedoch liegen keine Belege für ein Hybrid Noun als koordinierter
Controller vor. Im Belegmaterial sind zwar vier Belege für *kint* vorhanden,
diese stehen jedoch stets im Plural in unkoordinierten Zusammenhängen.

## 2.1 Nominale Controller
### 2.1.1 Koordinierte nominale Controller

Zunächst wollen wir *beide/-iu* in direkter Abhängigkeit von koordinierten
nominalen NPs wie *Cuonrat unde Ulrich* näher betrachten, wobei ich
Kombinationen von Pronomen und Eigenname an dieser Stelle mitzähle.

| Controller 1 | Controller 2 | beide  | beidiu |
|--------------|--------------|-------:|-------:|
| 1SG.M        | 3SG.F        |        | 1      |
| 3SG.M        | 3SG.F        | (2)    | 1      |
| 3SG.I        | 3SG.I        | 1      | 3      |

'Beide/-iu' in direkter Abhängigkeit von zwei koordinierten Controllern kommt
nur in 11 Fällen vor; der überwältigende Teil der Belege folgt dem Muster 'A
und B ... sie/wir/die beide'. Insofern zeichnet sich hier kein sehr
aussagekräftiges Bild ab. Dass die Belegzahl hier so niedrig ist, liegt daran,
dass die direkte Modifikation zweier NPs durch *beide/-iu* anscheinend
vermieden wird und *beide/-iu* darüber hinaus nur selten als Pronomen auftritt.
Des Weiteren ist anzumerken, dass keine Belege für belebte Controller mit
neutralem Genus mit direkter Beziehung zu *beide/-iu* als Target vorliegen.

Bei der Aufstellung oben scheint sich lediglich anzudeuten, dass unbelebte
Controller eher eine Form vom Typ *beidiu* nach sich ziehen, im einzelnen
handelt es sich um folgende Kombinationen aus Controller und Target:

    (1)	a. garten        (M) + aker         (M) → beidú  (CAO IV: Nr. 3249, 417.04)
    	b. hof           (M) + zehenden     (M) → beidev (CAO V: Nr. N 241 (616 b), 195.38--39)
    	c. hovs          (N) + weingarten   (M) → baidev (CAO II: Nr. 1436, 639.07)
    	d. Daz Schefwege (N) + daz Hæimpvͦch (N) → baide  (CAO II: Nr. 923, 277.37)

Bemerkenswert ist, dass das grammatische Geschlecht in diesen Fällen keine
Rolle zu spielen scheint, sondern das Bindeglied in der Unbelebtheit der
Konjunkte besteht. Zu (1d) ist vermerkt, dass in dieser Urkunde (CAO II: Nr.
923 aus Kloster Aldersbach) keine Adjektive mit *iu*-Endung vorkommen, jedoch
sowohl *diu* als auch *deu*. Andere Urkunden, die diesem Ort zugeschrieben
werden, enthalten Modifikatoren mit *iu*-Endung:

    (2) a. ebigev rv̂ 'ewige Ruhe' (CAO II: Nr. 1210, 478.25)
        b. alle ſinev chint 'alle seine Kinder' (478.28)
        c. alliv iar 'alle Jahre' (CAO III: Nr. 2346, 457.43/45; fester Ausdruck)
        d. manigev jare 'viele Jahre' (CAO III: Nr. 2428, 501.43)
        e. ſiniv chint 'seine Kinder' (CAO V: Nr. N 434 (1210 a), 321.27)

Behagel (19XX: XXX), Dal (20XX: XXX) und Paul (2007: § X YY) stellen die
Hypothese auf, dass besonders im Fall zweier belebter NPs mit unterschiedlichem
Genus/Geschlecht die morphologisch neutrale Kongruenzform mit *iu* auftritt,
und dies auch bei der Kombination zweier NPs mit gleichem Genus(/Geschlecht?)
möglich ist, allerdings weniger häufig. Für NPs gleichen Geschlechts sieht die
Verteilung folgendermaßen aus -- allerdings auch hier wieder mit der
Einschränkung, dass die Belegzahl nicht sonderlich aussagekräftig ist.

| Controller 1 | Controller 2 | beide  | beidiu |
| ------------ | ------------ | ------:| ------:|
| 1SG.M        | 1SG.M        | 1      |        |
| 1SG.M        | 3SG.M        | 1      |        |
| 3SG.M        | 3SG.M        | 1      |        |
| 3SG.I[M]     | 3SG.I[M]     |        | 2      |
| 3SG.I[N]     | 3SG.I[N]     | 1      |        |

Eine Präferenz für *beide* kann für die drei koordinierten männlichen NPs
beobachtet werden. Die beiden Belege mit maskulinem, unbelebtem Controller
lösen die Form *beidiu* aus; ausgerechnet der Kombination zweier neutraler
Controller folgt allerdings ein Beleg für *beide*. In der betreffenden Urkunde
CAO II (Nr. 923) treten *iu*- und *eu*-Formen nur beim definiten Artikel und
beim kurzen Demonstrativpronomen auf (*div*, *dev*), daneben steht einfaches
*i* (*di*, *ſi*). Ansonsten steht bei adjektivisch deklinierenden Modifikatoren
*e*:
    
    (3) a. ein halbe Hvbe 'eine halbe Hufe [ACC.SG.F]' (277.28)
        b. ditze Geſcheft 'diese Geschäfte [NOM.PL.N]' (278.11)
        c. ditze geſcheft 'diese Geschäfte [ACC.PL.N]' (278.12)

Für die Belege mit Controllern unterschiedlichen Geschlechts bzw. Genus
verteilen sich die Belege folgendermaßen:

| Controller 1 | Controller 2 | beide  | beidiu |
| ------------ | ------------ | ------:| ------:|
| 1SG.M        | 3SG.F        |        | 1      |
| 3SG.M        | 3SG.F        | (2)    | 1      |
| 3SG.I[N]     | 3SG.I[M]     |        | 1      |

Die insgesamt fünf Belege verteilen sich so, dass *beidiu* in allen Fällen
auftritt, jedoch bei der Kombination von männlich und weiblich zumindest
zweimal *beide* auftritt -- diese stammen beide aus der Urkunde CAO IV (Nr.
2748):

    (4) a. Petir vnde vrowa Jrmele bede
           'Peter und Frau Irmela beide' (115.24)

        b. Pe[tir] · vnde vrowe · Jr[mele] ·/ bede
           'Peter und Frau Irmela beide' (115.27--28)

Der Text dieser Urkunde trägt westmitteldeutsche Züge, insofern ist es nicht
verwunderlich, dass keine Belege für *iu*-Formen vorliegen, da hier kein
Unterschied zwischen *e* und *iu* bei der adjektivisch starken Deklination
gemacht wird (KSW II.1: § A10--11).

### 2.1.2 Unkoordinierte nominale Controller

Als Vergleichsmaterial wurden des Weiteren Belege aufgenommen, in denen sich
*beide/-iu* auf eine einzige nominale NP im Plural bezieht. Die belebten
Controller verteilen sich dabei auf nur zwei Annotationstypen:

| Controller | beide | beidiu | beid  |
|------------|------:|-------:|------:|
| 3PL.M      | 21    | (2)    | 4     |
| 3PL.?[N]   |  1    |        |       |

Auffällig ist auch hier die Konzentration auf *beide* bei männlicher Referenz
mit 21 (+ 4) Belegen gegenüber zwei Belegen für *beidiu*:

    (5) a. Vnde hant die bruͦdir von ſant Johanniſ daz ſelbi guͦt / beidú ſamit
           / wider verlúhen Heinrich jrme ſun
           'Und die Brüder von Sankt Johannes haben dasselbe Gut, beide
           zusammen, wieder verliehen an Heinrich, ihren [= Frau Agnes'] Sohn.'
           (CAO I: Nr. 201, 211.25--26)

        b. Diſ dingeſ gezuga ſint · Ruͦd · von der palma min Oͤhen · vͦlrich von
           Grvͤnenberch min Oͤhen bedu Jungherren
           'Zeugen dieser Verhandlung sind: Rüdiger von der Palme, mein Onkel,
           Ulrich von Grünenberg, mein Onkel, beide Junker' (CAO IV: Nr. 2915,
           213.33--35)

In beiden Fällen handelt es sich um Urkunden aus dem alemannischen Sprachraum.
Daher trifft man in CAO I (Nr. 201) aus Freiburg i. Br. (niederalemannisch) als
Endungen *i* und *ú* an (*daz ſelbi guͦt* 211.25, 211.26, 211.28), wobei
zumindest beim Pronomen der 3PL.NOM freie Variation zwischen *ſi* und *ſv̓* zu
herrschen scheint. Es könnte also die für das Alemannische typische Ausweitung
von *siu* auf alle Genera des Plurals vorliegen. In der Urkunde CAO IV (Nr.
2915) aus dem Kloster St. Urban (hochalemannisch) tritt keine Variation
zwischen *e* und *iu* auf, hier ist ansonsten nur *endru guͦt* 'andere Güter'
(ACC.N.SG; 213.27) belegt.

Bei dem einzigen Beleg für *beide* für ein Neutrum, bei dem kein Geschlecht
zugeordnet werden kann, handelt es sich um einen Beleg für *kinde* (6). Die
Namen der Kinder werden dabei nicht ausdrücklich genannt.

    (6) mit miner wirtin vron Clementen vnd vnſer beide kinde hant vnd willen
        'mit Hand und Willen meiner Ehefrau, Frau Clementine, und unser beiden
        Kinder' (CAO III: Nr. 1843, 146.12--13)

Bei den unbelebten Controllern herrscht im Vergleich zu den belebten mehr
Variation, besonders bei morphologischen Neutra; die Form von *beide/-iu*
scheint sich auch hier bei maskuline Controllern nach dem Genus zu richten.

| Controller | beide | beidiu |
|------------|------:|-------:|
| 3PL.I[M]   |  8    |        |
| 3PL.I[N]   | (5)   | 6      |

Bei zweien der Belege, bei denen *beidiu* den Neutra zugeordnet wurde, handelt
es sich um den Controller *jârgezît* (*iargezit*, CAO IV: Nr. 3331, 468.16 und
21). Laut WMU 2 ist dieses Wort ein Neutrum, allerdings mit der Anmerkung, dass
das "Genus häufig nicht bestimmbar" sei: Als weitere Möglichkeit ist noch das
Femininum in Klammern angegeben (956--957).

Auffällig ist, dass bei unbelebten Neutra fünfmal *beide* steht. Sieht man sich
die Belege näher an, stellt man schnell fest, dass zwei Belege aus Kölner
Urkunden stammen (CAO II: 602; 623). In beiden diesen Urkunden liegt keine
Variation zwischen *e*- und *iu*-Formen vor. Dasselbe gilt für die Urkunde CAO
IV (Nr. 2748) von der Burg Altleiningen, die ebenfalls westmitteldeutsches
Gepräge aufweist. Die Schreibsprache der Urkunde CAO IV (Nr. 2841) aus
Hohenrain weist westoberdeutsche (d. h. alemannische) Merkmale auf, allerdings
ergibt die Durchsicht der Urkunden für diesen Ort, dass auch hier sehr
regelmäßig *e* geschrieben wird (neben wenigen Belegen für alemannisches *ú*).
Für den Urkundenort der Urkunde CAO II (Nr. 1584), Herrenchiemsee, tritt trotz
ostoberdeutscher Schreibsprache Variation anscheinend nur beim definiten
Artikel auf.

### 2.1.3 Zusammenfassung

Insgesamt besteht das Problem, dass *beide/-iu* mit direktem Bezug auf nominale
Controller im Urkundenmaterial sehr selten vorkommen. Bei direktem Bezug auf
zwei Substantive als Controller drängt sich aufgrund der Belegmenge sogar der
Eindruck auf, dass dieses Szenario nach Möglichkeit vermieden wird.
Allgemeingültige Aussagen lassen sich so keine machen.

Dessen ungeachtet kann beobachtet werden, dass beim Bezug von *beide/-iu* auf
koordinierte Controllern deren Belebtheit eine Rolle spielt: Bei gleichem
Geschlecht wird bei belebten Controllern die Form *beide* präferiert, bei
unbelebten dagegen die Form *beidiu* unabhängig von deren Genus. Bei
kombinierten Controllern mit unterschiedlichem Geschlecht ist *beidiu*
auch bei belebten Controllern zumindest möglich.

Bei der Auswertung von *beide/-iu* mit direktem Bezug auf ein Plural-Substantiv
als Controller kommen bei den belebten Controllern nur männliche Referenten vor
und ein Beleg für *kinde* ohne Hinweis auf deren Geschlecht. In beiden Fällen
steht *beide*; Belege für *beidiu* stammen aus dem Alemannischen, *iu* könnte
hier also bloße Pluralmarkierung darstellen. Bei unbelebten Controllern fällt
auf, dass maskulin mit *beide* korrespondiert, es aber beim Neutrum Variation
zwischen *beide* und *beidiu* gibt.

## 2.2 Referenzielle Controller
### 2.2.1 Bezug auf koordinierte Substantive

Pronominale Controller wie 'wir' oder 'sie' (PL) referenzieren bereits eine
Kombination von Controllern mit ihren jeweiligen semantischen Features,
weswegen in der folgenden Tabelle Kombinationen als singuläre Controller
verzeichnet sind. In zwei Fällen wurden sogar finite Verben der 3PL als
Controller aufgenommen, da es sich um Pro-drop-Formen handelt, explizite
Subjektspronomen also fehlen:

    (7) a. ſuln 'sollen [3PL.IND.PRES]' (CAO IV: Nr. 3376, 493.21)
        b. hant 'haben [3PL.IND.PRES]' (CAO V: Nr. N 150 (324 b), 108.31)
        c. hant 'haben [3PL.IND.PRES]' (CAO V: Nr. N 202 (492 a), 156.16)

In zwei Fällen ist das Geschlecht des Controllers nicht ermittelbar: In CAO III
(Nr. 1843, 146.13) wird lediglich von *beide kinde* 'beide Kinder' gesprochen,
in CAO I (Nr. 214, 218.18) von *zewain kinden* 'zwei Kindern', ohne dass aus
dem Kontext ersichtlich ist, ob es sich dabei um zwei Jungen, zwei Mädchen oder
einen Jungen und ein Mädchen handelt.

| Controller    | beide | beidiu | beidi | beid  |
|---------------|------:|-------:|------:|------:|
| 1SG.M + 1SG.M | 13    |  1     |       |       |
| 1SG.M + 1SG.F |  5    |  3     | 1     |       |
| 1SG.F + 1SG.M |  2    |        |       |       |
| 1SG.M + 3SG.M |  4    |        |       |       |
| 1SG.M + 3SG.F |  5    | 19     |       |       |
| 1SG.F + 3SG.M |  2    |        | 2     |       |
| 3SG.M + 3SG.M | 13    |        |       | 2     |
| 3SG.M + 3SG.F | 14    | 30     | 1     |       |
| 3SG.F + 3SG.F |  8    |        |       |       |
| 3SG.F + 3SG.M |  1    |  5     | 1     |       |
| 3SG.I + 3SG.I |  2    | 35     |       | 1     |
| 3SG.I + 3PL.I |       |  1     |       |       |

Drei Belege für *beidi* und fünf Belege für *beid* lassen sich nicht in das
Schema *beide*—*beidiu* einordnen. Alle Belege für *beid* und *beidi* im
gesammelten Belegmaterial verteilen sich folgendermaßen auf Urkundenorte:

| Typ   | Beleg | Ort                | Anzahl |
|-------|-------|--------------------|-------:|
| beidi | beidi | Basel              | 2      |
|       |       | Kloster Tenennbach | 1      |
|       | bedi  | Straßburg          | 2      |
| beid  | baid  | Regensburg         | 1      |
|       | beid  | Gurk (Kärnten)     | 1      |
|       | paid  | Heunburg           | 1      |
|       |       | Stift St. Florian  | 1      |
|       | payd  | Stift St. Florian  | 1      |
|       | ped   | Salzburg           | 1      |
|       | peid  | Augsburg           | 1      |

Auffällig dabei ist, dass die Variante *beidi* nur im Niederalemannischen
belegt ist, die Variante *beid* dagegen hauptsächlich im Bairischen. Die
Schreibweise ⟨i⟩ könnte als Entrundung von *iu* aufgefasst werden (Schirmunski
1962: 466--467), *beid* als Resultat der Apokope von mhd. *e*, die im 13. Jh.
zunächst im Bairischen gewirkt hat, bevor sie sich weiter ausbreitete (Lindgren
1953: 178--179).

Zur besseren Übersicht werden die relevanten Belege aus der Tabelle oben hier
noch einmal gesondert dargestellt -- hier zunächst diejenigen Belege, die auf
eine Kombination zweier in ihren Features ungleiche, belebte NPs referieren:

| Controller    | beide | beidiu | beidi |
|---------------|------:|-------:|------:|
| 1SG.M + 1SG.F |  5    |  3     | 1     |
| 1SG.F + 1SG.M |  2    |        |       |
| 1SG.M + 3SG.F |  5    | 19     |       |
| 1SG.F + 3SG.M |  2    |        | 2     |
| 3SG.M + 3SG.F | 14    | 30     | 1     |
| 3SG.F + 3SG.M |  1    |  5     | 1     |

Wie aus der obigen Tabelle zu entnehmen ist, finden sich besonders große
Häufungen von Belegen vom Typ *beidiu* bei denjenigen Controllern, die sich auf
die Kombination von Mann und Frau beziehen und hier insbesondere bei dritten
Personen: Von den 57 (+ 5) Belegen für *beidiu* (gegenüber 29 Belegen für
*beide*) entfallen 35 (+ 2) auf Kombinationen von 3SG.M und 3SG.F, daneben
entfallen noch 19 (+ 2) Belege auf die Kombination von 1SG.M und 3SG.F
beziehungsweise 1SG.F und 3SG.M, also Belege wie der folgende:

    (8) Jch Rvͦdolf von Dúrrenbach [...] **vnde Adelheit** min wirtin [...] daſ
        wir beidú        
        'Ich, Rudolf von Dürrenbach [...] und Adelheit meine Ehefrau [...] dass
        wir beide' (CAO II: Nr. 1154, 432.05--06)

Bei der Kombination von zwei ersten Personen unterschiedlichen Geschlechts
herrscht mehr Variation, insofern *beidiu* in drei Fällen belegt ist, *beide*
dagegen in sieben Fällen. Was die hier beschriebene Variation auslöst oder
zumindest begünstigt, wird Gegenstand weiterer Untersuchungen sein.

Große Variation herrscht gerade bei den Kombinationen von 3SG.M und 3SG.F, wo
den erwähnten 35 Belegen für *beidiu* (37 Belege, wenn man *beidi* als
*iu*-Form auffasst) immerhin 15 für *beide* gegenüberstehen. In all diesen
Fällen handelt es sich beim Controller um eine Pronominalform: ein
Personalpronomen (??? Belege), ein Relativpronomen (??? Belege) oder ein
Demonstrativpronomen (??? Belege). Diese Pronomen markieren Genus overt (*sie*,
*diu*), wo sich keine indifferente Form ausgebildet hat (*si*/*su*, *di*).[^3]
Weitere Untersuchungen sind also auch hier geboten -- wobei nach wie vor das
anfangs erwähnte Problem besteht, dass sich das Genus in vielen Fällen nicht
eindeutig zuordnen lässt.

[^3]: Das Mittelhochdeutsche kennt auch morphologisch indifferente
Relativpartikeln wie *unde* oder *so*. Diese treten im hier gesammelten
Belegmaterial jedoch nur in ??? Fällen auf, sind also vernachlässigbar.

Dem gegenüber verhalten sich die Belege für Pronomen, die eine Kombination
zweier Menschen von gleichem Geschlecht repräsentieren, sehr einheitlich, wenn
auch diese Kombination im Belegmaterial im Vergleich zur
gemischtgeschlechtlichen Referenz nur etwa ein Drittel so häufig vorkommt. Der
Übersicht halber seien die Belege aus der Tabelle oben wie zuvor noch einmal
gesondert aufgelistet:

| Controller    | beide | beidiu | beid  |
| --------------|------:|-------:|------:|
| 1SG.M + 1SG.M | 13    | (1)    |       |
| 1SG.M + 3SG.M |  4    |        |       |
| 3SG.M + 3SG.M | 13    |        | 2     |
| 3SG.F + 3SG.F |  9    |  1     |       |

Eine Form des Typs *beide* ist für 'wir' (1SG.M + 1SG.M), das sich auf zwei
Männer bezieht 13-mal belegt (gegenüber einmal *beidiu*), genauso oft, belegt
ist auch die Kombination zweier pronominaler Referenzen auf zwei männliche
dritte Personen (3SG.M + 3SG.M; noch weitere zwei Male, wenn man *beid* als
Schwa-Apokope interpretiert). Ebenfalls lösen die Kombinationen 1SG.M + 3SG.M
und 3SG.F + 3SG.F vier- beziehungsweise neunmal die Form *beide* aus, doch auch
Pronomen mit Bezug auf zwei weibliche dritte Personen zieht einmal die Form
*beidiu* nach sich. Auch wenn bei diesen Fällen große Regelmäßigkeit herrscht,
ist es dennoch vonnöten, sie in Relation zu den oben besprochenen Fällen von
Variation zu setzen. Damit soll sichergestellt werden, dass die
morphosyntaktischen Voraussetzungen in beiden Fällen dieselben sind.

Bei den beiden Belegen für *beidiu* als Kongruenzform von 1SG.M + 1SG.M und
3SG.F + 3SG.F handelt es sich um die folgenden:

    (9) a. wir abt vollant / vnd Bertholt der prior [...] haben [!] wir [...]
           vnſer bediu inſigel           
           'wir, Abt Volland, und Berthold, der Prior, [...] hängen wir [...]
           unsere beiden (?) Siegel' (CAO II: Nr. 682, 96.03--11)

        b. engiltrvt ir tohtir Vn̄ annvn ir thohtir thohtir [...] ſterbinz
           beidiv
           'Engeltraut, ihrer Tochter, und Anna, der Tochter ihrer Tochter,
           [...] sterben’s beide' (CAO II: 629, 57.24--25)

Im Fall von (9a) ist nicht ganz sicher, worauf sich *bediu* bezieht -- auf Abt
Volland und Prior Berthold, oder auf die Siegel. Letzeres wäre aber durchaus
denkbar; in diesem Fall würde sich *bediu* nach dem N.PL von *insigel*
richten.[^4] (9b) ist dagegen einer der zwei Belege, in denen ein
klitisiertes Pronomen (*-z* ← *si/sie/siu*) den direkten Controller von
*beide/-iu* darstellt.

[^4]: Ein ähnlicher Fall liegt in CAO III (Nr. 1843, 146.13) vor, wo sich
    *beide* dem Kontext nach auf *kinde* beziehen müsste, nicht auf die Eltern:

    ![Kongruenz in CAO III (Nr. 1843, 146.13)](grafiken/1843.png "Kongruenz in CAO III (Nr. 1843, 146.13)"){width=600dp}

    Bemerkenswert ist hier, dass in dieser Urkunde im NOM.PL durchgängig *-v*
    bei Kongruenz von Modifikatoren verwendet wird, allerdings *beide kinde*
    steht:

    * *meͥnv herbvn* 'meine Erben' (NOM.M.PL.ST), 146.28
    * *diſv vorgeſchribenv dingv* 'diese zuvor beschriebenen Dinge' (NOM.PL.N.ST), 146.21

Bisher wurde nur die Variation bei belebten pronominalen Controllern
besprochen. Die unbelebten Controller im Belegmaterial teilen sich wie folgt
nach den Genera ihrer Referenz auf. Zunächst folgt ein Blick auf diejenigen
Belege, die unbelebte Controller haben, die sich auf NPs mit
unterschiedlichem Genus beziehen.

| Controller          | beidiu |
|---------------------|-------:|
| 3SG.I[M] + 3SG.I[F] | 4      |
| 3SG.I[M] + 3SG.I[N] | 1      |
| 3SG.I[N] + 3SG.I[M] | 2      |
| 3SG.I[N] + 3SG.I[F] | 1      |
| 3SG.I[N] + 3PL.I[M] | 2      |

Im Gegensatz zur Diskussion der Belege für Targets von belebten pronominalen
Controllern zeigt sich hier ein einstimmiges Bild: die Kongruenzform lautet
stets *beidiu*. Ähnlich sieht es jedoch auch bei den Kombinationen von
unbelebten Controllern mit Bezug auf NPs von gleichem Genus aus -- auch hier
überwiegt *beidiu* klar neben jeweils einem Beleg für *beide* und *beid*.

| Controller          | beide | beidiu | beid |
|---------------------|------:|-------:|-----:|
| 3SG.I[M] + 3SG.I[M] |  1    |  3     | 1    |
| 3SG.I[N] + 3SG.I[N] |  1    | 24     |      |

Bei den drei abweichenden Belegen handelt es sich um die folgenden:

    (10) a. Da han ich zv gegeben [...] mines aigens [...] einen Weingarten
            [...] Vnd einen Chrvͦtgarten [...] · die geltent beide            
            'Dazu habe ich gegeben ... von meinem Eigen ... einen Weinberg
            und einen Gemüsegarten ..., die bringen beide ein' (CAO III: Nr.
            2396, 484.27--29)

         b. vn̄ ain zehent in der pleſwicz · vn̄ ze Belen ain zehent die paid
            von dē patriarch ze lehent ſint
            'und ein Zehnt in der _pleſwicz_ und in Wöllan ein Zehnt, die beide
            Lehen des Patriarchen sind' (CAO III: Nr. 2401, 487.08--09)

         c. ſwas jch dâ eîgenz hatte. Vnde [...] ſwas min lên da iſt older was.
            Vn̄ han dv̂ bede īme alſo gegeben vn̄ vurlûwen
            'was immer ich da an Eigen hatte, und ... was immer mein Lehen dort
            ist oder war, und habe ihm diese beiden [= Eigen und Lehen] also
            gegeben und verliehen' (CAO V: Nr. N 567 (1686 a), 409.15--17)

Zum Vergleich mit (10a) und (10b) seien Gegenbelege aus denselben Urkunden in
(11a,b) zitiert:

    (11) a. Vnd han div ſelben vor genanten gvͦt elliv driv zelehen enpfangen
            'und habe die selben vorgenannten Güter alle drei als Lehen
            empfangen' (CAO III: Nr. 2396, 484.30--31)

         b. mit allev dew vn̄ dar ze gehoͤrt
            'mit all dem, das dazugehört' (CAO III: Nr. 2401, 487.11)

In CAO III (Nr. 2396) gibt es Variation zwischen *e* und *iu* (10a, 11a), in
CAO III (Nr. 2401) kommt *iu* zumindest in der idiomatischen Wendung *mit alliu
diu* vor (10b, 11b) -- dies stellt allerdings einen Überrest des Instrumentals
dar, keinen NOM oder AKK.PL. Die Interpretation des Belegs aus CAO V (Nr. N 567
(1686 a)) in (10c) ist aufgrund des Satzbaus schwierig und richtet sich hier
nach der zugehörigen Regeste (CAO V: Reg. 124); Variation zwischen *e* und *iu*
bei Modifikatoren gibt es hier keine. Auch in anderen Urkunden aus Rheinfelden
AG gibt es keine entsprechenden *iu*-Belege.

### 2.2.2 Bezug auf unkoordinierte Plural-Substantive

Zuletzt bleiben noch Belege für Targets, die sich auf einen pronominalen
Controller beziehen, der syntaktisch nicht auf zwei koordinierten NPs
rekurriert, sondern auf eine einzige NP im Plural. Diese Belege sollen als
Vergleich für die Belege mit Bezug auf koordinierte NPs dienen. Nachstehend
folgt der relevante Ausschnitt aus der oben gezeigten Tabelle.

| Controller      | beide | beidiu | beid  |
| ----------------|------:| ------:|------:|
| 3PL.M           | 2     |        | 2     |
| 3PL.F           | 3     |        |       |
| 3PL.F[N]        |       | 2      |       |
| 3PL.?[N]        |       | 4      |       |
| 3PL.I[F]        | 3     |        |       |

Bei den Targets mit belebtem Bezug zeigt sich auch hier Variation, die vom
Genus abzuhängen scheint. Im Fall von männlich-maskulinen und
weiblich-femininen Controllern zeigt sich eine Präferenz für *beid(e)*. Bei
Controllern, deren Geschlecht nicht feststellbar ist, oder nicht mit dem Genus
übereinstimmt, die aber allesamt dem Genus nach neutral sind, zeigt sich
Präferenz für die Form *beidiu*. Das Target der drei Belege für
unbelebt-feminine Referenz hat die Form *beide*. Der Kontext für die
inkongruenten oder unklaren Controller wird in den nächsten Beispielen
(12)--(14) ausgeführt.

Zunächst wollen wir uns *beidiu* bei femininer Referenz mit neutralem Genus
widmen. Dies sind in beiden Fällen Belege für *kint*, wobei aus dem Kontext der
Urkunde heraus ersichtlich ist, dass es sich um zwei junge Frauen handelt:

    (12) a. ſweſter Gerdrauden vnd ſweſter Diemvden hern wernhereſ chinden
            [...] vnd ſwenne der vorbenanten chinde einez ſtirbet [...] Di
            weil ſi peidev lebent
            'Schwester Gertraut und Schwester Diemut, Herrn Wernhers Kindern,
            ... Und wenn von den vorgenannten Kindern eines stirbt ... Während
            sie beide leben' (CAO IV: Nr. 2960, 240.31--38)

         b. engiltrvt ir tohtir Vn̄ annvn ir thohtir thohtir [...] vn̄ ſtirpth
            der vor genandon kint eiz [...] ſterbinz beidiv            
            'Engeltraut, ihre Tochter, und Anna, ihrer Tochter Tochter, ... Und
            stirbt der vorgenannten Kinder eines [...] Sterben's beide' (CAO
            II: Nr. 629, 57.24--25)

Die Namen der Kinder sind im Fall von (12) bekannt: In (12a) handelt es sich um
*Gerdrau[t]* und *Diemv[t]*. *[S]i peidev* bezieht sich grammatikalisch dabei
auf die *vorbenanten chinde*, unter welchem die beiden jungen Frauen
zusammengefasst werden. Das Neutrum *chinde* ist in diesem Kontext eindeutig
auf zwei weibliche Personen bezogen. Die Kongruenzform *peidev* richtet sich
dabei nach dem Genus von *chinde*, nicht nach dem Geschlecht der von *chinde*
vertretenen Personen. Genauso verhält es sich in (12b) für Tochter Engeltraut
und Enkelin Anna, die ebenfalls unter 'vorgenannte Kinder' zusammengefasst
werden, auf das sich wiederum *-z beidiv* dem Genus nach bezieht.

In all den Fällen in (13) geht es um Unterhaltsregelungen für die zwei Kinder
eines Paares, wobei die Namen der Kinder im Kontext der Urkunde im Gegensatz zu
den Beispielen in (12) nicht genannt werden. Das Geschlecht der Kinder bleibt
damit unbekannt, es kann nur festgestellt werden, dass es sich um Personen
handelt; 'Kind' ist dabei formal ein Neutrum.

    (13) a. Daz herre Cvͦnrat fledeli vn̄ ſin wirtenne her zem kloſter komen mit 
            zewain kinden [...] vn̄ ſwaz daz gv̂t ze dirbehain giltet daz ſvn ſvͥ 
            beidvͥ han [...] vn̄ ſwen ſvͥ beidvͥ en ſîn           
            'dass Herr Conrad Fledeli und seine Ehefrau zum Kloster herkamen
            mit zwei Kindern ... und was immer das Gut in Dürbheim einbringt,
            das sollen sie [= die Kinder] beide haben ... und wenn sie beide
            nicht [mehr] sind' (CAO I: Nr. 214, 218.17--24)

         b. vnde ſwenne der ſelben chinde einz ſtirbet [...] die wile ſi beidiv
            lebent
            'Und wenn derselben Kinder eines stirbt ... während sie beide
            leben' (CAO IV: Nr. 2719, 96.44--97.1)

         c. vnde daz der chinde einz dannoch lebt oder ſi beidiv
            'und dass eines der Kinder sodann noch lebt, oder sie beide'
            (CAO IV: Nr. 2719, 97.8--9)

In den in (14) aufgeführten Fällen geht es im Gegensatz zu den Beispielen in (12) und (13) um Grundstücke, also unbelebte Dinge.

    (14) a. die matha die ligent bede ze brvnſtat
            'Die Wiesen, die liegen beide in Brunstatt' (CAO III: Nr. 1950,
            212.18)

         b. vn̄ het er ir die ſelben Matta vfgegeben lidig vn̄ lere / vn̄ het ſi
            beide von ir enphangen
            'und er hat ihr dieselben Wiesen ledig und leer überlassen und 
            hat sie beide von ihr empfangen' (CAO IV: Nr. 2733, 105.23--24)

         c. daz wier vnſers rehten aigens auf geben haben vreyleich / vn̄
            vnuerſprochenleich zcuͦ huͦben / die paide gelegent ſint zce duͤlach
            'dass wir von unserem rechtmäßigen Eigen freimütig und
            unangefochten aufgegeben haben zwei Hufen, die beide in Dullach
            gelegen sind' (CAO IV: Nr. 3116, 339.19--20)

Die unbelebten Dinge, die in (14) verhandelt werden, sind alle dem Genus nach
feminin. Bei der Nennung der Anzahl tritt die Form *beide* auf. Dabei liegt in
keiner der Urkunden Variation zwischen Formen mit *e* und *iu* vor, wenn für
Basel (14a), Freiburg i. Br. (14b) und Gornji Grad (14c) auch adjektivische
Modifikatoren mit beiden Endungen belegt sind (wobei nicht überprüft wurde, ob
es Variation innerhalb derselben Urkunden gibt, oder ob diese entweder stets
*e*- oder *iu*-Typen enthalten).

### 2.2.3 Askedals (1973) Hypothese der 'Monoflexion'

Bisher wurden Controller nur abstrakt gemäß ihrer Annotation dargestellt.
Askedal (1973) stellt basierend auf seiner Beleganalyse zu Gottfrids von
Straßburg *Tristan* und Wolframs von Eschenbach *Parzival* die Hypothese auf,
dass es eine Tendenz zur 'Monoflexion' in der Konstruktion *sie beide* gebe, da
Belege vom Typ *si beidiu* -- zumindest in den beiden untersuchten
literarischen Werken in den Ausgaben von Marold und Schröder (1969) und
Lachmann und Hartl (1952) -- auffällig häufig vorkommen.[^5] Im vorliegenden
Belegmaterial zum CAO verteilen sich die Kombinationen (nach Typen
zusammengefasst) wie in der folgenden Tabelle dargestellt. Da im Alemannischen
laut Grammatik typischerweise *sú* für alle drei Genera zu finden ist (KSW 2.1:
§ P76), wird dieses Pronomen getrennt von *siu* aufgeführt (*suͥ* wurde dabei
als Transkriptionsvariante von *sú* aufgefasst).[^6] Um im Zweifelsfall nicht
zu übergeneralisieren, wird *sú* nicht mit *si* zusammengefasst; das Gleiche
gilt für *diu*, *dú* und *di* (*duͥ* ist nicht belegt).

[^5]: Askedal (1973) zitiert die Stellen nach dem Text der jeweiligen Edition,
der normalisiertes Mittelhochdeutsch aufweist, wie in der damaligen
Editionsphilologie üblich. Ohne seine akribische Belegsammlung in Abrede
stellen zu wollen, müssen Askedals (1973) Angaben daher *cum grano salis*
aufgefasst werden, wenn auch die Tatsache, dass Variation auftritt, dafür
spricht, dass die Editoren in diesem Fall Variation in der Morphologie nicht
beseitigt haben. Nichtsdestoweniger verdeckt die Entscheidung für eine
bestimmte Variante bei der Präsentation des normalisierten Editionstexts
mögliche Variation zwischen den verschiedenen Textzeugen. <!-- Gärtner und
Wisbey (1974) merken an, dass es für philologische Arbeiten "heutzutage" kein
Problem sei, "die Sammlung der Textzeugen anhand von Mikrofilmen und Kopien"
vorzunehmen (345). -->

[^6]: Ein Blick in die in Marburg lagernden Fotos der jeweiligen
Originalurkunden würde Klarheit schaffen, ob es sich dabei um genuine
*i*-Superskripte handelt oder lediglich um einen vertikalen Strich, falls sich
diese überhaupt zuverlässig unterscheiden lassen. Die Transkription variiert in
ihrem Versuch des möglichst zeichengetreuen Abdrucks (Wilhelm u.a. 1932:
LX--LXI) zwischen ⟨ú/v́⟩, ⟨v̍⟩, ⟨u̓/v̓⟩, ⟨v͑⟩, ⟨v͗⟩ und ⟨uͥ/vͥ⟩. Teilweise steht
auch einfach nur ⟨u/v⟩, ohne Diakritikum.

| Controller | beide   | beidiu | beidi | beidú | beid |
|------------|--------:|-------:|------:|------:|-----:|
| sie        |  3      |  2     |       | 2     |      |
| siu        |  1 (+1) |  3     |       | 1     | (1)  |
| sú         |  5 (+2) |        |       | 7     |      |
| si         | 18      | 13     |  1    | 8     |      |
| die        |  3      |  1     |       |       | 1    |
| diu        |         | 30     |       |       |      |
| dú         | (1)     |        |       | 3     |      |
| di         |  1      |  1     |       |       |      |

Daneben gibt es noch zwei Belege mit klitisiertem Pronomen, die sich jedoch
strukturell unterscheiden:

    (15) a. ſiz               ped
            ſi=z              ped
            3PL.NOM=3SG.N.ACC beide.NOM.PL
            'sie's beide' (CAO I: Nr. 491, 432.38)
    
         b. ſterbinz                 beidiv
            ſterb-in=z               beid-iv
            sterben-3PL.PRES=3PL.NOM beide-NOM.PL
            'sterben's beide' (CAO II: Nr. 629, 57.25)

In (15a) bezieht sich *ped* auf *ſi*, das Klitikum *-z* steht für das
Objektspersonen *eʒ*. In (15b) dagegen stellt das Klitikum *-z* das
Subjektspronomen der 3PL dar, auf das sich *beidiv* bezieht. Unter Vorbehalt
kann (15a) also dem Fall *si beid* zugerechnet werden. Da in CAO II (Nr. 629)
sonst nur die Form *ſi* für Pronomen der 3PL belegt ist, könnte man (15b)
ebenfalls unter Vorbehalt dem Fall *si beidiu* zurechnen.

In der obigen Tabelle zeigen sich -- wie von Askedal (1973) beobachtet --
starke Häufungen bei *si beide* und *si beidiu*, nicht jedoch bei *di
beide/-iu*. Ob *si beide/-iu* systematisch auftritt, wäre gesondert zu
untersuchen. Nicht auszuschließen ist, dass im Großteil der gesammelten
Urkunden die Form des 3PL-Pronomens einfach schon zu *si* hin ausgeglichen
wurde.[^7] Die Mittelhochdeutsche Grammatik (KSW 2.1: § P76) macht keine
genauen Angaben zum zeitlichen Ablauf dieses Ausgleichs ("spätmhd.").

[^7]: In den Texten, die das ReM (Klein u.a. 2016) dem Oberdeutschen der 2.
Hälfte des 13. Jhs. zuordnet, gibt es für 3PL-Pronomen vom Typ *si* 1870, für
Formen vom Typ *sie* 69 und für Formen vom Typ *siu* 133 Belege (alem. *suͥ*
wird dabei *siu* zugeordnet). Formen vom Typ *si* treten also auch in diesem
Korpus weitaus am häufigsten auf. Die automatische Annotation des ReM
unterliegt manueller Überprüfung (Dipper 2015? Klein und Dipper 2016?), wobei
auch Schmids (2019) Tagger für das ReM eine Zuverlässigkeit von 89,5 % bei
automatischem part-of-speech tagging mit morphologischer Annotation erreicht.
Die Zuverlässigkeit der automatischen Annotation sei daher mit der von
manueller Annotation vergleichbar (Schmid 2019: [3]--[4]).

Von Interesse sind in diesem Fall aber nicht nur Kombinationen, bei denen das
Pronomen unmarkiert ist, sondern auch solche, wo es dem Anschein nach zu
Diskonruenz in der morphologischen Markierung zwischen pronominalem Controller
und *beide/-iu*-Target kommt. Im Folgenden sollen die betreffenden Belege aus
der Tabelle noch einmal nach ihren semantischen Personenmerkmalen
aufgeschlüsselt werden. Eingeklammerte Belege stellen Urkundenorte dar, die
fehlerhaft annotiert sind oder an denen zumindest bei kursorischer Durchsicht
keine Variation zwischen *e* und *iu* in der adjektivisch starken Deklination
ausgemacht werden konnte: Letzeres war der Fall in Schaffhausen, Straßburg und
im Stift St. Florian.

| Wortart | Personenmerkmale    | Controller | beide  | beidiu | beidú | beid |
|---------|---------------------|------------|-------:|-------:|------:|-----:|
| PPER    | 3SG.M + 3SG.F       | sie        |        | 2      | 2     |      |
|         | 3SG.M + 3SG.M       | siu        | (1)    |        |       | (1)  |
|         | 3SG.F + 3SG.F       |            | 1      |        |       |      |
|         | 3SG.M + 3SG.M       | sú         | 1      |        |       |      |
|         | 3SG.M + 3SG.F       |            | 2 (+1) |        |       |      |
|         | 3SG.F + 3SG.F       |            | 2 (+1) |        |       |      |
| DRELS   | 3SG.I[N] + 3PL.I[M] | die        |        | 1      |       |      |
| DDS     | 3SG.I[N] + 3SG.I[N] | dv̂         | (1)    |        |       |      |

Die Karte zeigt die Verteilung der Belege in der obigen Tabelle im Raum. Rot
bezeichnet dabei Varianten von *sie beidiu*, blau Varianten von *siu beide*.

![Karte der Belege für *sie beide* mit inkongruenter Deklination](grafiken/2019-10-28_sie+die_beide_cao_inkongruent.png "Karte der Belege für *sie beide* mit inkongruenter Deklination"){width=500dp}

Sieht man sich die Urkundenorte zu den jeweiligen Belegen an, fällt auf, dass
nahezu alle Belege für Kombinationen vom Typ *sie beidiu* in Regionen östlich
des Alemannischen fallen. Des Weiteren deutet sich in Stams und Heiligkreuztal
an, dass *sie beidiu* eventuell lexikalisiert ist, wobei das adjektivische
Deklinationsparadigma nach grober Durchsicht des CAO ansonsten regelmäßig
erscheint. In CAO IV (Nr. 3045) aus Michelfeld kommt neben *ſie beidv* noch
einmal *ſi* mit gemischtgeschlechtlichem Bezug vor, doch gibt es aus Michelfeld
keine weiteren Urkunden, die als Vergleichsmaterial herangezogen werden könnten.

| Beleg       | Personenmerkmale    | Nummer    | Ort                      | Stelle | Befund
|-------------|---------------------|-----------|--------------------------|--------|-------
| ſie baidiv  | 3SG.M + 3SG.F       | II, 636   | Kl. Stams                | 64.26  | danach nur noch *ſi* mit gleicher Referenz; adj. *e*/*iu* regelmäßig
| ſie baideiw | 3SG.M + 3SG.F       | III, 2367 | Kl. Heiligkreuztal       | 471.30 | in gleicher Zeile noch einmal *ſiw* mit gleicher Referenz; adj. *e*/*iu* regelmäßig
| ſie bedv    | 3SG.M + 3SG.F       | I, 371    | Rothenburg ob der Tauber | 344.12 | regelmäßig *ſie* mit gleicher Referenz, einmal *ſi*; *ir einez* (344.12); adj. *e*/*iu* unregelmäßig
| ſie beidv   | 3SG.M + 3SG.F       | IV, 3045  | Michelfeld               | 293.21 | davor einmal *ſi* mit gleicher Referenz; *ir einſ* (293.20); keine Vergleichsmöglichkeit mit anderen Urkunden
| die beidiv  | 3SG.I[N] + 3PL.I[M] | III, 2033 | Zwiefalten               | 268.11 | Unterschied zwischen *die* und *div* bei Relativpronomen (*alle die echer vn̄ daz holz · div gelegen ſint*, 268.12--13); adj. *e*/*iu* in anderen Urkunden regelmäßig

Die Belege vom Typ *siu/sú beide* kommen dagegen (aufgrund der *ú*-Graphie
erwartungsgemäß) aus dem alemannischen Sprachraum. *Si* und *sú* kommen
zumindest in Urkunden aus Zürich wechselnd vor; weitere Detailuntersuchungen
wären nötig, um das Paradigma zu rekonstruieren. In Bezug auf mehrere Personen
mit unterschiedlichem Geschlecht tritt aber scheinbar regelmäßig *sú* auf.

| Beleg       | Personenmerkmale    | Nummer    | Ort                      | Stelle | Befund
|-------------|---------------------|-----------|--------------------------|--------|-------
| ſiv beide   | 3SG.F + 3SG.F       | II, 1504  | Zürich                   | 679.13 | ansonsten *ſi* (3PL.F[N].NOM, 679.13) für *ſinv̓ kint sweſter Elſbêth vn̄ sweſter Mechthilt* (679.12); adj. *e*/*iu* in anderen Urkunden regelmäßig
| ſv̓ baide    | 3SG.M + 3SG.M       | V, N 401  | Überlingen               | 297.7  | davor einmal *ſi* mit gleicher Referenz (297.06); adj. *e*/*iu* in anderen Urkunden regelmäßig
| ſv̓ beide    | 3SG.M + 3SG.F       | II, 1414  | Zürich                   | 623.26 | regelmäßig *ſv̓* mit gleicher Referenz; *Swenne ſv̓ aber beidv̓ erſterbent* (623.28); adj. *e*/*iu* in anderen Urkunden regelmäßig
| ſv̓ beide    | 3SG.M + 3SG.F       | III, 1717 | Zürich                   | 41.9   | ansonsten *ſi* (3SG.M+3SG.M.NOM, 41.14) für *hern wernher Bibirlin vn̄ hern wernher dem Brechter*; adj. *e*/*iu* in anderen Urkunden regelmäßig
| ſv̓ beide    | 3SG.F + 3SG.F       | II, 1416  | Zürich                   | 624.25 | ansonsten regelmäßig *ſi* für 3SG.F.NOM/ACC; adj. *e*/*iu* in anderen Urkunden regelmäßig
| ſv̓ beide    | 3SG.F + 3SG.F       | III, 1661 | Kl. Kirchberg            | 2.38   | regelmäßig *ſv̓* mit gleicher Referenz; adj. *e*/*iu* in anderen Urkunden regelmäßig

<!--
PPER    II.636.064.26      3SG.M + 3SG.F        ſie    baidiv
PPER    III.2367.471.30    3SG.M + 3SG.F        ſie    baideiw
PPER    I.371.344.12       3SG.M + 3SG.F        ſie    bedv
PPER    IV.3045.293.21     3SG.M + 3SG.F        ſie    beidv

PPER    II.979.317.07      3SG.M + 3SG.M        ſiv    bêde → ungültig?
PPER    IV.2694.083.02     3SG.M + 3SG.M        ſev    paid → keine Variation
PPER    II.1504.679.13     3SG.F + 3SG.F        ſiv    beide

PPER    V.N401.297.07      3SG.M + 3SG.M        ſv̓     baide
PPER    II.1414.623.26     3SG.M + 3SG.F        ſv̓     beide
PPER    III.1717.041.09    3SG.M + 3SG.F        ſv̓     beide
PPER    V.N664.477.12      3SG.M + 3SG.F        ſv     beîde → keine Variation
PPER    II.1416.624.25     3SG.F + 3SG.F        ſv̓     beide
PPER    III.1661.002.38    3SG.F + 3SG.F        ſv̓     beide
PPER    IV.2786.137.35     3SG.F + 3SG.F        ſv̓     bede → ungültig

DRELS   III.2033.268.11    3SG.I[N]+3PL.I[M]    die    beidiv

DDS     V.N567.409.16      3SG.I[N]+3SG.I[N]    dv̂     bede → ungültig
-->

### 2.2.4 Zusammenfassung

Der weitaus größere der Urkundenbelege für *beide/-iu* entfällt auf Targets mit
pronominalem Controller und damit auf eine bloß *indirekte* Abhängigkeit von
koordinierten oder sonstwie kombinierten Controllern. Dies bedeutet jedoch
nicht, dass entsprechende weitere Untersuchungen für diesen syntaktischen
Zusammenhang nicht relevant wären, da auch auf Distanz Variation in der
Genusmarkierung von *beide/-iu* als indirektes Target zweier NPs beobachtet
werden kann. Dies mag zum einen dadurch bedingt sein, dass Pronomina der 3.
Person (*sie/siu*, *die/diu*) Genus als morphologische Kategorie offen
anzeigen. Doch auch bei Pronomen ohne morphologische Genusmarkierung (*ich*,
*wir*, *uns*, *si*, *di*) steht im Hintergrund immer noch die semantische
Referenz insofern dass Pronomen Nominalphrasen koindizieren.

Anhand der Urkundenbelege lässt sich beobachten, dass beim Bezug auf zwei
belebte Substantive von gleichem Geschlecht und Genus die Form *beide* deutlich
überwiegt, beim Bezug auf zwei unbelebte Substantive dagegen die Form *beidiu*,
unabhängig von deren Genus. Beim Bezug auf zwei belebte Substantive von
unterschiedlichem Geschlecht und Genus dagegen liegt Variation zwischen *beide*
und *beidiu* vor. Beim Bezug auf zwei unbelebte Substantive von
unterschiedlichem Genus wird dagegen ebenfalls die Form *beidiu* bevorzugt.

Beim pronominalen Bezug auf unkoordinierte Plural-NPs steht bei belebter
Referenz, bei der Geschlecht und Genus übereinstimmen, ebenfalls *beide*. Dies
ist auch der Fall bei den wenigen Belegen für unbelebte Feminina (*maten*
'Wiesen', *huoben* 'Hufen'). Bei belebten Neutra (*kinde* 'Kinder') wird
dagegen *beidiu* bevorzugt. Insgesamt deutet sich hier also an, dass die
Kategorie Genus in diesen Fällen die größere Rolle spielt.

# 3 Targets in Abhängigkeit der Distanz vom Controller

Bei der Distanz zwischen Controller und Target wurden zwei Arten der
Distanzmessung vorgenommen: zum einen die absolute Distanz in Wörtern zwischen
Controller und Target und zum anderen die syntaktische Distanz zwischen
Controller und Target bezüglich der Lokalität der Kongruenzbeziehung. Bei
letzterem Kriterium wurde unterteilt in: gleiches Satzglied, gleicher Teilsatz,
anderer (Teil-)Satz.[^8] Des Weiteren wurde aufgenommen, ob es sich bei dem
jeweiligen Target um einen floatenden Quantor (*floating quantifier*; Sportiche
1988) handelt. Da floatende Quantoren syntaktisch markiert sind, werden sie
innerhalb des Abschnitts zur syntaktischen Distanz gesondert behandelt.

[^8]: In der Terminologie der LFG würde man von gleicher Funktion, gleichem
F-Strukturkern und anderem F-Strukturkern sprechen (Bresnan u.a. 2016: Kap. 10;
Müller 2019: Kap. 7).

Koordinierte Controller besitzen durch die Linearität von Sprache bedingt
jeweils unterschiedliche Entfernungen zum Target. Außerdem wurden bei der
Belegaufnahme nicht nur koordinierte NPs aufgenommen, sondern auch einige
Fälle, in denen sich ein Target auf zwei Controller bezieht, die zwar relativ
nah beieinander stehen, jedoch nicht syntaktisch koordiniert werden. Dies ist
der Fall beispielsweise in komitativen Kontexten:

    (16) vur Agneſᵢ mit hern Lukasⱼ hant [...] vn̄ hantᵢ₊ⱼ bediᵢ₊ⱼ veriehen
         'Frau Agnes mit Herrn Lukas' Hand [...] und haben beide erklärt'
         (CAO V: Nr. N 202 (492a), 156.11--16)

Für Untersuchung der Verhältnisse bei direkter Abhängigkeit zwischen Controller
und *beide/-iu* spielt dieser Umstand weniger eine Rolle, da in den meisten
Fällen das Target von einem Pronomen abhängt, nicht von zwei Substantiven. Im
Zweifelsfall wird die Distanz beider NPs zum Target angegeben. Da Kongruenz mit
dem entfernteren Controller selten ist (Corbett 2006: XXX, Dammel 20XX: XXX)
und in allen Fällen *beide/-iu* seinen Controllern folgt, sind die Tabellen im
Folgenden nach dem näheren Controller sortiert.

Es wird angenommen, dass die Tendenz zur Kongruenz *ad sensum*, also
pragmatisch nach den semantischen Merkmalen des Bezugsworts, mit wachsender
Entfernung bzw. abnehmender syntaktischer Lokalität zwischen Controller und
Target steigt (Corbett 1979; Wechsler und Zlatić 2003: 83--84; Panther 2009).
Wie wir bereits oben gesehen haben, liegt bei unbelebtem Bezug unabhängig vom
Genus eine Präferenz für die Form *beidiu* vor, insofern gehe ich davon aus,
dass diese Form als [ANIM --] markiert ist. Im Umkehrschluss bedeutet dies,
dass die Form *beide* als [ANIM +] markiert ist, da beim Bezug auf Dinge im
Belegmaterial so gut wie nie diese Form auftritt.

Es lässt sich jedoch beobachten, dass die Form *beidiu* offen ist als
Ausgleichsform für die Kombination unterschiedlicher Geschlechter. Entsprechend
der beobachteten Variation in der Kongruenz bei Targets mit Bezug auf Personen
unterschiedlichen Geschlechts ist in nächster Nähe von Controllern daher sowohl
mit *beide* als auch *beidiu* zu rechnen, während sich auf weitere Distanz vom
Controller die Form *beide* durchsetzen sollte.

| Belebtheit | Geschlecht | Genus | beide | beidiu |
|:----------:|:----------:|:-----:|:-----:|:------:|
| +          | M, M       | M, M  | ×     |        |
| +          | F, F       | F, F  | ×     |        |
| +          | M, F       | M, F  | ×     | ×      |
| --         | Ø, Ø       | *, *  |       | ×      |

## 3.1 Koordinierte nominale Controller

### 3.1.1 Wortdistanz

Da die Belegmenge für *beide/-iu* in direkter Abhängigkeit von koordinierten
Controllern sehr klein ist, ist auch hier die tabellarische Darstellung nach
der Wortdistanz nicht allzu aufschlussreich.[^9] Zu beobachten bleibt einzig,
dass *beidiu* fast ausschließlich bei geringer Wortdistanz zwischen Controller
und Target auftritt, *beide* dagegen bei größerer Distanz:

[^9]: Ein Wortabstand von 0 bedeutet, dass Target und Controller direkt
nebeneinander stehen, 1 bedeutet, dass ein Wort dazwischen steht, etc.

| Wortdistanz 1 | Wortdistanz 2 | beide | beidiu |
|--------------:|--------------:|------:|-------:|
|  0            |  0            |       |      1 |
|  3            |  0            |     2 |      1 |
|  4            |  1            |       |      1 |
|  9            |  3            |       |      1 |
| 20            |  3            |       |      1 |
|  9            |  4            |     2 |        |
| 24            | 24            |     1 |        |
| 30            | 26            |     1 |        |

Wie zuvor ausgeführt, ist es nicht sinnvoll, den Wortabstand des Targets
losgelöst von der Bedeutung seines Controllers zu betrachten. Zunächst folgt
also die Verteilung der Belege für Controller mit persönlichem Bezug in
Abhängigkeit der Distanz in Wörtern zwischen Controller und Target.

| Distanz 1 | Distanz 2 | Controller 1 | Controller 2 | *beide* | *beidiu* |
|----------:|----------:|--------------|--------------|--------:|---------:|
|  3        |  0        | 3SG.M        | 3SG.F        | 2       | 1        |
|  9        |  3        | 1SG.M        | 3SG.F        |         | 1        |
|  9        |  4        | 1SG.M        | 1SG.M        | 1       |          |
| 24        | 24        | 3SG.M        | 3SG.M        | 1       |          |
| 30        | 26        | 1SG.M        | 3SG.M        | 1       |          |

Wie auch zuvor ist die Belegmenge zu klein, um valide Aussagen zu machen,
jedoch deutet sich die erwartete Tendenz zumindest an: Bei geringem Abstand
zwischen Controller und Target treten sowohl *beide* als auch *beidiu* auf, bei
größerem Abstand nur noch *beide*.

Für Targets mit unbelebter Referenz wurde zuvor keine große Variation
beobachtet und das Auftreten von Formen des Typs *beide* war durch Ausgleich in
der Morphologie bedingt. Entsprechend sei die folgende Tabelle nur der
Vollständigkeit halber beinhaltet -- unabhängig von der Distanz zeigt sich
*beidiu*, wobei hier Belege mit größerer Distanz zwischen Controller und Target
fehlen.

| Distanz 1 | Distanz 2 | Controller 1 | Controller 2 | *beide* | *beidiu* |
|----------:|----------:|--------------|--------------|--------:|---------:|
|  0        |  0        | 3SG.I[N]     | 3SG.I[M]     |         | 1        |
|  4        |  1        | 3SG.I[M]     | 3SG.I[M]     |         | 1        |
|  9        |  4        | 3SG.I[N]     | 3SG.I[N]     | 1       |          |
| 20        |  3        | 3SG.I[M]     | 3SG.I[M]     |         | 1        |

Der Beleg mit disparaten Werten zwischen den beiden Controllern lässt sich
durch den Einschub eines Relativsatzes erklären, *beidev* wurde dabei als von
*hof* und *zehenden* kontrolliertes Target analysiert: Hof und Zehnt wechseln
beide in gleicher Weise den Besitzer. Trotz der verschachtelten Satzstruktur
gibt es kein Anzeichen dafür, dass *beidev* hier Teil der korrelativen
Konjunktion *beide ... unde* ist.

    (17) [...] minen hof, den ich ze Jnningen hete, da etwenne der alte
         Stevdler ovf ſaz, verkaufft han mit dem zehenden, der drovz gat,
         beidev vnuerſchaidenlichen fvͤr reht aigen vmb sibenzech phvnt
         Auſpurger phenninge, der ſi mich gewert hant, vn̄ haben in den hof ovf
         geben ich vn̄ min hovſfrowe frow Mehthilt [...]         
         'meinen Hof, den ich in Inningen hatte, auf dem vormals der alte
         Steudler saß, verkauft habe mit dem Zehnt, der daraus hervor geht,
         beide unterschiedslos als rechtmäßiges Eigen, für siebzig Pfund
         Augsburger Pfennige, die sie mir gezahlt haben, und haben ihnen den
         Hof übergeben, ich und meine Ehefrau, Frau Mechthild' (CAO V: Nr.
         N-241-616-b, 195.37--41)

### 3.1.2 Syntaktische Distanz

Für die syntaktische Distanz liegt die folgende Belegverteilung vor. Hierbei
ist -- wie erwartet -- keine Variation in der Lokalität zwischen beiden
Controllern zu beobachten, das heißt, es kann nicht beobachtet werden, dass
beide Controller in unterschiedlichen (Teil-)Sätzen stehen würden. Aufgrund der
geringen Belegmenge sind auch hier keine starken Konzentrationen von Belegen zu
beobachten. Die Menge für Belege vom Typ *beide* nimmt mit wachsender
syntaktischer Distanz ab, während sie für mit Belege vom Typ *beidiu* gleich
bleibt.

| Distanz              | beide | beidiu |
|----------------------|------:|-------:|
| gleiches Satzglied   | 3     | 2      |
| gleicher Teilsatz    | 2     | 2      |
| anderer (Teil-)Satz  | 1     | 2      |

Wie zuvor ist auch bei der Aufschlüsselung der semantischen Annotation der
Controller von *beide* in Relation zur syntaktischen Distanz keine auffällige
Konzentration in der Annotation der Belege zu finden. Aufgrund der geringen
Beleganzahl lässt sich hier keine Tendenz ausmachen, dass die Variation bei
belebtem Bezug zwischen *beide* und *beidiu* mit wachsender syntaktischer
Entfernung zwischen Controller und Target zunimmt.

| Distanz              | Controller 1 | Controller 2 | beide | beidiu |
|----------------------|--------------|--------------|-------|--------|
| gleiches Satzglied   | 3SG.M        | 3SG.M        | 1     |        |
|                      | 3SG.M        | 3SG.F        | 2     | 1      |
| gleicher Teilsatz    | 1SG.M        | 1SG.M        | 1     |        |
|                      | 1SG.M        | 3SG.F        |       | 1      |
| anderer (Teil-)Satz  | 1SG.M        | 3SG.M        | 1     |        |

Wie bei der Aufstellung zur Wortdistanz oben lässt sich beim Bezug auf
unbelebte NPs feststellen, dass *beidiu* auf allen Stufen vertreten ist. Bei
dem einen Beleg für *beide* handelt es sich um den zuvor diskutierten Beleg aus
CAO II (Nr. 923).

| Distanz              | Controller 1 | Controller 2 | beide | beidiu |
|----------------------|--------------|--------------|-------|--------|
| gleiches Satzglied   | 3SG.I[N]     | 3SG.I[M]     |       | 1      |
| gleicher Teilsatz    | 3SG.I[M]     | 3SG.I[M]     |       | 1      |
|                      | 3SG.I[N]     | 3SG.I[N]     | 1     |        |
| anderer (Teil-)Satz  | 3SG.I[M]     | 3SG.I[M]     |       | 2      |

Die wenigen vorhandenen Belege noch weiter aufzuspalten, erscheint wenig
sinnvoll zur Beantwortung der Frage, ob sich floatende Quantoren anders
verhalten als nicht-floatende Quantoren.

## 3.2 Referenzielle Controller

Die Frage, wie sich *beide/-iu*-Targets im Bezug auf die Distanz zu
pronominalen Controllern verhalten, lässt sich zweiteilen: Zum einen kann die
Distanz der direkten Abhängigkeit zwischen pronominalem Controller und Target
untersucht werden, zum anderen kann die Distanz des Pronomens zu seinen
Referenten miteinbezogen werden, also die indirekte Abhängigkeit zwischen den
Erstcontrollern und dem *beide/-iu*-Target.

Im letzteren Fall bilden jedoch ohnehin verschiedene Pronomen den größten Teil
der nicht-kombinierten Controller, die ein *beide/-iu*-Target mit seinen
Erstcontrollern verbinden. Da mit der Wortart des Controllers auch syntaktische
Distanz und damit absolute Distanz vom Erstcontroller einhergeht (vgl. Wechsler
und Zlatić 2003: 84), wird es aussagekräftiger sein, Variation direkt in
Abhängigkeit von der Wortart zu untersuchen, statt den Umweg über die
kumulierte Wortdistanz zu gehen.

| Anzahl | Wortart des des koindizierten Controllers |
|-------:|-------------------------------------------|
| 123    | Personalpronomen                          |
|  36    | Relativpronomen                           |
|   6    | Demonstrativpronomen                      |
|   4    | Reflexivpronomen                          |
|   3    | finites Verb                              |
|   1    | nominalisiertes Adjektiv                  |
|   1    | Possessivpronomen                         |

Zunächst wollen wir unseren Blick aber auf die direkte Abhängigkeit zwischen
pronominalem Controller und Target lenken.

### 3.2.1 Wortdistanz bei direkter Abhängigkeit

Wie zuvor zeigt die folgende Tabelle eine Übersicht über die Verteilung der
verschiedenen Formen des Targets *beide/-iu* in Abhängigkeit von der
Wortdistanz zum jeweiligen pronominalen Controller. Der Controller koindiziert
dabei zwei NPs unabhängig von deren Personenmerkmalen. Aufgrund der geringen
Wortdistanz bei den meisten Belegen ist davon auszugehen, dass der Großteil der
Targets in derselben NP wie ihr Controller stehen.

| Distanz | beide | beidiu  | beidi | beid |
|--------:|------:|--------:|------:|-----:|
|  0      | 58    | 79 (+1) | 5     | 2    |
|  1      |  7    |  7      |       | 1    |
|  2      |  3    |  4      |       |      |
|  3      |       |  2      |       |      |
| 13      |       |  1      |       |      |
| 33      |  1    |         |       |      |

Dabei ist sehr auffällig, dass der weitaus größte Teil der Targets in
unmittelbarer Nachbarschaft des Controllers steht und die Belegzahl schon bei
einem Wort Abstand signifikant abnimmt; auf mittlere und größere Distanz finden
sich dann so gut wie keine Belege mehr. In der folgenden Untersuchung sollen
die Personenmerkmale als weiteres Kriterium mit einbezogen werden.

Zunächst wollen wir uns den kombinierten Controllern mit belebter Referenz und
gleichem Geschlecht widmen. Dabei zeigt sich eine klare Präferenz für die Form
*beide*.

| Distanz | Controller        | beide | beidiu | beid |
|--------:|-------------------|------:|-------:|-----:|
|  0      | 1SG.M + 1SG.M     | 11    | (1)    |      |
|         | 1SG.M + 3SG.M     |  4    |        |      |
|         | 3SG.M + 3SG.M     |  7    |        | 1    |
|         | 3SG.F + 3SG.F     |  8    |        |      |
|  1      | 1SG.M + 1SG.M     |  1    |        |      |
|         | 3SG.M + 3SG.M     |  5    |        | 1    |
|  2      | 3SG.M + 3SG.M     |  2    |        |      |
| 33      | 1SG.M + 1SG.M     |  1    |        |      |

Der Ausreißer *beidiu* entstammt der Urkunde CAO II (Nr. 682), die bereits in
in Beispiel (9a) zitiert und im Anschluss besprochen wurde als fraglich
bezüglich der Referenz von *beidiu* (Abt Volland und Berthold oder *insigel*
als Erstcontroller, letzteres ist wahrscheinlicher). Der Beleg ist daher hier
eingeklammert.

Bei pronominalen Controllern mit belebter Referenz herrscht besonders bei
kombinierten dritten Personen von unterschiedlichem Geschlecht Variation, wie
bereits beobachtet wurde. Diese Targets stehen sämtlich direkt neben ihrem
Controller, wobei in etwa zwei Dritteln der Fälle die Form *beidiu* auftritt.
Wenn erste Personen involviert sind, liegt das Gewicht dagegen auf *beide*.

| Distanz | Controller        | beide | beidiu | beidi |
|----- --:|-------------------|------:|-------:|------:|
|  0      | 1SG.M + 1SG.F     |  4    |  2     | 1     |
|         | 1SG.F + 1SG.M     |  2    |        |       |
|         | 1SG.M + 3SG.F     |  5    |        |       |
|         | 1SG.F + 3SG.M     |  2    | 16     | 2     |
|         | 3SG.M + 3SG.F     | 13    | 25     | 1     |
|         | 3SG.F + 3SG.M     |  1    |  5     | 1     |
|  1      | 3SG.M + 3SG.F     |  1    |  3     |       |
|  2      | 1SG.M + 1SG.F     |  1    |  1     |       |
|         | 1SG.M + 3SG.F     |       |  2     |       |
|         | 3SG.M + 3SG.F     |       |  1     |       |
|  3      | 1SG.M + 3SG.F     |       |  1     |       |
| 13      | 3SG.M + 3SG.F     |       |  1     |       |

Der Verdacht liegt nahe, dass dies damit zu tun haben könnte, dass Pronomen wie
*wir* (32 Belege) und *uns* (5 Belege) im Gegensatz zu *sie* (5 Belege), *siu*
(3 Belege) und *diu* (1 Beleg) nicht nach Genus flektieren, die Flexion von
*beide/-iu* also weniger von der Morphologie des Controllers abhängt als von
der Semantik. Andererseits ist *si* am häufigsten belegt (31 Belege), auf *su*
entfallen immerhin 8 Belege. Variation in der Kongruenzform liegt also vor,
obwohl *si* und *su* unflektiert sind.

Ab einem Abstand von zwei Wörtern stabilisiert sich die Verteilung der wenigen
Belege dahingehend, dass *beidiu* die einzige belegte Form ist. Wenn die
Wahrscheinlichkeit für Kongruenz nach der Semantik mit wachsender Entfernung
des Targets zum Controller zunimmt, fällt der einzige Beleg für einen Abstand
von 13 Wörtern auf, für den eine Form des Typs *beidiu* vermerkt ist (18).

    (18) daz wier hern Rvedgern von Jntzeinſtorf [...], vnt ſein havſvrowen
         froͮn Katherein [...] daz ſi den bedevſamt fverbaz gervechlichen haben
         ſvln vntz an ir beder tot, vnt avch bedev beleiben ſvln in der
         freivnge
         'dass wir Herrn Rüdiger von Inzersdorf und seine Ehefrau, Frau
         Katharina, ... dass sie denn beidesamt fürderhin *gervechlichen* haben
         sollen bis an ihr beider Tot, und auch beide bleiben sollen in der
         Freistätte' (CAO V: N 590 (1769 b), 423.18--26)

In diesem Fall wurde *ſi* im vorhergehenden Satzkonjunkt als Controller gezählt
und *bedev* als sich darauf beziehende Pronominalform. Variation zwischen *e*
und *iu* ist in dieser Urkunde außerhalb von *alle iar* (423.33) keine zu
beobachten, was allerdings auch daran liegt, dass keine weiteren adjektivisch
deklinierenden Modifikatoren mit *e* oder *iu* auftreten, die sich nicht auf
Herrn Rüdiger und Frau Katharina beziehen. Am Urkundenort, Wien, ist Variation
zwischen *e*- und *iu*-Formen ansonsten belegt.

Bei unbelebter Referenz mit gleichem Genus liegen die Belege für *beide/-iu*
tendenziell sogar noch näher an ihrem Controller: Selbst bei einem Abstand von
einem Wort finden sich nur noch drei Belege. Der Großteil der Belege in diesem
syntaktischen Kontext entfällt auf *beidiu*, wie zu erwarten war.

| Distanz | Controller          | beide | beidiu | beid |
|--------:|---------------------|------:|-------:|-----:|
|  0      | 3SG.I[M] + 3SG.I[M] |       |  1     | 1    |
|         | 3SG.I[N] + 3SG.I[N] | 1     | 23     |      |
|  1      | 3SG.I[M] + 3SG.I[M] | 1     |  2     |      |
|         | 3SG.I[N] + 3SG.I[N] |       |  1     |      |

Nichtsdestoweniger finden sich im Belegmaterial auch hier mehrere Belege mit
*beid(e)*. Die jeweiligen Belege wurden bereits oben in Beispiel (10) zitiert
und anschließend besprochen. Dabei wurde festgestellt, dass im Fall des Belegs
für *paid* tatsächlich Variation in der betreffenden Urkunde (CAO III: Nr.
2396) vorliegt, bei den anderen beiden jedoch *beidiu* nur in der feststehenden
Wendung *mit alliu diu* auftritt (CAO III: Nr. 2401, 487.11) und in der
Urkunde, in der der andere Beleg für *beide* enthalten ist (CAO V: Nr. N 567
(1686 a)), keine Variation herrscht.

Im Fall von unbelebter Referenz mit unterschiedlichem Genus ist die Variation
ähnlich klein wie bei unbelebter Referenz mit gleichem Genus. Hier liegen sogar
ausschließlich Belege vom Typ *beidiu* vor, die ebenfalls nur in sehr geringer
Distanz zu ihrem Controller stehen.

| Distanz | Controller          | beidiu |
|--------:|---------------------|-------:|
|  0      | 3SG.I[M] + 3SG.I[F] | 3      |
|         | 3SG.I[M] + 3SG.I[N] | 3      |
|         | 3SG.I[N] + 3SG.I[F] | 1      |
|  1      | 3SG.I[M] + 3SG.I[F] | 1      |
|  3      | 3SG.I[N] + 3PL.I[M] | 1      |

### 3.2.2 Syntaktische Distanz bei direkter Abhängigkeit

Im letzten Abschnitt wurden die einzelnen *beide-/iu*-Targets und ihre
pronominalen Controller nach dem Wortabstand untersucht. Dabei hat sich vor
allem bei Controllern, die sich auf zwei Personen unterschiedlichen Geschlechts
beziehen und deren Target in unmittelbarer Distanz zu seinem Controller steht,
Variation zwischen *beide* und *beidiu* gezeigt. In anderen Fällen zeigte sich
*beidiu* als die bevorzugte Form. 

Die folgende Tabelle weist die Belegverteilung nach syntaktischem Abstand
beziehungsweise sinkender Lokalität der Kongruenzbeziehung aus. Auch hier ist
deutlich sichtbar, dass die allermeisten Targets im gleichen Satzglied wie ihr
Controller stehen, und meistenteils in unmittelbarer Nähe. Bei Targets im
gleichen Teilsatz aber unterschiedlichem Satzglied konzentrieren sich die
Belege bereits auf *beidiu*. In diese Distanzkategorie fallen auch alle 53
Belege, die als floatende Quantoren verzeichnet sind und weiter unten
diskutiert werden.

| Distanz             |       | beide | beidiu | beidi | beid |
|---------------------|:-----:|------:|-------:|------:|-----:|
| gleiches Satzglied  | 0.. 2 | 58    | 52     | 4     | 1    |
| gleicher Teilsatz   | 0..13 | 10    | 42     | 1     | 2    |
| anderer (Teil-)Satz |   33? | *1    |        |       |      |

Bei dem einen Beleg für *beide*, der als sich in einem anderen (Teil-)Satz
befindlich annotiert ist, handelt es sich um den folgenden:

    (19) Wir Otto von Ohſſinſtein / vn̄ Otte der Lantvogt ſin ſvn veriehent daz
         dis war ſi daz hie geſchriben ſtat / vn̄ gelobendes ſtête zvͦ habenne
         mit gvͦten druwen ane alle geverde / vn̄ henkent dar vmbe bêide vnſer
         Jngeſigel an diſen selben brief zvͦ eime vrkvnde
         'Wir, Otte von Ochsenstein und Otte, der Landvogt, sein Sohn,
         verkünden, dass wahr sei, was hier geschrieben steht, und geloben, es
         mit aufrichtiger Treue einzuhalten, ohne irgendwelche betrügerischen
         Absichten, und hängen darum beide unser Siegel an diese selbe Urkunde
         zu einem Zeugnis' (CAO II, Nr. 1145, 427.3--5)

**PROBLEM:** *Beide* (427.5) wurde hier als sich direkt auf *Wir* (427.3)
beziehend gewertet. Bei dieser Passage handelt es sich um einen langen Satz,
dessen Teilsätze mit *unde* verbunden sind. Jeder dieser Teilsätze weist ein
finites Verb auf, sodass *bêide* in seinem Satz an sich als Modifikator eines
leeren Subjektspronomen gewertet werden kann, da *wir* in jedem Satzkonjunkt
elidiert wird. Der syntaktische Abstand sinkt daher nach der Phrasenstruktur
auf 'gleicher Teilsatz', außerdem handelt es sich bei *bêide* um einen
gefloateten Quantor (bei der Topikalisierung der Subjekts-NP bleibt *bêide* in
SpecVP zurück). Die Frage ist, wie der Wortabstand in diesem Fall gewertet
wird (3 statt 33?):

    (20)                       CP
                      .--------+---------.
                      |                  |
                      NP                 C'
                      |          .-------'-------.
                      |          |               |
                      |          C'              VP
                      |      .---'---.       .---'---.
                      |      |       |       |       |
                      |      |      AdvP     NP      V'
                      |      |    .--'---.   |     .-'-.
                      |      |    |      |   |     |   |
                      |      |    |      |   AP    |   |
                      |      |    |      |   |     |   |
                      |      |    |      |   |     |   |
                      N⁰     C⁰   |      |   A⁰    |   |
                      |      |    |______|   |     |___|
         Wirᵢ ... vn̄  _ᵢ  henkent dar vmbe bêideᵢ   ...
          |           |                      |
          |           '--0-------1---2----3--|
          '-···-29--Ø---30------31--32---33--'

Wie auch zuvor zeigt sich bei gleichem Geschlecht der belebten Referenz des
pronominalen Controllers eine ausgesprochene Präferenz für *beide*, wobei zu
beachten ist, dass die Belegmenge nach 'gleiches Satzglied' extrem abnimmt.

| Distanz             | Controller        | beide | beidiu | beid |
|---------------------|-------------------|------:|-------:|-----:|
| gleiches Satzglied  | 1SG.M + 1SG.M     | 11    | (1)    |      |
|                     | 1SG.M + 3SG.M     |  4    |        |      |
|                     | 3SG.M + 3SG.M     |  9    |        |      |
|                     | 3SG.F + 3SG.F     |  7    |        |      |
| gleicher Teilsatz   | 1SG.M + 1SG.M     |  1    |        |      |
|                     | 3SG.M + 3SG.M     |  5    |        | 1    |
|                     | 3SG.F + 3SG.F     |  1    |        |      |
| anderer (Teil-)Satz | 1SG.M + 1SG.M     | *1    |        |      |

Im Fall des pronominalen Bezugs des Controllers von *beide/-iu* auf zwei
belebte Erstcontroller mit verschiedenem Geschlecht bestätigt sich die Annahme:
In der Tat überschreitet die Kongruenzbeziehung die Grenze des Satzglieds
nicht, und Konzentration auf *beidiu* ist erst bei der nächsten Stufe,
'gleicher Teilsatz' zu beobachten. Ansonsten herrscht innerhalb der Phrase
Variation wie bereits im letzten Abschnitt beschrieben.

| Distanz             | Controller        | beide | beidiu | beidi |
|---------------------|-------------------|------:|-------:|------:|
| gleiches Satzglied  | 1SG.M + 1SG.F     |  3    |  2     | 1     |
|                     | 1SG.F + 1SG.M     |  2    |        |       |
|                     | 1SG.M + 3SG.F     |  5    | 17     |       |
|                     | 1SG.F + 3SG.M     |  2    |        | 2     |
|                     | 3SG.M + 3SG.F     | 14    | 25     | 1     |
|                     | 3SG.F + 3SG.M     |  1    |  5     |       |
| gleicher Teilsatz   | 1SG.M + 1SG.F     |  2    |  1     |       |
|                     | 1SG.M + 3SG.F     |       |  2     |       |
|                     | 3SG.F + 3SG.M     |       |        | 1     |
|                     | 3SG.M + 3SG.F     |       |  5     |       |

Im Fall der Belege für pronominalen Bezug auf unbelebte Erstcontroller von
gleichem Genus bleibt ebenfalls alles wie bereits beobachtet: *beidiu* ist klar
die präferierte Form. Anzumerken ist lediglich, dass in diese Fall nahezu keine
Belege für *beide/-iu* im gleichen Satzglied vorliegen.

| Distanz            | Controller          | beide | beidiu | beidi | beid |
|--------------------|---------------------|------:|-------:|------:|-----:|
| gleiches Satzglied | 3SG.I[M] + 3SG.I[M] |       |  1     |       |      |
|                    | 3SG.I[N] + 3SG.I[N] | 1     |        |       |      |
| gleicher Teilsatz  | 3SG.I[M] + 3SG.I[M] | 1     |  2     |       | 1    |
|                    | 3SG.I[N] + 3SG.I[N] |       | 24     |       |      |

Auch wenn das Genus der unbelebten Referenzen divergiert, ist *beidiu* die
einzig belegte Kongruenzform des Targets. Hier gibt es immerhin drei Belege
für ein Target, das im gleichen Satzglied wie sein Controller steht.

| Distanz            | Controller          | beidiu |
|--------------------|---------------------|-------:|
| gleiches Satzglied | 3SG.I[M] + 3SG.I[F] | 1      |
|                    | 3SG.I[M] + 3SG.I[N] | 1      |
|                    | 3SG.I[N] + 3SG.I[M] | 1      |
| gleicher Teilsatz  | 3SG.I[M] + 3SG.I[F] | 3      |
|                    | 3SG.I[N] + 3SG.I[M] | 1      |
|                    | 3SG.I[N] + 3SG.I[F] | 1      |

## 3.3 Variation bei gefloateten Quantoren

Generell fällt bei dem hier vorgenommenen Schnitt durch die Daten auf, dass
nahezu alle Belege für 'gleicher Teilsatz' als gefloatet markiert sind,
unabhängig von der Konfiguration der Personenmerkmale. Gefloatete
*beide/-iu*-Belege wurden deshalb als sich im 'gleichen Teilsatz' befindlich
markiert, weil sie zwar auf das Satzsubjekt bezogen sind und unter einer
funktionalen Betrachtungsweise auch Teil der SUBJ-Funktion sein sollten, sich
jedoch nicht wie das Satzsubjekt in der Subjekts-NP unter CP befinden, sondern
unter VP. Natürlich haben wir keine Mittelhochdeutsch-Muttersprachler mehr, die
man zwecks einer Umstellprobe befragen könnte, allerdings müsste sich modernes
Standarddeutsch in etwa folgendermaßen verhalten (vgl. Sportiche 1988):

    (21)             CP
          .----------'----------.
          |                     |
          DP                    C'
          |                     |
          |                     C'
          |         .-----------'----------.
          |         |                      |
          |         |                      VP
          |         |           .----------'----------.
          |         |           |                     |
          |         |           DP                    V'
          |         |        .--'--.                  |
          |         |        |     |                  V'
          |         |        D'    DP           .-----'-----.
          |         |        |     |            |           | 
          |         |        D'    |            NP          | 
          |         |        |     |            |           | 
          |         |        |     |        .---'---.       |
          |         |        |     |        |       |       |
          |         |        |     |        DP      N'      |
          |         |        |     |        |       |       |
          |         |        |     |        |       N'      |
          |         |        |     |        |       |       |
          D⁰        C⁰       D⁰    D⁰       D⁰      N⁰      V⁰
          |         |        |     |        |       |       |
         Sieᵢ   schreibenⱼ   _ᵢ  beide    eine Dissertation _ⱼ

         a.  Eine Dissertation schreiben sie beide
            *Eine Dissertation schreiben beide sie
         b.  Was sie beide schreiben, ist eine Dissertation
            *Was sie eine Dissertation schreiben, ist beide
            *Was sie schreiben, ist beide eine Dissertation

---

* C-Struktur so korrekt? Ist 'beide' in diesen Fällen Adjektiv oder Pronomen,
Modifikator oder Apposition? (OTOH, wenn es eine Apposition wäre, wäre es dann
trennbar?!)
* Oli meinte, ich sollte nach "gespaltene Topikalisierung" suchen, Gisbert 
Fanselow hat da einschlägige Dinge zu geschrieben. Per Fernleihe bestellt:
    * Ott, Dennis. 2012. Local instability: Split topicalization and quantifier
      float in German. Linguistische Arbeiten 544. Berlin: de Gruyter.
    * Fanselow, Gisbert, und Damir Ćavar. 2002. Distributed Deletion. In
      Theoretical approaches to universals. Hrsg. von Artemis Alexiadou.
      Amsterdam: Benjamins. doi:10.1075/la.49.05fan.

---

Vergleicht man die Annotation der Belege für pronominalen Bezug des Controllers
auf kombinierte belebte NPs mit gleichem Geschlecht, ergibt sich kein
Unterschied: In allen Fällen tritt *beid(e)* auf; der einzige Beleg für
*beidiu* wurde zuvor schon als nicht aussagekräftig bewertet.

| gefloatet | Distanz             | Controller        | beide | beidiu | beid |
|:---------:|---------------------|-------------------|------:|-------:|-----:|
| --        | gleiches Satzglied  | 1SG.M + 1SG.M     | 11    | (1)    |      |
|           |                     | 1SG.M + 3SG.M     |  4    |        |      |
|           |                     | 3SG.M + 3SG.M     |  7    |        | 1    |
|           |                     | 3SG.F + 3SG.F     |  7    |        |      |
| +         | gleicher Teilsatz   | 1SG.M + 1SG.M     |  2    |        |      |
|           |                     | 3SG.M + 3SG.M     |  6    |        | 1    |
|           |                     | 3SG.F + 3SG.F     |  1    |        |      |

Im Fall des Bezugs auf Personen mit verschiedenem Geschlecht zeigt sich, dass
die beobachtete Konzentration auf *beidiu* bei größerer syntaktischer Distanz
als 'gleiches Satzglied' anscheinend mit Quantifier-Float einhergeht. Hier wäre
es wünschenswert, mehr Daten zu haben, um mehr als eine Annahme formulieren zu
können. Das Fehlen von Variation bei der Kombination von männlicher und
weiblicher Referenz ist jedenfalls auffällig -- allerdings liegen für beide
Kombinationsmöglichkeiten bei gefloatetem *beidiu* nur 5 (+ 1) Belege vor,
während es bei nicht-gefloatetem *beide/-iu* 30 (+ 1) Belege für *beidiu* im
gleichen Satzglied gibt, mit *beide* zusammen sogar insgesamt 46.

| gefloatet | Distanz            | Controller        | beide | beidiu | beidi |
|:---------:|--------------------|-------------------|------:|-------:|------:|
| --        | gleiches Satzglied | 1SG.M + 1SG.F     |  3    |  2     | 1     |
|           |                    | 1SG.F + 1SG.M     |  2    |        |       |
|           |                    | 1SG.M + 3SG.F     |  5    | 16     |       |
|           |                    | 1SG.F + 3SG.M     |  2    |        | 2     |
|           |                    | 3SG.M + 3SG.F     | 14    | 25     | 1     |
|           |                    | 3SG.F + 3SG.M     |  1    |  5     |       |
| +         | gleicher Teilsatz  | 1SG.M + 1SG.F     |  2    |  1     |       |
|           |                    | 1SG.M + 3SG.F     |       |  3     |       |
|           |                    | 3SG.M + 3SG.F     |       |  5     |       |
|           |                    | 3SG.F + 3SG.M     |       |        | 1     |

In (22) werden die drei Belege für gefloatetes *beide* bzw. *beidiu* in
Abhängigkeit eines Pronomens mit Bezug auf kombinierte erste Personen
unterschiedlichen Geschlechts gezeigt.

    (22) a. Johannes Vende [...] vn̄ Grede [...] Wir verſchieſſent oͧch bede
            'Johannes Vende ... und Grete ... Wir veräußern auch beide' (CAO V,
            Nr. N 100 (144a), 69.40--71.21)

         b. Wir loben ovch paide, ich Herman von Hipleinſtorf vnd ich Agnes
            ſein hauſvrowe            
            'Wir geloben auch beide -- ich, Herman von Hipleinstorf, und ich,
            Agnes, seine Ehefrau' (CAO V, Nr. N 701 (2149b), 506.32--33)

         c. wîr dú vorgenanten herre Otto / vn̄ fro Berchte / [...] wîr heîn
            och beîdú [...] gelobet                        
            'Wir, die vorgennanten, Herr Otto und Frau Bertha, [...] wir haben
            auch beide [...] gelobt' (CAO IV, Nr. 2931, 223.1--6)

Die Urkunde des Belegs (22a) stammt aus Straßburg und typisch für diesen Ort
wird in dieser Urkunde ausschließlich die Form *bede* verwendet. In dieser
Urkunde steht auch sonst unterschiedslos *e*. Belege mit *iu* sind für
Straßburg nur schwach repräsentiert, eher steht *i* oder *e*. Im Fall der
Urkunde zu Beleg (22b) liegt keine Variation zwischen *e* und *iu*-Typen vor,
obwohl für Wiener Urkunden Variation diesbezüglich generell belegt ist. Neben
dem Beleg für *beîdú* in (22c) zeigt die dazugehörige Urkunde aus Basel keine
weiteren Belege mit *ú*; bei dem einzigen anderen adjektivisch deklinierenden
Modifikator steht regulär *e* (*anderre biderber lúton genuͦge*, 223.11). Für
Basel sind ansonsten ebenfalls sowohl *iu*-Belege als auch *e*-Belege generell
vorhanden. Insgesamt sollte diesen drei Belegen also nicht zu viel Aussagekraft
zugemessen werden.

Bei *beide/-iu* in Abhängigkeit eines Pronomens, das sich auf zwei unbelebte
Substantive mit gleichem Genus bezieht, liegen für ungefloatete Targets
generell nur drei Belege vor. Das Gros der Belege entfällt auf gefloatete
Targets, wobei die Form *beidiu* unabhängig vom Genus der Erstcontroller die
Regel darstellt. Alle drei Belege für *beid(e)* wurden bereits in (10) zitiert
und anschließend diskutiert: Zumindest der eingeklammerte Beleg für
ungefloatetes *beide* mit Bezug auf zwei unbelebte Maskulina ist bezüglich der
Variation zwischen *e* und *iu* vermutlich als neutral zu werten.

| gefloatet | Distanz            | Controller         | beide | beidiu | beid |
|:---------:|--------------------|--------------------|------:|-------:|-----:|
| --        | gleiches Satzglied | 3SG.I[M] + 3SG.I[M]|       |  2     |      |
|           |                    | 3SG.I[N] + 3SG.I[N]| (1)   |        |      |
| +         | gleicher Teilsatz  | 3SG.I[M] + 3SG.I[M]| 1     |  2     | 1    |
|           |                    | 3SG.I[N] + 3SG.I[N]|       | 24     |      |

Ähnlich wie in der vorigen Tabelle sieht es auch bei der Belegpräsentation für
*beide/-iu* in Abhängigkeit von pronominalen Controllern aus, die sich auf zwei
unbelebte Substantive unterschiedlichen Geschlechts beziehen. Zwar entfallen
sämtliche Belege für das Target auf den Typ *beidiu*, doch auch hier ist die
Beleglage für ungefloatetes *beidiu* nur sehr dünn.

| gefloatet | Distanz             | Controller          | beidiu |
|:---------:|---------------------|---------------------|-------:|
| --        | gleiches Satzglied  | 3SG.I[M] + 3SG.I[F] |  1     |
|           |                     | 3SG.I[M] + 3SG.I[N] |  1     |
|           |                     | 3SG.I[N] + 3SG.I[M] |  1     |
| +         | gleicher Teilsatz   | 3SG.I[M] + 3SG.I[F] |  3     |
|           |                     | 3SG.I[N] + 3SG.I[M] |  1     |
|           |                     | 3SG.I[N] + 3SG.I[F] |  1     |
|           |                     | 3SG.I[N] + 3PL.I[M] |  1     |

Insgesamt ist also festzustellen, dass -- abgesehen von der teils prekären
Beleglage -- sich zumindest tendenziell andeutet, dass in Abhängigkeit von
pronominalen Controllern mit kombiniertem Bezug auf unbelebte Substantive auch
unter dem Gesichtspunkt des Quantifier-Float generell *beidiu* steht. Genauso
steht beim kombinierten Bezug auf belebte Substantive *beide*, wenn diese das
gleiche Geschlecht haben.

Beim kombinierten Bezug auf belebte Substantive mit unterschiedlichem
Geschlecht hat sich dagegen angedeutet, dass beim ungefloateten Quantor
Variation zwischen *beide* und *beidiu* herrscht, beim gefloateten dagegen
ebenfalls *beidiu* bevorzugt wird. Gerade im letzeren Fall wären mehr Belege
jedoch wünschenswert gewesen, um ein klareres Bild zu erhalten.

## 3.4 Wortdistanz bei indirekter Abhängigkeit

Da extrem viele Belege für *beide/-iu* direkt nach einem Pronomen stehen, ist
die Aussagekraft dieser Statistik nur bedingt nützlich. Alle diese Belege mit
ihren absoluten Entfernungen zu ihren Erstcontrollern aufzulisten wäre ebenso
müßig. Daher wurden im Folgenden die absoluten Wortentfernungen zwischen dem
ersten Erstcontroller (der zweite Erstcontroller steht gewöhnlich nicht allzu
weit dahinter) und dem *beide/-iu*-Target aufsummiert. Dies liefert zwar nicht
in jedem Fall hundertprozentig korrekte Werte, da in wenigen Fällen ein
Pronomen als Bindeglied vor seinem Controller steht. Nichtsdestoweniger ergeben
sich durch die Aufsummierung Näherungswerte zur Entfernung.

Da es für alle Entfernungswerte nur einen einzigen Beleg gibt, ist es sinnvoll,
Klassen zu bilden. die Entfernungswerte folgen dabei in etwa der Zipf'schen
Verteilung, daher wurde die Formel zur Wortklassenbildung auf die
Wortentfernung angewandt (Logarithmus zur Basis 2 der absoluten Häufigkeit des
größten Werts geteilt durch den Wert des jeweiligen Elements, jeweils gerundet
auf die nächste ganze Zahl). Je höher der resultierende Klassenwert ist, desto
näher liegt das *beide/-iu*-Target beim Erstcontroller. Des Weiteren werden die
verschiedenen Flexionstypen zusammengefasst: *e* und *Ø* bilden zusammen die
eine Klasse; *iu*, *ú* und *i* die andere.

---

**PROBLEM:** Ich kenne mich mit Statistik nicht gut genug aus, um zu wissen, was hier ein sinnvolles Vorgehen zur Klasseneinteilung ist.

---

Trägt man nun die Belege für gleichgeschlechtliche kombinierte Controller und
ihre Entfernung zum jeweiligen *beide/-iu*-Target auf, zeigt sich deutlich,
dass Belege vom Typ *e* (bzw. *Ø*) klar dominieren. Die meisten Belege liegen
in oberer mittlerer Distanz vor, zwischen 30 und 48 Wörtern (der Maximalwert
ist 334 Wörter).

![Statistik: Variation der Flexion von *beide/-iu* nach Wortdistanz (gleichgeschlechtliche Erstcontroller)](grafiken/2019-10-26_beide_cao_dist_m+m_f+f.png "Statistik: Variation der Flexion von *beide/-iu* nach Wortdistanz (gleichgeschlechtliche Erstcontroller)"){width=500dp}

Bei den gemischtgeschlechtlichen Erstcontrollern dominiert zunächst *beide* bei
einer Entfernung zwischen 5 und 9 Wörtern; danach wächst die Zahl der Belege
für *beidiu* jedoch rapide und erreicht ihren Höhepunkt bei einer Entfernung
zwischen 41 und 78 Belegen, wobei *iu*-Belege häufiger als *e*-Belege bleiben.
Der Maximalwert beträgt 443 Wörter.

![Statistik: Variation der Flexion von *beide/-iu* nach Wortdistanz (verschiedengeschlechtliche Erstcontroller)](grafiken/2019-10-26_beide_cao_dist_m+f_f+m.png "Statistik: Variation der Flexion von *beide/-iu* nach Wortdistanz (verschiedengeschlechtliche Erstcontroller)"){width=500dp}

Bei den unbelebten Erstcontrollern dominiert *beidiu* als Target und erreicht
seinen Höhepunkt bei einer Distanz zwischen 5 und 9 Wörtern -- in den meisten
Fällen handelt es sich um *insigel*-Formeln [[Wie nennt man die auf
wissenschaftlich? → Schulze]]. Der Maximalwert beträgt hier 55 Wörter.

![Statistik: Variation der Flexion von *beide/-iu* nach Wortdistanz (unbelebte Controller)](grafiken/2019-10-26_beide_cao_dist_i+i.png "Statistik: Variation der Flexion von *beide/-iu* nach Wortdistanz (unbelebte Erstcontroller)"){width=500dp}

# 4 Targets in Abhängigkeit der Wortart des Controllers

Wie erwähnt, korreliert die Wortart von Controller und Target mit der
syntaktischen Distanz; für ihre Studie zur Kongruenz im Serbo-Kroatischen
stellen Wechsler und Zlatić (2003: 84) folgendes Schema zur Kongruenz bei
Targets innerhalb eines Satzes auf:

| Agreement target    | CONCORD |     | INDEX |     | pragmatic |
|---------------------|:-------:|:---:|:-----:|:---:|:---------:|
| NP-internal         | ×       |     |       |     |           |
| secondary predicate | ×       |     |       |     |           |
| relative pronoun    | ×       | and | ×     |     |           |
| primary predicate   |         |     | ×     | or  | ×         |

"CONCORD" bezeichnet dabei die grammatikalisch-syntaktisch gesteuerte
Kongruenzbeziehung zwischen einem Phrasenkopf und seinen Attributen innerhalb
einer maximalen Projektion (also z. B. zwischen einem Substantiv und seinem
Artikel oder seinen Adjektiven). "INDEX" bezeichnet die
grammatikalisch-semantisch gesteuerte Kongruenzbeziehung zwischen einem
Phrasenkopf und anaphorischen Elementen, die sich auf ihn beziehen (also z. B.
einem Substantiv und darauf bezogene Pronomen).

Die syntaktische Distanz von Controller und Target nimmt in diesem Schema nach
unten hin zu; ebenso steigt die Wahrscheinlichkeit für semantische Kongruenz
(diese spielt für das INDEX-Feature bedingt eine Rolle, für pragmatische
Kongruenz jedoch in jedem Fall), was aus Corbetts (1979, 2006) *Agreement
Hierarchy* deutlich wird, die Wechsler und Zlatić (2003) adaptieren:

    (23) Agreement Hierarchy (Corbett 1979: 204; 2006: XXX):
         attributive > predicate > relative pronoun > personal pronoun

Corbett (1979) erklärt dazu:

> The possibility of syntactic agreement decreases monotonically from left to
> right. The further left an element on the hierarchy, the more likely
> syntactic agreement is to occur, the further right, the more likely semantic
> agreement. [...] The condition of monotonic decrease requires that if
> syntactic agreement with a particular controller occurs in a given position
> on the hierarchy it will also occur in all other positions to the left;
> conversely, if semantic agreement is possible in a given position it will
> also be possible in all other positions to the right. (204)

Fleischer (2012) erweitert Corbetts (1979) Hierarchie um einige Wortarten,
insofern er besonders bei Pronomen feinere Unterscheidungen trifft:

    (24) modifizierte Kongruenzhierarchie (Fleischer 2012: 192)
         Artikel, attributiv verwendetes Demonstrativpronomen > andere
         attributive Relationen > Relativpronomen > anaphorisch verwendetes
         Demonstrativpronomen, Personalpronomen > anaphorisch verwendetes
         Possessivpronomen

Wenn also zum Beispiel ein Relativpronomen semantische Kongruenz aufweist, dann
sollten Elemente, die weiter rechts in der Hierarchie stehen, dies auch
erlauben. Im Fall von koordinierten Nominalphrasen verschiedenen Geschlechts
würde sich semantische Kongruenz dadurch äußern, dass der Unterschied zwischen
männlich und weiblich im Extremfall aufgehoben ist und stattdessen nur noch die
Belebtheit eine Rolle spielt.

Die Controller im Urkundensample verteilen sich der Menge nach insgesamt
folgendermaßen. Insgesamt fällt die starke Konzentration auf Personal- und
Relativpromen als Controller auf.

| Anzahl | Wortart des direkten Controllers    |
|-------:|-------------------------------------|
|    133 | Personalpronomen                    |
|     39 | Relativpronomen                     |
|     38 | Substantiv                          |
|      9 | Eigenname                           |
|      6 | Demonstrativpronomen (anaphorisch)  |
|      4 | Eigenname + Eigenname               |
|      4 | Reflexivpronomen                    | 
|      3 | finites Vollverb                    |
|      3 | Substantiv + Substantiv             |
|      2 | Personalpronomen + Eigenname        |
|      1 | Personalpronomen + Personalpronomen |
|      1 | Possessivpronomen                   |
|      1 | Eigenname + Substantiv              |
|      1 | Adjektiv (anaphorisch)              |

Darunter finden sich folgende Wortarten bei der direkten Beziehung zwischen
*beide* und zwei Substantiven. Die Variation der Wortart ist hier nicht sehr
ausgeprägt. Im häufigsten Fall bilden zwei Namen die Controller von
*beide/-iu*.

| Anzahl | Wortart des direkten Controllers    |
|-------:|-------------------------------------|
|      4 | Eigenname + Eigenname               |
|      3 | Substantiv + Substantiv             |
|      2 | Personalpronomen + Eigenname        |
|      1 | Personalpronomen + Personalpronomen |
|      1 | Eigenname + Substantiv              |

Für anaphorische Controller, die das *beide/-iu*-Target indirekt mit einem
Erstcontroller verbinden, ergibt sich folgende Teilmenge. Hier sind es vor
allem Personalpronomen, die den Controller von *beide/-iu* bilden, doch auch
die drei weiter oben besprochenen Pro-drop-Verben tauchen hier auf, neben einem
nominalisierten Adjektiv (*vorgenannten*, CAO I: Nr. 199, 210.37), das auf ein
Erstcontrollerpaar verweist.

| Anzahl | Wortart des direkten Controllers    |
|-------:|-------------------------------------|
|    120 | Personalpronomen                    |
|     36 | Relativpronomen                     |
|      6 | Demonstrativpronomen (anaphorisch)  |
|      4 | Reflexivpronomen                    |
|      3 | finites Vollverb                    |
|      1 | Possessivpronomen                   |
|      1 | Adjektiv (anaphorisch)              |

Folgende Verteilung ergibt sich für die parallele Sammlung von *beide/-iu* in
Bezug auf einfache Plural-NPs. Hier steht *beide/-iu* besonders häufig in
direkter Abhängigkeit von Substantiven.

| Anzahl | Wortart des direkten Controllers    |
|-------:|-------------------------------------|
|     38 | Substantiv                          |
|     13 | Personalpronomen                    |
|      9 | Eigenname                           |
|      3 | Relativpronomen                     |

## 4.1 Nominale Controller
### 4.1.1 Koordinierte nominale Controller

Widmen wir uns zunächst unter den wenigen Belegen dem häufigsten Fall, in dem
zwei Namen koordiniert werden. Zunächst scheint sich eine Präferenz für *beide*
anzudeuten.

| Wortart 1 | Wortart 2 | Controller 1 | Controller 2 | beide |
|-----------|-----------|--------------|--------------|------:|
| NE        | NE        | 3SG.M        | 3SG.M        | (1)   |
| NE        | NE        | 3SG.M        | 3SG.F        | (2)   |
| NE        | NE        | 3SG.I[N]     | 3SG.I[N]     | (1)   |

Sieht man sich die Belege näher an, handelt es sich bei den zwei Belegen für
*beide* bei kombinierter 3SG.M und 3SG.F um die zwei Belege, die bereits oben
unter (4) besprochen wurden. Die zugehörige Urkunde trägt westmitteldeutsche
Züge und damit geht einher, dass hier allein *bede* steht und keine
Unterscheidung zwischen *e* und *iu*-Typen gemacht wird.

Dass bei der Kombination zweier 3SG.M die Form *beide* steht, überrascht
mittlerweile nicht mehr -- wie wir bereits gesehen haben, stellt dies die
generelle Tendenz dar. In diesem Fall stammt der Belege aus der Urkunde CAO IV
(Nr. 2607):

    (25) Daz B~chart vn̄ Cvͦnrat von lv̓ſtenowe [...] vn̄ hant dc beide / ain
         dem adern vor mir vf gegeben         
         'Dass Burkhard und Conrad von Lustnau ... und haben das beide, einer
         dem anderen, vor mir veräußert' (CAO IV: Nr. 2607, 32,40--33,1)

Dieser Beleg ist falsch klassifiziert: In der Koordination fällt ein mögliches
Subjektspronomen aus (*beide* ist ein gefloateter Quantor). Da das Satzkonjunkt
ein finites Verb enthält, kann im Zweifelsfall wohl dieses als nächstes overtes
Element angesehen werden, von dem *beide* abhängt, wenn man nicht gar die
syntaktische Lücke in der Phrasenstruktur anstelle des Subjekts, auf das sich
*beide* ansonsten beziehen würde, als Controller ansehen möchte (dies ist von
der bevorzugten Grammatiktheorie abhängig).

Der einzig verbleibende Beleg wurde bereits in (3) zitiert und ist ebenfalls in
seiner Kongruenzform nicht aussagekräftig, weil in der betreffenden Urkunde CAO
II (Nr. 923), wie oben bemerkt, *iu*- und *eu*-Formen nur beim definiten
Artikel und beim kurzen Demonstrativpronomen auftreten; adjektivisch
deklinierende Modifikatoren zeigen alle *e*.

Aufgrund der geringen Belegzahl liegen für *beide/-iu* in direkter Abhängigkeit
von zwei Substantiven (genauer gesagt: Appellativa) liegen keine Substantive
vor, die sich auf Personen beziehen. Inwiefern hier Variation in Abhängigkeit
der Wortart -- spezifisch im Vergleich zu Eigennamen -- vorliegt oder nicht,
lässt sich daher nicht feststellen.

| Wortart 1 | Wortart 2 | Controller 1 | Controller 2 | beidiu   |
|-----------|-----------|--------------|--------------|---------:|
| NA        | NA        | 3SG.I[M]     | 3SG.I[M]     |  1 (+ 1) |
| NA        | NA        | 3SG.I[N]     | 3SG.I[M]     |  1?      |

In allen Fällen liegt als Form des Targets *beidiu* vor; wir konnten bereits
oben feststellen, dass diese Form im Bezug auf unbelebte Controller die zu
erwartende darstellt. In einem der beiden Fälle für zwei unbelebte maskuline
Substantive liegt erneut ein fehlerhaft annotierter Beleg vor:

    (26) Daſ ich hern Ekhart dem waltman [...] Einen garten vnde einen aker
         ligent beidú bi der Rinderlinen aker vn̄ der Rúwerinen aker fúr lidig
         eigen gegeben         
         'dass ich Herrn Eckhard dem Waldmann ... einen Garten und einen Acker,
         (die) liegen beide bei der *Rinderlinen* Acker und der *Rúwerinen*
         Acker, als unbesetztes Eigen gegeben habe' (CAO IV: Nr. 3249,
         417.3--5)

Der Beleg in (26) ist insofern fehlerhaft, als *beidú* in einem uneingeleiteten
Relativsatz steht, entsprechend fehlt ein Subjektspronomen in Form eines
Relativpronomens, das *beidú* bestimmt. Der nächste overte Controller ist damit
das Verb *ligent* '(sie) liegen', das das Subjekt lizensiert. *Beidú* ist auch
in diesem Fall gefloatet. Daneben lässt sich in der vorliegenden Urkunde keine
Variation zwischen adjektivisch starker Flexion mit *e* und *iu* feststellen,
wobei jedoch Belege mit *e* und *iu*/*ú* in Freiburg im Breisgau prinzipiell
vorkommen (bei schneller Durchsicht der Belege korrespondierte *iu*/*ú* immer
mit dem NOM/ACC.N.PL).

Der andere Beleg für zwei unbelebte maskuline Substantive wurde bereits in (17)
zitiert und erscheint zutreffend. Im Fall des Belegs für eine Kombination eines
unbelebten Neutrums mit einem unbelebten Maskulinum steht *baidev* erneut in
einem Relativsatz:

    (27) Jch ſol in auch ir hovs vn̄ ir weingarten / baidev vn̄ ich von [in] han
         vn̄ ich in ['in' über der Zeile nachgetragen] gegeben auch han
         'Ich soll ihnen auch ihr Haus und ihren Weingarten, beide so ich von
         [ihnen] habe und ich ihnen auch gegeben habe' (CAO II: Nr. 1436,
         639.7--8)

Ob *baidev* sich hier anaphorisch auf *ir hovs vn̄ ir weingarten* bezieht und
vom Relativsatz *vn̄ ich von [in] han* modifiziert wird oder ob sich *baidev*
auf die Relativpartikel *vn̄* bezeiht, ist hier nicht ganz klar. In eindeutigen
Fällen folgt *beide/-iu* jedoch gewöhnlich seinem pronominalen Controller,
insofern ist die erstere Vermutung bei dieser Lesart wahrscheinlicher. Möglich
ist aber auch die Lesart, bei der eine korrelative Konjunktion *beide ... unde*
vorliegt: 'sowohl die ich von ihnen erhalten habe, als auch (die) ich ihnen
gegeben habe'.

Im Fall der Kombination eines Pronomens mit einem Eigennamen liegt sowohl ein
Beleg für *beide* als auch ein Beleg für *beidiu* vor. Die Vermutung liegt
nahe, dass dies mit der Kombination der Geschlechter der jeweiligen Referenten
zusammenhängt.

| Wortart 1 | Wortart 2 | Controller 1 | Controller 2 | beide | beidiu |
|-----------|-----------|--------------|--------------|------:|-------:|
| PPER      | NE        | 1SG.M        | 3SG.M        | (1)   |        |
| PPER      | NE        | 1SG.M        | 3SG.F        |       |  1     |

Wie wir bereits gesehen haben, tritt bei der Kombination von zwei Personen
gleichen Geschlechts in der Regel *beide* auf (28), bei der Kombination von
zwei Personen ungleichen Geschlechts gibt es zwar Variation, doch ist *beidiu*
sehr gebräuchlich (29).

    (28) ſo ſol ich [= Rudger von Aichaim] vn̄ Gerhoh von Radek [...] ze
         Salzburch in varen / vn̄ baide dar vz nimmer chomen         
         'so sollen ich und Gerhoch von Radeck ... nach Salzburg hinein fahren
         und beide nicht mehr dort herauskommen' (CAO II: Nr. 1244, 493,37--40)

Auch (28) ist aus syntaktischen Gründen falsch klassifiziert, da sich dafür
argumentieren lässt, dass sich *baide* auch hier auf ein Nullsubjekt bezieht.
Das Konjunkt *baide dar vz nimmer chomen* enthält selbst kein finites Verb;
dieses wird genauso wie das Subjekt vom ersten Teil impliziert und fällt der
Ellipse anheim. *Beide* ist in diesem Fall gefloatet. Weitere Belege aus
Salzburg deuten darauf hin, dass an diesem Ort Variation zwischen *e*- und
*iu*-Formen vorliegt.

    (29) Jch seibot von freihaim / vnd fraͮwe Peters mein Havſfraͮwe / veriehen
         beidev         
         'Ich, Seibot von Freiham, und Frau Peters, meine Ehefrau, verkünden
         beide' (CAO IV: Nr. 3248, 416.23)

In (29) befindet sich *beidev* als gefloateter Quantor im selben Satz wie sein
Controller. Aufgrund mangelnder Vergleichsmöglichkeiten ist nicht eindeutig,
welches der drei Kriterien den Ausschlag für die Kongruenzform vom Typ *iu*
gibt: Bezug auf zwei Personen unterschiedlichen Geschlechts, Quantifier-Float
oder die Wortart der Controller.

Des weiteren liegt ein einziger Beleg für die Kombination eines männlichen
Eigennamens mit einem auf eine weibliche Person referierenden Substantiv vor.
Auch hier fehlt jegliche Vergleichsmöglichkeit.

| Wortart 1 | Wortart 2 | Controller 1 | Controller 2 | beidiu |
|-----------|-----------|--------------|--------------|-------:|
| NE        | NA        | 3SG.M        | 3SG.F        |  1     |

Der zugehörige Beleg wird in (30) zitiert. Syntaktisch ist er unauffällig.

    (30) swenne aber her Rvͦdiger vnd ſin hovſfrowe bediv niht enſint    
         'Wenn aber Herr Rüdiger und seine Ehefrau beide nicht mehr sind'
         (CAO IV, Nr. 3262, 425.13--14)

---

**FRAGE:** Was ist die 'normale' Position von *beide*?

* *beide Rüdiger und seine Frau* → gewöhnlich bleibt hier *beide* in SpecVP?!
* *beide Eheleute*
* *sie beide*.

---

Zuletzt bleibt noch die ebenfalls einzige Kombination von zwei
Personalpronomen, wobei sich beide jeweils auf einen Mann beziehen, wie im
Beleg in (31) deutlich wird. Aufgrund der Belegmenge ist auch hier kein
Vergleich möglich; der Beleg ist ansonsten unauffällig.

| Wortart 1 | Wortart 2 | Controller 1 | Controller 2 | beide |
|-----------|-----------|--------------|--------------|------:|
| PPER      | PPER      | 1SG.M        | 1SG.M        |  1    |

    (31) wir · krafte von hohenloch · vn̄ · wir ludewic von durne · geloben
         baide vf vnſern eit         
         'Wir, Kraft von Hohenlohe, und wir, Ludwig von Walldürn, schwören
         beide bei unserem Eid' (CAO III, Nr. 2529, 563.5--6)

### 4.1.2 Unkoordinierte nominale Controller

Wie zuvor besteht auch beim Kontrollsample mit einfachen Plural-NPs als
Controller von *beide/-iu* das Problem, dass relativ wenige Belege vorliegen.
Variation in der Wortart besteht naturgemäß ohnehin nur zwischen appellativen
Substantiven und Eigennamen. Hier liegen zu wenige Belege mit Eigennamen vor,
um sinnvoll mit appellativen Substantiven vergleichen zu können. Aufgrund der
geringen Menge an (verwendbaren) Belegen für *beide/-iu* in direktem Bezug auf
koordinierte Controller ist im Grunde auch in dieser Hinsicht kein Vergleich
möglich.

| Wortart | Controller          | beide | beidiu | beid |
|---------|---------------------|------:|-------:|-----:|
| NA      | 3PL.M               | 13    | (2)    |  3   |
|         | 3PL.?[N]            |  1    |        |      |
|         | 3PL.I[M]            |  8    |        |      |
|         | 3PL.I[N]            | (5)   |  6     |      |
| NE      | 3PL.M               |  8    |        |  1   |

Die beiden eingeklammerten Belege zum Typ *beidiu* bei Substantiven der 3PL.M
wurden bereits in (5) zitiert und anschließend besprochen: Hier liegt
vermutlich die alemannische Ausweitung der *iu*-Form auf den gesamten Plural
vor. Der Beleg mit unbekanntem Geschlecht einer belebten 3PL wurde in (6)
zitiert; hier handelt es sich um das Wort *kinde*, das nirgends im Text weiter
definiert wird. Ausschlag zur Form *beide* gibt vermutlich der belebte Bezug.
Wie oben besprochen, fehlt bei allen fünf Belegen für *beide* beim Bezug auf
unbelebte Neutra Variation zwischen *e* und *iu*.

## 4.2 Referenzielle Controller
### 4.2.1 Bezug auf koordinierte Substantive

Wie wir oben gesehen hatten, sind unter den pronominalen Controllern von
*beide/-iu* die Personalpronomen insgesamt am häufigsten vertreten. Hier
herrscht entsprechend auch die größte Variation. Da die Belege so zahlreich
sind, dass es nicht möglich ist, jeden einzelnen zu untersuchen, sollen nur
diejenigen Zeilen berücksichtigt werden, in denen es die meiste Variation gibt,
beziehungsweise diejenigen Einzelbelege, die augenscheinlich aus der Reihe
fallen.

Insgesamt ergibt sich bei den Personalpronomen das gewohnt homogene Bild:
Targets mit persönlichem Bezug zeigen die Form *beide*, wobei bei
gemischtgeschlechtlichem Bezug die Form *beidiu* möglich ist und oftmals
*beide* sogar noch in der Anzahl übersteigt. Targets mit unpersönlichem Bezug
zeigen dagegen genusunabhängig *beidiu* -- wenn man die alemannischen Belege
vom Typ *beidú* denen für einfaches *beidiu* beirechnet.[^10]

[^10]: Belege vom Typ *beidú* erscheinen durchweg in Positionen, in denen auch
*beidiu* mit mehr als einem Einzelbeleg vertreten ist, daher sollte es möglich
sein, *beidú* neben *beidi* als Variante von *beidiu* anzusehen.

| Wortart | Controller          | beide | beidiu | beidú | beidi | beid |
|---------|---------------------|------:|-------:|------:|------:|-----:|
| PPER    | 1SG.M + 1SG.M       | 12    |        |       |       |      |
|         | 1SG.M + 1SG.F       |  4    |  2     |  1    |  1    |      |
|         | 1SG.F + 1SG.M       |  2    |        |       |       |      |
|         | 1SG.M + 3SG.M       |  4    |        |       |       |      |
|         | 1SG.M + 3SG.F       |  5    | 14     |  2    |       |      |
|         | 1SG.F + 3SG.M       |  2    |        |       |  2    |      |
|         | 3SG.M + 3SG.M       | 10    |        |       |       |  2   |
|         | 3SG.M + 3SG.F       | 14    | 17     | 10    |  1    |      |
|         | 3SG.F + 3SG.F       |  7    |        |       |       |      |
|         | 3SG.F + 3SG.M       |       |  2     |  3    |       |      |
|         | 3SG.I[M] + 3SG.I[M] |       |        |  1    |       |      |
|         | 3SG.I[M] + 3SG.I[N] |       |        |  1    |       |      |
|         | 3SG.I[N] + 3SG.I[M] |       |  1     |       |       |      |

Da die Tabelle relativ groß ist, möchte ich sie zur besseren Übersicht auch
hier nach Kombination der Controller aufteilen. Zunächst wollen wir uns die
Controller ansehen, die sich auf zwei NPs vom gleichen Geschlecht beziehen.

| Wortart | Controller          | beide | beid |
|---------|---------------------|------:|-----:|
| PPER    | 1SG.M + 1SG.M       | 12    |      |
|         | 1SG.M + 3SG.M       |  4    |      |
|         | 3SG.M + 3SG.M       | 10    |  2   |
|         | 3SG.F + 3SG.F       |  7    |      |

In allen Fällen steht hier *beide* bzw. seine apokopierte Form *beid*, wobei
freilich nicht ausgeschlossen werden kann, dass ein Teil der Belege für *beide*
aus westmitteldeutschen Urkunden stammt, bei denen ohnehin keine Variation
zwischen *beide* und *beidiu* zu erwarten ist. Auch die Form indifferente Form
*bede*, die im Elsass verbreitet ist, wurde nicht ausgefiltert. Da auch
andernorts mit fehlender Variation zu rechnen ist, bilden diese Belege eine Art
Grundrauschen; für jede Urkunde und jeden Belegort einzeln zu überprüfen, ob
Variation vorliegt oder nicht, würde den Aufwandsrahmen sprengen.

Das gleiche Vorbehalt gilt natürlich auch bei den folgenden Belegen in
Abhängigkeit von einem pronominalen Controller mit gemischtgeschlechtlichem
Bezug, wobei möglicherweise invariante alemannische *beidiu*-Formen noch
hinzukommen.

| Wortart | Controller          | beide | beidiu | beidú | beidi |
|---------|---------------------|------:|-------:|------:|------:|
| PPER    | 1SG.M + 1SG.F       |  4    |  2     |  1    |  1    |
|         | 1SG.F + 1SG.M       |  2    |        |       |       |
|         | 1SG.M + 3SG.F       |  5    | 14     |  2    |       |
|         | 1SG.F + 3SG.M       |  2    |        |       |  2    |
|         | 3SG.M + 3SG.F       | 14    | 17     | 10    |  1    |
|         | 3SG.F + 3SG.M       |       |  2     |  3    |       |

Insgesamt wird auch hier wieder deutlich, dass Variation hauptsächlich zwischen
*beide* und *beidiu* vorliegt, wobei *beidiu* (35 Belege) zahlenmäßig *beide*
(27 Belege) überlegen ist, insbesondere, wenn man die alemannischen Varianten
*beidú* (16 Belege) und *beidi* (4 Belege) noch hinzuzählt. Wie schon zuvor
beobachtet, sind Kombinationen mit erster und dritter Person sowie aus zwei
dritten Personen am häufigsten belegt. Die Belegzahl ist in dieser Gruppe zu
groß, als dass es sich lohnen würde, Einzelbelege zu besprechen.

Bei Personalpronomen mit unbelebter Referenz spielen Personenmerkmale -- wie
zuvor beobachtet -- keine Rolle, hier kommen keine Formen vom Typ *e* vor.
Allerdings ist die Belegzahl in dieser Gruppe sehr klein, sodass nur aufgrund
der Wortart des direkten Controllers keine zuverlässigen Aussagen gemacht
werden können.

| Wortart | Controller          | beidiu | beidú |
|---------|---------------------|-------:|------:|
| PPER    | 3SG.I[M] + 3SG.I[M] |        |  1    |
|         | 3SG.I[M] + 3SG.I[N] |        |  1    |
|         | 3SG.I[N] + 3SG.I[M] |  1     |       |

Beide Belege für *beidú* stammen aus Freiburg im Breisgau; die Urkunden CAO I:
(Nr. 326) und CAO IV (Nr. 3249) enthalten beide keine eindeutigen Belege für
Variation zwischen *e* und *iu* in der adjektivisch starken Pluralmarkierung.

Die zweite größere Gruppe innerhalb der pronominalen Controller von *beide/-iu*
bilden die Relativpronomen. Diese kommen mit persönlichem Bezug allerdings nur
zweimal im Belegmaterial vor, und beide Male steht eine Form vom Typ *beide* in
Bezug auf zwei Erstcontroller vom gleichen Geschlecht. Bei unbelebtem Bezug
steht sehr regelmäßig *beidiu* unabhängig vom Genus, wie zu erwarten. Die
größte Häufung besteht bei Belegen, die sich auf zwei neutrale NPs beziehen,
was eindeutig auf die Textsorte zurückzuführen ist: in 21 von 24 Fällen ist der
Bezug die Kombination zweier *insigel*, deren Anhängigkeit zur Beglaubigung
durch die Vertragsparteien im Urkundentext erwähnt wird.

| Wortart | Controller          | beide | beidiu | beidú | beid |
|---------|---------------------|------:|-------:|------:|-----:|
| DRELS   | 3SG.M + 3SG.M       |  1    |        |       |      |
|         | 3SG.F + 3SG.F       |  1    |        |       |      |
|         | 3SG.I[M] + 3SG.I[M] |  1    |  2     |       |  1   | <
|         | 3SG.I[M] + 3SG.I[F] |       |  3     |       |      |
|         | 3SG.I[N] + 3SG.I[N] |       | 23     |  1    |      |
|         | 3SG.I[N] + 3SG.I[M] |       |        |  1    |      |
|         | 3SG.I[N] + 3SG.I[F] |       |  1     |       |      |
|         | 3SG.I[N] + 3PL.I[M] |       |  1     |       |      |

Bei den beiden abweichenden Belegen mit *beid(e)* bei unbelebt-maskulinem Bezug
hadnelt es sich die in (10) zitierten Belege. Dabei deutete sich Variation
zwischen *e*- und *iu*-Formen zumindest in einer der beiden Urkunden an.

Die wenigen vorhandenen Belege für *beide/-iu* in direkter Abhängigkeit von
einem Demonstrativpronomen verhalten sich insgesamt unauffällig.

| Wortart | Controller          | beide   | beidiu | beidú |
|---------|---------------------|--------:|-------:|------:|
| DDS     | 3SG.M + 3SG.M       | 1 (+ 1) |        |       |
|         | 3SG.M + 3SG.F       |         |  1     |  1    |
|         | 3SG.I[N] + 3SG.I[N] | (1)     |        |       |
|         | 3SG.I[M] + 3SG.I[F] |         |  1     |       |

Der eingeklammerte Beleg für *beide* mit Bezug auf zwei männliche Personen
entstammt einer Veldenzer Urkunde (CAO II: Nr. 904), die einen
westmitteldeutschen Schreibdialekt aufweist. Der eingeklammerte Beleg für
*beide* mit Bezug auf zwei unbelebte Neutra wurde bereits in (10c) zitiert. In
beiden Fällen fehlt Variation zwischen *e*- und *iu*-Formen.

Bei den Reflexivpronomen liegen ebenfalls nur wenige Belege vor.

| Wortart | Controller          | beide | beidiu |
|---------|---------------------|------:|-------:|
| PRF     | 1SG.M + 1SG.M       |  1    |        |
|         | 1SG.M + 1SG.F       | (1)   |        |
|         | 1SG.M + 3SG.F       |       |  1     |
|         | 3SG.M + 3SG.F       |       |  1     |

Auffällig ist hier der Beleg für *beide* mit Bezug auf zwei erste Personen
unterschiedlichen Geschlechts in (32). Dieser Beleg weist für Straßburg
übliches, indifferentes *bede* auf.

    (32) Johannes Vende [...] vn̄ Grede [...] Wir verzihent oͧch vns bede alleſ
         rehteſ geiſtlicheſ vnde weltlicheſ
         'Johannes Vende ... und Grete ... Wir verzichten beide auf alle
         Rechtsansprüche, geistliche und weltliche' (CAO V, Nr. N 100 (144 a),
         69.41--71.24)

Im Fall des anderen Belegs für *beide* liegt zwar in der Urkunde selbst (CAO V:
Nr. N 727 (2240 a)) bei adjektivisch deklinierenden Modifikatoren keine
Variation zwischen *e* und *iu* vor, in anderen Urkunden aus Überlingen sind
jedoch *iu*-Formen belegt (z. B. *wir baidiu* in CAO III, Nr. 2226, 373.40).

In mehreren Fällen liegen Verben mit gedroppten Subjektspronomen vor, die als
nächste overte Controller für *beide/-iu* annotiert sind. In allen Fällen liegt
Bezug auf gemischtgeschlechtliche NPs vor, und in allen Fällen steht *beidiu*
oder eine Form, die mit *beidiu* zumindest assoziiert werden kann. Insgesamt
liegen jedoch nicht genügend Belege vor, um gesicherte Aussagen zu machen.

---

**ACHTUNG:** Oben sind noch ein paar Belege, die falsch annotiert wurden und
auch hier stehen müssten. Wie verhalten die sich?]

---

| Wortart | Controller          | beidiu | beidú | beidi |
|---------|---------------------|-------:|------:|------:|
| VVFIN   | 1SG.M + 3SG.F       |  1     |  1    |       |
|         | 3SG.F + 3SG.M       |        |       |  1    |

Ein einzelner Beleg liegt vor für ein (pro)nominalisiertes Adjektiv, das als
direkter Controller von *beide* interpretiert wurde (33).

| Wortart | Controller          | beide |
|---------|---------------------|------:|
| ADJS    | 3SG.F + 3SG.M       |  1    |

In diesem Fall liegt erwartungsgemäß die Form *beide* vor, wobei die Urkunde
ansonsten stets *ſi* und *di* enthält, sowie die Form *alli di liute* (210.26).
Ob hier Variation zwischen *e* und *-iu* (→ *-i*) vorliegt, ist nicht sicher,
wenn auch für Basel generell Belege sowohl mit *e* als auch mit *iu* vorliegen.

    (33) alſe [...] vro govte / vn viuian ir wirt / die vorgenanten beide vnſ
         batent
         'wie ... Frau Guta und Vivian, ihr Ehemann, die beiden vorgenannten,
         uns baten' (I.199.210.36--37)

Ebenfalls liegt ein Einzelbeleg vor für ein Possessivpronomen, das als direkter
Controller von *beide* interpretiert wurde. In diesem Fall bezieht sich das
Pronomen auf zwei Männer, der Quantor ist allerdings vom Typ *beidiu*.

| Wortart | Controller          | beidiu |
|---------|---------------------|-------:|
| DPOSA   | 1SG.M + 1SG.M       | (1)    |

Dieser Beleg wurde in (9a) zitiert und für falsch klassifiziert befunden:
Wahrscheinlicher ist, dass sich *bediu* in CAO II (Nr. 682, 96.11) auf das
nachfolgende Wort *inſigel* bezieht: *[vnſer [bediu inſigel]]* 'unsere beiden
Siegel'.

### 4.2.2 Bezug auf unkoordinierte Plural-Substantive

Auch beim indirekten Bezug auf unkoordinierte Plural-Substantive als
Erstcontroller zeigen die meisten Belege für *beide/-iu*-Targets ein
Personalpronomen als direkten Controller. Auch hier liegen pro Personenmerkmal
nur wenige Belege vor.

| Wortart | Controller          | beide | beidiu | beid |
|---------|---------------------|------:|-------:|-----:|
| PPER    | 3PL.M               | 1     |        | 2    |
|         | 3PL.F               | 3     |        |      |
|         | 3PL.F[N]            |       | 2      |      |
|         | 3PL.?[N]            |       | 4      |      |
|         | 3PL.I[F]            | 1     |        |      |

Die wenigen vorhandenen Belege verteilen sich so, dass Bezug auf männliche und
weibliche Personen mit der Form *beid(e)* einhergeht, ebenso liegt bei einem
unbelebten Femininum ein *e*-Beleg vor. Belege für Bezug auf *kinde*, deren
Geschlecht bekannt ist (weiblich), zeigen immerhin zweimal die Form *beidiu*.
Genauso verhält es sich bei den vier *kinden*, deren Geschlecht unbekannt ist.
Insgesamt ergibt sich also der Eindruck, dass hier nicht das natürliche
Geschlecht, sondern das grammatische ausschlaggebend für die Kongruenzform des
Targets ist. Dieser Befund lag allerdings auch schon oben vor, ohne Bezug auf
die Wortart des Controllers im Speziellen.

Lediglich drei Belege für *beide* beziehen sich auf ein Relativpronomen.

| Wortart | Controller          | beide |
|---------|---------------------|------:|
| DRELS   | 3PL.M               | (1)   |
|         | 3PL.I[F]            |  2    |

Von diesen dreien muss ein Beleg ausscheiden (CAO III, Nr. 1820, 134.38), da er
elsässisch *bede* aufweist -- der Urkundenort wird zwar mit Bad Mergentheim
angegeben, der Urkundentext ist aber in alemannischem Schreibdialekt verfasst.
Auch einer der verbleibenden zwei Belege hat die Form *bede* (CAO III, 1950,
212.18). Da diese Urkunde jedoch aus Basel stammt und zumindest beim
historischen kurzen Demonstrativum einige Belege für *dú* enthält (neben
einigen Belegen für adjektivisches *iu* in Basel generell), kann hier nicht
klar entschieden werden. In beiden verbleibenden Fällen kann, ebenso wie bei
den Personalpronomen, dafür argumentiert werden, dass sich die Deklination des
Targets nach dem Genus der unbelebten Referenz richtet.

### 4.2.4 Zusammenfassung

Insgesamt hat sich bei der Auswertung der Belege nach Wortart (und
Personenmerkmalen) das bewährte Muster gezeigt: Bei direkter Abhängigkeit von
*beide/-iu* von pronominalen Controllern mit Bezug auf koordinierte NPs ist das
natürliche Geschlecht ausschlaggebend, insofern bei der Kombination von
Erstcontrollern gleichen Geschlechts die Form *beide* auftritt, bei
unterschiedlichem Geschlecht die Form *beidiu*, und bei unbelebten Referenten
generell *beidiu* unabhängig von deren Genus. Bei pronominalem Bezug auf
einfache Plural-NPs hingegen korrespondiert die Deklination des Targets
hingegen anscheinend mit dem Genus, wenn von den wenigen Belegen überhaupt auf
eine Gesetzmäßigkeit geschlossen werden kann.

Sofern genügend Belege vorhanden waren (eigentlich nur bei Personal- und
Relativpronomen als Controller), hat sich kein auffälliger Unterschied im
Verhalten der Targets in Abhängigkeit der verschiedenen Wortarten ihrer
Controller gezeigt.

* Lassen sich irgendwelche Wechsel in der Flexion bei anaphorischen Ketten
  beobachten (z.B. *X unde Y diu ... sie beide*?)
  